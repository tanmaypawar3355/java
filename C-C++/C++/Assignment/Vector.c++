#include<iostream>
#include<vector>

int main() {

	std::vector<int> vec = {10,20,30,50,30};

	//size
	std::cout << "Size of vector is " << vec.size() << std::endl;
	
	//max_size	
	std::cout << "Size of vector is " << vec.max_size() << std::endl;

	//resize
	vec.resize(100);
	std::cout << "After resizing size of vector is " << vec.size() << std::endl;
	
	//capacity
	vec.resize(40);
	std::cout << "Capacity of vector is " << vec.capacity() << std::endl;
	vec.resize(200);
	std::cout << "Again Capacity of vector is " << vec.capacity() << std::endl;

	//empty
	if(vec.empty() == 0){
                std::cout << "Bharlay" << std::endl;
        }else{
                std::cout << "Kahich nahiye" << std::endl;
        }

	//reserve
	vec.reserve(500);
	std::cout << "New capacity of vector is " << vec.capacity() << std::endl;

	vec.shrink_to_fit();
	std::cout << "After shrinking capacity of vector is " << vec.capacity() << std::endl;
	std::cout << "After shrinking size of vector is " << vec.size() << std::endl;




	// Element access
	// at
	std::cout << "Element at 3 is " << vec.at(3) << std::endl;
	
	//front
	std::cout << "Element at front is " << vec.front() << std::endl;

	//back
	std::cout << "Element at back is " << vec.back() << std::endl;

	//data
	std::cout << "Address of data is " << vec.data() << std::endl;
	std::cout << "data is " << *(vec.data()) << std::endl;





	// Modifiers
	//assign
	vec.assign(7,100);
	std::vector<int>::iterator itr;
	
	//push_back
	std::cout << "After push back" << std::endl;
	vec.push_back(500);
	for(itr = vec.begin(); itr != vec.end(); itr++){
		std::cout << *itr << std::endl;
	}

	//pop_back
	std::cout << "After pop back" << std::endl;
	vec.pop_back();
	for(itr = vec.begin(); itr != vec.end(); itr++){
		std::cout << *itr << std::endl;
	}
		
	//insert
	std::cout << "After inserting" << std::endl;
	vec.insert(vec.begin(),200);
	for(itr = vec.begin(); itr != vec.end(); itr++){
		std::cout << *itr << "    ";
	}
	
	//erase
	//std::vector<int>::const_iterator citr;
	std::cout << "\nAfter erasing" << std::endl;
	vec.erase(vec.begin());
	for(itr = vec.begin(); itr != vec.end(); itr++){
		std::cout << *itr << "    ";
	}

	//swap
	std::vector<double> dvec = {10.5,20.5,30.5,50.5,30.5};
	std::vector<double> ddvec = {10.60,20.60,30.60,50.60,30.60};

	dvec.swap(ddvec);

	//clear
	std::cout << "\nClearing vector" << std::endl;
	ddvec.clear();
	std::vector<double>::iterator itr2;
	for(itr2 = ddvec.begin() ; itr2 != ddvec.end() ; itr2++) {

		std::cout << *itr2 << std::endl;
	}
	
	//emplace
	std::cout << "After emplacing at end" << std::endl; 
	dvec.emplace(dvec.end(),1000.5);
	for(itr2 = dvec.begin() ; itr2 != dvec.end() ; itr2++) {

		std::cout << *itr2 << std::endl;
	}
	
	return 0;
}
