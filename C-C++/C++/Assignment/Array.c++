#include<iostream>
#include<array>

class ArrayDemo {

	std::array<int,4> arr;

	public:
	ArrayDemo(std::array<int,4> arr) {

		this -> arr = arr;
	}

	int operator[](int num) {

		return arr[num];
	}
};

int main() {
		
	std::array<int,4> arr = {10,20,30,40};

	//capacity
	//size
	std::cout << arr.size() << std::endl;
	
	//max_size
	std::cout << arr.max_size() << std::endl;

	//empty
	//std::cout << arr.empty() << std::endl;
	if(arr.empty() == 0){
		std::cout << "Bharlay" << std::endl;
	}else{
		std::cout << "Kahich nahiye" << std::endl;
	}

	

	//Element access
	//operator[]
	ArrayDemo obj(arr);
	std::cout << "Operator[] " << obj[3] << std::endl;	

	//at
	std::cout << "elemeny at 2 is " << arr.at(2) << std::endl;

	//front
	std::cout << "front of arr is " << arr.front() << std::endl;
	
	//back
	std::cout << "back of arr is " << arr.back() << std::endl;
	
	//data
	std::cout << *(arr.data()) << std::endl;



	//Modifiers
	//fill
	std::cout << "Fill" << std::endl;
	arr.fill(500);
	std::array<int,4>::iterator itr1;
	for(itr1 = arr.begin() ; itr1 != arr.end() ; itr1++) {

		std::cout << *itr1 << std::endl;
	}


	std::array<int,4> arr1 = {10,20,30,40};
	std::array<int,4> arr2 = {1000,2000,3000,4000};

	arr1.swap(arr2);
}
