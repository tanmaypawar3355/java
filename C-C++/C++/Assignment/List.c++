#include<iostream>
#include<list>

int main() {

	std::list<int> lst = {10,20,30,40,50,20,40};

	std::list<int>::iterator itr1;

	// Iterator
	// begin & end
	std::cout << "begin & end" << std::endl;
	for(itr1 = lst.begin() ; itr1 != lst.end() ; itr1++) {

		std::cout << *itr1 << std::endl;
	}

	// rbegin & rend
	std::cout << "rbegin & rend" << std::endl;
	std::list<int>::reverse_iterator itr2;
	for(itr2 = lst.rbegin() ; itr2 != lst.rend() ; itr2++) {

		std::cout << *itr2 << std::endl;
	}

	// cbegin & cend
	std::cout << "cbegin & cend" << std::endl;
	std::list<int>::const_iterator itr3;
	for(itr3 = lst.cbegin() ; itr3 != lst.cend() ; itr3++) {

		std::cout << *itr3 << std::endl;
	}
	
	// crbegin & crend
	std::cout << "crbegin & crend" << std::endl;
	std::list<int>::const_reverse_iterator itr4;
	for(itr4 = lst.crbegin() ; itr4 != lst.crend() ; itr4++) {

		std::cout << *itr4 << std::endl;
	}
	

	// Capacity	
	// size
	std::cout << "size of lst is " << lst.size() << std::endl;
	
	// empty	
	//std::cout << lst.empty() << std::endl;
	
	// max_size
	std::cout << "Max size of lst is " << lst.max_size() << std::endl;

	
	// Element access
	// front
	std::cout << "Front of lst is " << lst.front() << std::endl;

	// back
	std::cout << "Back of lst is " << lst.back() << std::endl;
	



	// Modifiers
	// assign
	std::cout << "assign - Assigning 100" << std::endl;
	std::list<int>::iterator itrr1;
	lst.assign(6,100);	
	for(itrr1 = lst.begin() ; itrr1 != lst.end(); itrr1++) {
		std::cout << *itrr1 << std::endl;
	}

	// emplace_front  // replace krto pahilya element la  
	lst.emplace_front(500);
	std::cout << "emplace front" << std::endl;
	for(itrr1 = lst.begin() ; itrr1 != lst.end(); itrr1++) {
		std::cout << *itrr1 << std::endl;
	}

	// push_front    // sagle element pudhe dhaklun 0th vr nvin element thevto
	lst.push_front(1000);
	std::cout << "push front" << std::endl;
	for(itrr1 = lst.begin() ; itrr1 != lst.end(); itrr1++) {
		std::cout << *itrr1 << std::endl;
	}
	
	// pop_front   // pahila element gayab krun bakiche element mage dhakalto
	lst.pop_front();
	std::cout << "pop front" << std::endl;
	for(itrr1 = lst.begin() ; itrr1 != lst.end(); itrr1++) {
		std::cout << *itrr1 << std::endl;
	}

	// emplace_back  // replace krto last element la  
	lst.emplace_back(2500);
	std::cout << "emplace back" << std::endl;
	for(itrr1 = lst.begin() ; itrr1 != lst.end(); itrr1++) {
		std::cout << *itrr1 << std::endl;
	}

	// push_back   // 9th pos la thevto element theun
	lst.push_back(7000);
	std::cout << "push back" << std::endl;
	for(itrr1 = lst.begin() ; itrr1 != lst.end(); itrr1++) {
		std::cout << *itrr1 << std::endl;
	}
	
	// pop_front  // pahila element gayab krto
	lst.pop_back();
	std::cout << "pop back" << std::endl;
	for(itrr1 = lst.begin() ; itrr1 != lst.end(); itrr1++) {
		std::cout << *itrr1 << std::endl;
	}

	// emplace
	 lst.emplace(lst.end(),570);
	
	//insert
	lst.insert(lst.begin(),777);
	

	// erase
	lst.erase(lst.begin());

	// swap
	//lst.swap(3,7);
	std::list<int> lst3 = {10,20,30,40,50};
	std::list<int> lst4 = {100,120,130,140,150};
	//lst.swap(10.5f,50.5f);
	

	//resize
	std::cout << "size of lst is " << lst.size() << std::endl;
	lst.resize(500);
	std::cout << "New size of lst is " << lst.size() << std::endl;
	

	// clear
	std:: cout << "Clear zalyavr" << std::endl;
	lst.clear();
	lst.push_front(1900);
	for(itr1 = lst.begin() ; itr1 != lst.end() ; itr1++) {

		std::cout << *itr1 << std::endl;
		std::cout << "for loop" << std::endl;
	}
	
	// remove  // specific element dlt
	lst.remove(1900);



	return 0;
}




