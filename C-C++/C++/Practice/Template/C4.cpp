#include<iostream>

/*
template<typename T>
T min(T x, T y) {
	
	return (x < y)? x : y;
}
*/

template<typename T>
T min(T x, T y);

template<>
char min<char>(char x, char y) {
	
	return (x < y)? x : y;
}

template<>
int min<int>(int x, int y) {
	
	return (x < y)? x : y;
}

template<>
float min<float>(float x, float y) {
	
	return (x < y)? x : y;
}

template<>
double min<double>(double x, double y) {
	
	return (x < y)? x : y;
}

int main() {

        std::cout << min('A','B') << std::endl;
        std::cout << min(30,20) << std::endl;
        std::cout << min(30.5f,20.5f) << std::endl;
        std::cout << min(30.5,20.5) << std::endl;

        return 0;
}


