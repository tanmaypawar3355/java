#include<iostream>

class Employee {

	double sal;

	public :
		
		Employee(double sal) {

			this -> sal = sal;
		}

		int max(int x, int y) {

			if(x > y) 
				return x;
			else 
				return y;
		}

		Employee max(Employee& x, Employee& y) {

			if(x > y)
				return x;
			else 
				return y;
		}

		friend bool operator>(Employee& obj1,Employee& obj2) {

			return(obj1.sal > obj2.sal);
		}

		friend std :: ostream& operator<< (std::ostream& out, Employee& obj) {

			out << obj.sal;
			return out;
		}
};

int main() {

	Employee ashish(200000.00);
	Employee kanha(250000.00);

	std :: cout << ashish.max(ashish,kanha) << std :: endl;
	std :: cout << ashish.max(10,50) << std :: endl;
}
