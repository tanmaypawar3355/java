#include<iostream>

class AddDemo {

	public :
		template<typename T>
		T add(T x, T y) {

			return x + y;
		}
};

int main() {

	AddDemo obj;

	std :: cout << obj.add(10,20) << std :: endl;
	std :: cout << obj.add(10.5f,20.5f) << std :: endl;

	return 0;
}
