#include<iostream>

class Demo {

	public:
		Demo() {
			
			std::cout << "In constr" << std::endl;
		}

		~Demo() {
			
			std::cout << "In destr" << std::endl;
		}
};

int main() {

	Demo obj1;

	{
		Demo obj2;
	}

	std::cout << "End main" << std::endl;

	return 0;
}
