#include<iostream>

class Demo {
	
	public :
		int *ptrA = NULL;

		Demo() {

			ptrA = new int[50];

			for(int i = 0 ; i <= 50 ; i++){

				std::cout << i << std::endl;
				
				ptrA[i] = i;
			}

			std::cout << "constr" << std::endl;
		}

		~Demo() {

			//delete ptrA;
			delete[] ptrA;
			std::cout << "In destructor" << std::endl;
		}
};

int main() {

	Demo obj;

	return 0;
}
