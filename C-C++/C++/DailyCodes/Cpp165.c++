#include<iostream>

class Parent {

	int x = 10;
	int y = 20;

	public:
		Parent() {

			std::cout << "Constructor parent" << std::endl;
		}

		int a = 50;

		~Parent() {
			
			std::cout << "Destructor parent" << std::endl;
		}

		void printData() {
			
			std::cout << x << y << a << std::endl;
		}
};

class Child : public Parent {

	int z = 10;

	public:
		Child() {
			
			std::cout << "Constructor child" << std::endl;
		}

		~Child() {
			
			std::cout << "Destructor child" << std::endl;
		}

		void getData() {
			
			std::cout << z << a << std::endl;
		}
};

int main() {

	Child obj2;

	obj2.getData();
	obj2.printData();
	return 0;
}
