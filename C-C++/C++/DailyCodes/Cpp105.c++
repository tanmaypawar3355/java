#include<iostream>

class Demo {

	public:
		int x = 10;

		Demo() {

			std::cout << "In constr" << std::endl;
		}

		void getData() {

			std::cout << x << std::endl;
		}
};

int main() {

	Demo obj;
	obj.getData();

	obj.x = 50;
	obj.getData();

	return 0;
}

