#include<iostream>

class Parent {

	int x = 10;

	protected:
	int y = 20;

	public:
	int z = 30;

};

int main() {

	Parent obj;

	std::cout << obj.y << std::endl;
	std::cout << obj.z << std::endl;

	return 0;
}
