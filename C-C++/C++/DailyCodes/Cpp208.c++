#include<iostream>

class Parent {

	public:
		Parent() {

			std::cout << "Parent constructor" << std::endl;
		}

		virtual ~Parent() {
			
			std::cout << "Parent destructor" << std::endl;
		}
};

class Child : public Parent {

	public:
		int *ptr = NULL;
		Child() {
			
			ptr = new int[5];

			std::cout << "Child constructor" << std::endl;
		}

		~Child() {
			
			std::cout << "Child destructor" << std::endl;
			delete []ptr;
		}
};

int main() {

	Parent *obj = new Child();

	delete obj;

	return 0;
}
