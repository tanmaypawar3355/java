#include<iostream>

int* fun(int* val) {

	int a = *val + 40;

	std::cout << *val << std::endl;
	std::cout << a << std::endl;

	return &a;
}

int main() {

	int x = 10;
	std::cout << x << std::endl;

	int *ret = fun(&x);
	std::cout << *ret << std::endl;
	
	return 0;
}
