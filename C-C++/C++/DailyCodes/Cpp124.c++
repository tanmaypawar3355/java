#include<iostream>

class Demo {

	public:
	int x = 10;

	friend int operator+(const Demo& obj1,const Demo& obj2);
};

int operator+(const Demo& obj1,const Demo& obj2) {

	return obj1.x + obj2.x;
}

int main() {

	Demo obj1;
	Demo obj2;

	std::cout << obj1 + obj2 << std:: endl;

	return 0;
}

