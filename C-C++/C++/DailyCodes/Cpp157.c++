#include<iostream>

class Parent {

	public:
		Parent() {

			std::cout << "Constructor parent" << std::endl;
		}
};

class Child : public Parent {

	public:
		Child() {
			
			std::cout << "Constructor child" << std::endl;
		}
};

int main() {

	Child obj;

	return 0;
}
