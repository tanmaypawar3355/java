#include<iostream>

class Demo {

	public:
	int  x = 10;

	void getData() {

		std::cout << "In getData" << std::endl;
	}

	friend std::ostream& operator<<(const std::ostream& cout,const Demo& obj);
};

std::ostream& operator<<(const std::ostream& cout,const Demo& obj) {

	std::cout << obj.x << std::endl;
	//return cout;
}

int main() {

	Demo obj;

	std::cout << obj << std::endl;

	return 0;
}
