#include<iostream>

class Demo {

	public:
		Demo() {

			Demo obj5(10);
			Demo(10);
			Demo obj6(obj5);
			std::cout << "No-args" << std::endl;
		}

		Demo(int x) {
			
			std::cout << "Para" << std::endl;
		}

		Demo(Demo &xyz) {

			std::cout << "Copy" << std::endl;
		}
};

int main() {

	Demo obj1;
}


