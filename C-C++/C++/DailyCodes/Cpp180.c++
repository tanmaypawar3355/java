#include<iostream>

class Parent1 {

	public:
		Parent1() {

			std::cout << "constructor parent1" << std::endl;
		}
};

class Parent2 {

	public:
		Parent2() {

			std::cout << "constructor parent2" << std::endl;
		}
};

class Child : public Parent1,public Parent2 {

	public:

		Child() {

			std::cout << "Constructro child" << std::endl;
		}
};

int main() {

	Child obj;
	return 0;
}




