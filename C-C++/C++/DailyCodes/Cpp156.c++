#include<iostream>

class Parent {

	int x = 10;

	protected:
	int y = 20;

	public:
	int z = 30;

 	Parent() {     // parameterized constructor dila ki no argument constructor pn dyava lagto

	}

	Parent(int x,int y,int z) {

		this -> x = x;
		this -> y = y;
		this -> z = z;
	}

	void getData() {

		std::cout << x << y << z << std::endl;
	}
};

class Child : public Parent {           // This is privare inheritance
				 
	public:

	Child(int x,int y,int z) {

		Parent(x,y,z);
	}

	void getInfo() {

		getData();
	}
};

int main() {

	Child obj(40,50,60);
	obj.getInfo();
	
	return 0;
}

