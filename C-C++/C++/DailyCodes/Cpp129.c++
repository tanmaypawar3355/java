#include<iostream>

class Demo {

	int x = 10;

	public:
		Demo(int x) {

			this -> x = x;
		}

		int getData() const {

			return x;
		}
};

int operator+(const Demo& obj1,const Demo& obj2) {

	// return obj1.x + obj2.x;  
	// hyala erroe yenar karan x ha private ahe Demo madhi, ani ha normal funtion ahe. 
	// Friend function la private variable access astat pn normal function la class chya private variable cha access nasato
	
	return obj1.getData() + obj2.getData();
}

int main() {

	Demo obj1(30);
	Demo obj2(40);

	std::cout << obj1 + obj2 << std::endl;

	return 0;
}

	
