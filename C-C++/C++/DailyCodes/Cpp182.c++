#include<iostream>

class Parent {

	public:
		Parent() {

			std::cout << "Parent constructor" << std::endl;
		}

		virtual void getData(int x) {       //getData(int)
			
			std::cout << "Parent getData" << std::endl;
		}

		virtual void printData(float x) {    //printData(float)
			
			std::cout << "Parent printData" << std::endl;
		}
};

class Child : public Parent {

	public:
		Child() {
			
			std::cout << "Child constructor" << std::endl;
		}

		//overriding not allowed
		void getData(short int x) {    //getData(short int)
			
			std::cout << "Child getData" << std::endl;
		}
		
		//overriding
		void printData(float x) {      //printData(float)  
			
			std::cout << "Child printData" << std::endl;
		}
};

int main() {

	Parent *obj = new Child();
	obj -> getData(10);          // obj*.getData();
	obj -> printData(10.5f);     // obj*.printData();

	return 0;
}

