#include<iostream>

void fun(int x, int y) {

        std::cout <<  "int-int" << std::endl;
}
void fun(int x, float y) {

        std::cout << "int-float" << std::endl;
}

int main() {

        fun(10,20);
        fun(10,20.5f);
    
        return 0;
}
