#include<iostream>

class Parent {

	int x = 10;

	public:

	Parent() {
		
		std::cout << "Parent constructor" << std::endl;
	}

	Parent(Parent& obj) {

		std::cout << "Copy" << std::endl;
	}

	virtual void getData() {
		
		std::cout << "Parent x = " << x << std::endl;
	}
};

class Child : public Parent {

	int x = 10;

	public:

	Child() {
		
		std::cout << "Child constructor" << std::endl;
	}

	void getData() {
		
		// 1
		Parent::getData();		
		std::cout << "Child x = " << x << std::endl;
	}
};


int main() {

	Child obj;

	(Parent(obj)).getData();

	return 0;
}
