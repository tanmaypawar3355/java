#include<iostream>

class Parent {

	public:
		virtual void getData() {

			std::cout << "Parent getData" << std::endl;
		}

		virtual void printData() {
			
			std::cout << "Parent printData" << std::endl;
		}
};

class Child1 : public Parent {

	public:
		void getData() {
			
			std::cout << "Child1 getData" << std::endl;
		}
};

class Child2 : public Parent {

	public:
		void printData() {
			
			std::cout << "Child2 printData" << std::endl;
		}
};

int main() {

	Parent *obj = new Child1();

	obj -> getData();
}

			
