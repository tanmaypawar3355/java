#include<iostream>

class Demo {
	
	int x = 10;
	
	public:
	int operator++() {

		return ++x;
	}
};


int main() {

	Demo obj;

	std::cout << ++obj << std::endl;

	return 0;
}
