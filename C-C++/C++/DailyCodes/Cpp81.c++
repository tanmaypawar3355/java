#include<iostream>

class Demo {

	int x = 10;           // instance/non-static global
	static int y;	      // global static // class madhi initialize karta yet nhi. Baher karava lagto

	public:
	void fun() {

		int a = 10;          // local variable
		static int b = 20;   // local static variable

		std::cout << x << std::endl;
		std::cout << y << std::endl;
		std::cout << a << std::endl;
		std::cout << b << std::endl;
	}

 	static void gun() {

		int a = 10;
		static int b = 20;
		
		//std::cout << x << std::endl;
		std::cout << y << std::endl;
		std::cout << a << std::endl;
		std::cout << b << std::endl;
	}
};

int Demo::y = 20;

int main() {

	Demo obj1;
	Demo *obj2 = new Demo();

	obj1.fun();
	obj1.gun();

	obj2 -> fun();
	obj2 -> gun();

	return 0;
}

