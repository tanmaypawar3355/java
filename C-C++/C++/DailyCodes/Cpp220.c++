#include<iostream>

template<class T>
class Template {

	T data;
	public:

	Template() {

		std::cout << "In constructor" << std::endl;
	}
};

int main() {

	Template *obj1 = new Template();
	Template *obj2 = new Template();

	return 0;
}
