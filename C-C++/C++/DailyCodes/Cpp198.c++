#include<iostream>

class IDemo {

	public:
		virtual void fun1() = 0;
		virtual void fun2() = 0;

};

class Adapter : public IDemo {

	public :
		void fun1() {
		
		}
		void fun2() {

		}
};

class DemoChild1 : public Adapter {
	
	public:
	void fun1() {

		std::cout << "child1" << std::endl;
	}
};

class DemoChild2 : public Adapter {

	public:
	void fun2() {
		
		std::cout << "child2" << std::endl;
	}
};

int main() {

	DemoChild1 *obj1 = new DemoChild1();
	DemoChild2 *obj2 = new DemoChild2();

	return 0;
}


