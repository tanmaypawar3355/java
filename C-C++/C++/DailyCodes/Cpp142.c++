#include<iostream>

class Demo {

	int x;

	public:
	Demo(int x) {

		this -> x = x;
	}

	int getX() const {

		return x;
	}

	int operator+(const Demo& obj2) {

		std::cout << "member" << std::endl;
		return this->x + obj2.x;
	}

	friend int operator+(const Demo& obj1,const Demo& obj2) {

		std::cout << "friend" << std::endl;

		return obj1.x + obj2.x;
	}
};

int operator+(const Demo& obj1,const Demo& obj2) {

	return obj1.getX() + obj2.getX();
}


int main() {

	Demo obj1(50);
	Demo obj2(100);

	std::cout << obj1 + obj2 << std::endl;

	return 0;
}
