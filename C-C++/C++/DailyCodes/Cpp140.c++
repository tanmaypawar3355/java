#include<iostream>

class Demo {

	int x = 10;

	public:
		Demo(int x) {

			this -> x = x;
		}

		friend std::ostream& operator<<(std::ostream& out,const Demo& obj) {

			out << obj.x;
			return out;
			
		}
};

int main() {

	Demo obj(50);

	std::cout << obj << std::endl;

	return 0;
}
