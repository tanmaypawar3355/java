#include<iostream>

class Parent {

	int x = 10;

	public:

	Parent() {

		std::cout << "Parent" << std::endl;
	}

	friend std::ostream& operator<<(std::ostream& out,const Parent& obj) {

		out << "In Parent" << std::endl;
		out << obj.x;
		return out;
	}

	Parent(Parent& obj) {
		
		std::cout << "Copy" << std::endl;
	}
};

class Child : public Parent {

	int x = 20;

	public:

	Child() {
		
		std::cout << "Child" << std::endl;
	}
	
	friend std::ostream& operator<<(std::ostream& out,const Child& obj) {

		out << "In Child" << std::endl;
		out << obj.x;
		return out;
	}
};

int main() {

	Child obj1;

	std::cout << (const Parent)obj1 << std::endl;    // copy 
	std::cout << (const Parent&)obj1 << std::endl;	 // No call to copy

	Child obj2;

	std::cout << obj2 << std::endl;
	//operator<<(ostream&,child&)
	
	return 0;
}

