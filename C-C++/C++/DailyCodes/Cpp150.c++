#include<iostream>

class Demo {

	int arr[5] = {10,20,30,40,50};

	public:

	void getArray() {

		for(int i = 0 ; i < (sizeof(arr)/sizeof(int)) ; i++){

			std::cout << arr[i] << "  ";
		}
		std::cout << std::endl;
	}

	int& operator[](int index) {

		return arr[index];
	}
	
	int operator()(int x,int y) {

		return x+y;
	}
};

int main() {

	Demo obj;

	obj[2] = 70;

	obj.getArray();

	int ret = obj(40,50);
	std::cout << ret << std::endl;

	return 0;
}
