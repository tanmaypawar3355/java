#include<iostream>

class Demo {

	int x;
	int y;

	public:

		friend std::istream& operator>>(std::istream& in,Demo& obj) {   
		
			// ith const Demo& obj ghetla tr const kleyamule to data nhi chnage krun shaknar

			in >> obj.x;
			in >> obj.y;	

			return in;
		}

		void printData() {

			std::cout << x << " " << y << std::endl;
		}
};

int main() {

	Demo obj;

	std::cout << "Enter value" << std::endl;

	std::cin >> obj;

	obj.printData();

	return 0;
}
