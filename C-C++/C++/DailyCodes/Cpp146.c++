#include<iostream>

class Demo {
	
	int x = 10;
	
	public:
	friend int operator++(Demo& obj) {

		return ++obj.x;
	}
};


int main() {

	Demo obj;

	std::cout << ++obj << std::endl;

	return 0;
}
