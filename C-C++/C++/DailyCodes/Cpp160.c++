#include<iostream>

class Parent {

	public:
		Parent() {

			std::cout << "Constructor parent" << std::endl;
		}

		~Parent() {
			
			std::cout << "Destructor parent" << std::endl;
		}
};

class Child : public Parent {

	public:
		Child() {
			
			std::cout << "Constructor child" << std::endl;
		}

		~Child() {
			
			std::cout << "Destructor child" << std::endl;
		}
};

int main() {

	Child *obj = new Child();
	delete obj;

	return 0;
}
