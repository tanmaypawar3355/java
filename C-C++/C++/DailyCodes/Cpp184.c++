#include<iostream>

class Parent {

	public:
		Parent() {

			std::cout << "Parent constructor" << std::endl;
		}

		virtual void getData(int x) {       //getData(int)
			
			std::cout << "Parent getData" << std::endl;
		}

		virtual void printData(float x) {    //printData(float)
			
			std::cout << "Parent printData" << std::endl;
		}
};

class Child : public Parent {

	public:
		Child() {
			
			std::cout << "Child constructor" << std::endl;
		}

		//overriding not allowed
		void getData(short int x) {    //getData(short int)
			
			std::cout << "Child getData" << std::endl;
		}
		
		//overriding
		void printData(float x) {      //printData(float)  
			
			std::cout << "Child printData" << std::endl;
		}
};

int main() {

	Parent *obj1 = new Child();
	obj1 -> getData(10);          // obj*.getData();
	obj1 -> printData(10.5f);     // obj*.printData();
				      
	Child obj1;
	Parent *obj2 = &obj1;
	obj2 -> getData(10);
	obj2 -> printData(10.5f);

	Child obj1;
	Parent& obj3 = obj1;
	obj3.getData(10);
	obj3.printData(10.5f);


	return 0;
}

