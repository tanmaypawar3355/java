#include<iostream>

class Parent {

	int x = 10;

	protected:
	int y = 20;

	public:
	int z = 30;

	Parent() {

		std::cout << "Parent constructor" << std::endl;
	}
};

class Child : protected Parent {

	public:

		Child() {
		
			std::cout << "Child constructor" << std::endl;
		}
};

int main() {

	Child obj;
		
	std::cout << obj.x << obj.y << obj.z << std::endl;

	return 0;
}
