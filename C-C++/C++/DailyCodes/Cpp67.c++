#include<iostream>


class Company {

	int countEmp = 5000;
	std::string name = "IBM";

	public :

	Company() {

		std::cout << "In company constructor" << std::endl;
	}

	void empInfo() {

		std::cout << countEmp << std::endl;
		std::cout << name << std::endl;
	}
};

class Employee {

	int empId = 10;
	float empSal = 95.00f;

	public :

	Employee() {

		std::cout << "In employee constructor" << std::endl;
	}

	void empInfo() {

		Company obj;

		std::cout << empId << std::endl;
		std::cout << empSal << std::endl;
		
		obj.empInfo();

		// std::cout << obj.countEmp << std::endl;  
		// std::cout << obj.name << std::endl;
	}
};


int main() {

	Employee *emp = new Employee();

	emp -> empInfo();
	return 0;
}

