#include<iostream>

class Demo {

	public:
		Demo() {

			std::cout << "In constr" << std::endl;
		}

		Demo(int x) {
			
			std::cout << "In para" << std::endl;
		}

		Demo(Demo &ref) {
			
			std::cout << "In copy" << std::endl;
		}
};

int main() {

	Demo obj1;
	Demo obj2;
	Demo obj3;

	Demo arr[] = {obj1,obj2,obj3};

	return 0;
}


			

