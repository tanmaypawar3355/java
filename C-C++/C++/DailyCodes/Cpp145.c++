#include<iostream>

class Demo {

        int x = 10;

        public:

        friend void* operator new(size_t size) {

                std::cout << "Here" << std::endl;

                void *ptr = malloc(size);

                //void *ptr = ::operator new(size);

                return ptr;
        }

	friend void operator delete(Demo *ptr) {

		free(ptr);
	}

        void getData() {

                std::cout << x << std::endl;
        }
};

int main() {

        Demo *obj = new Demo();

        obj -> getData();

	delete obj;

        return 0;
}
