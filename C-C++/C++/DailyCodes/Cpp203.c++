#include<iostream>

class Demo {

	public:

		Demo() {

			std::cout << "Demo constructor" << std::endl;
		}
};

class Child1 : public Demo {

	public:
		Child1() {
			
			std::cout << "Child1 constructor" << std::endl;
		}
};

class Child2 : public Demo {

	public:
		Child2() {
			
			std::cout << "Child2 constructor" << std::endl;
		}

};


class ChildBal : public Child1,public Child2 {

	public:
		ChildBal() {
   			std::cout << "ChildBal constructor" << std::endl;
		}
};

int main() {

	ChildBal obj;

	return 0;
}
