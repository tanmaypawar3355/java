#include<iostream>

class Demo {

	public:
		int x = 10;
		Demo() {

			std::cout << "In constr" << std::endl;
			std::cout << x << std::endl;
		}

		Demo(int x) {

			this -> x = x;
			std::cout << "In constr para" << std::endl;
			std::cout << x << std::endl;
			Demo();
		}

		~Demo() {
			
			std::cout << "Destructor" << std::endl;
		}
};

int main() {

	Demo obj(50);
	std::cout << "End main" << std::endl;

	return 0;
}
