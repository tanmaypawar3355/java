#include<iostream>

class Parent {

	int x = 10;

	public:

	Parent() {
		
		std::cout << "Parent constructor" << std::endl;
	}

	void getData() {
		
		std::cout << "Parent x = " << x << std::endl;
	}
};

class Child : public Parent {

	int y = 10;

	public:

	Child() {
		
		std::cout << "Child constructor" << std::endl;
	}

	void printData() {
		
		std::cout << "Child y = " << y << std::endl;
	}
};


int main() {

	Child obj;
	
	obj.getData();
	obj.printData();

	return 0;
}
