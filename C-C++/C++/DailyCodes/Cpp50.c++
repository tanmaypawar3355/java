#include<iostream>

int main() {

	int x = 10;
	int &y = x;
	int *ptr = &x;

	std::cout << sizeof(x) << std::endl;
	std::cout << sizeof(y) << std::endl;
	std::cout << sizeof(ptr) << std::endl;

	return 0;
}

