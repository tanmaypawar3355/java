#include<iostream>

class Parent {

	public:
	virtual void marry() = 0;

	virtual void property() {

		std::cout << "flat,car,gold" << std::endl;
	}
};

void Parent::marry() {

	std::cout << "Kriti" << std::endl;
	//Parent::marry();
}

class Child : public Parent {

	void marry() {

		std::cout << "Kiara" << std::endl;
	}
};

int main() {

	Parent *obj = new Child();

	obj -> marry();
	obj -> property();

	return 0;
}


