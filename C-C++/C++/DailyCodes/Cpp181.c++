#include<iostream>

class Parent {

	public:
		Parent() {

			std::cout << "Parent constructor" << std::endl;
		}

		virtual void getData() {
			
			std::cout << "Parent getData" << std::endl;
		}
};

class Child : public Parent {

	public:
		Child() {
			
			std::cout << "Child constructor" << std::endl;
		}

		void getData() {
			
			std::cout << "Child getData" << std::endl;
		}
};

int main() {

	Parent *obj = new Child();
	obj -> getData();

	return 0;
}

