#include<iostream>

template<class T>
class Template {

        T data;
        public:

        Template() {

                std::cout << "Template constructor" << std::endl;
        }
};

template<>
class Template<int> {

	int data;
	public:
		
		Template() {

			std::cout << "Int constructor" << std::endl;
		}
};

int main() {

	Template<char> obj1;
	Template<int> obj2;
	Template<float> obj3;

	return 0;
}

