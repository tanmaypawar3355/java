#include<stdio.h>
#include<string.h>
#include<stdlib.h>

struct Lang {

	char langName[10];
	char founder[20];

};

void main() {

	struct Lang *ptr = (struct Lang*)malloc(sizeof(struct Lang));

	strcpy(ptr -> langName,"C");
	strcpy(ptr -> founder,"Ritchie");

	printf("%s\n",ptr -> langName);
	printf("%s\n",ptr -> founder);
}
