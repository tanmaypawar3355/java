#include<iostream>

class Demo {

	public:
	int  x = 10;

	void getData() {

		std::cout << "In getData" << std::endl;
	}

	friend ostream& operator<<(const ostream& cout,const Demo& obj);
};

ostream& operator<<(const ostream& cout,const Demo& obj) {

	std::cout << obj.x << std::endl;
	//return cout;
}

int main() {

	Demo obj;

	std::cout << obj << std::endl;

	return 0;
}
