#include<iostream>

template<class T>
class Template {

        T data;
        public:

        Template() {

                std::cout << "In constructor" << std::endl;
        }
};

int main() {

	Template<int> obj1;
	Template<double> *obj2 = new Template<double>();
	Template<double> obj3;

	std::cout << sizeof(obj1) << std::endl;
	std::cout << sizeof(obj3) << std::endl;

	return 0;
}
