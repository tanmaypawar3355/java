#include<iostream>

class Parent {

	int x = 10;

	public:

	Parent() {
		
		std::cout << "Parent constructor" << std::endl;
	}

	virtual void getData() {
		
		std::cout << "Parent x = " << x << std::endl;
	}
};

class Child : public Parent {

	int x = 10;

	public:

	Child() {
		
		std::cout << "Child constructor" << std::endl;
	}

	void getData() {
		
		std::cout << "Child x = " << x << std::endl;
	}
};


int main() {

	Parent *obj = new Child();

	obj -> getData();

	return 0;
}
