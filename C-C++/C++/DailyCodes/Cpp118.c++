#include<iostream>

class Two;

class One {

	int x = 10;

	protected:
	int y = 20;

	public :	
		One() {
			
			std::cout << "One constructor" << std::endl;
		}

	private:
		void getData() const {

			std::cout << "x = " << x << std::endl;
			std::cout << "y = " << y << std::endl;
		}

		friend void accessData(const One& obj1,const Two& obj2);
};


class Two {

	int a = 10;

	protected:
	int b = 20;

	public :	
		Two() {
			
			std::cout << "Two constructor" << std::endl;
		}

	private:
		void getData() const {

			std::cout << "a = " << a << std::endl;
			std::cout << "b = " << b << std::endl;
		}

		friend void accessData(const One& obj1,const Two& obj2);
};

void accessData(const One& obj1,const Two& obj2) {
			
	obj1.getData();
	obj2.getData();
	
	/*
	std::cout << "x = " << obj1.x << std::endl;
	std::cout << "y = " << obj1.y << std::endl;
	std::cout << "a = " << obj2.a << std::endl;
	std::cout << "b = " << obj2.b << std::endl;
	*/
}


int main() {

	One obj1;
	Two obj2;

	accessData(obj1,obj2);

	return 0;
}

