#include<iostream>

class Parent {

	public:

		int *ptr = NULL;
		Parent() {

			ptr = new int[5];

			std::cout << "Parent constructor" << std::endl;
		}

		~Parent() {
			
			std::cout << "Parent destructor" << std::endl;
			delete []ptr;
		}
};

class Child : public Parent {

	public:
		Child() {

			std::cout << "Child constructor" << std::endl;
		}

		~Child() {
			
			std::cout << "Child destructor" << std::endl;
		}
};

int main() {

	Parent *obj = new Child();

	delete obj;

	return 0;
}
