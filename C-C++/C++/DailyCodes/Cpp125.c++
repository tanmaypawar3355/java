#include<iostream>

class Demo {

	int a = 10;

	public:

		Demo() {

		}
};


int main() {

	int x = 10;

	std::cout << x << std::endl;
	//operator<<(cout,x);          => function call
	//Prototype:
	//ostream& operator<<(ostream&,int);    => predefined

	Demo obj;
	
	std::cout << obj << std::endl;
	//operator<<(cout,obj);        => function call
	//Prototype:
	//ostream& operator<<(ostream&,Demo&)     => Not predefined
	
	return 0;
}
