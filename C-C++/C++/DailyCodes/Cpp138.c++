#include<iostream>

class Demo {

	int x;

	public:
	Demo(int x) {

		this -> x = x;
	}

	void getData() {

		std:: cout << x << std::endl;
	}

	void* operator new(size_t size) {

		void *ptr = malloc(size);
		//void *ptr = ::operator new(size);

		return ptr;
	}
};

int main() {

	Demo *obj = new Demo(50);
	obj -> getData();

	return 0;
}


