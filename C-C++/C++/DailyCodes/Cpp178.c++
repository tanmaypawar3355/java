#include<iostream>

class Parent {

	int x = 10;

	protected:
	int y = 20;

	public:
	int z = 30;

	void getData() {

		std::cout << "In getData" << std::endl;
	}
};

class Child : public Parent {

	using Parent::getData; // member function la private kela

	public:	
		using Parent::y; // Varchya protected y cha scope public kela

};

int main() {

	Child obj;

	std::cout << obj.y << obj.z << std::endl;

	obj.getData();

	return 0;
}
