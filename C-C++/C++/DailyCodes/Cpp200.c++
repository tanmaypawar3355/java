#include<iostream>

class Demo {

	public:

		virtual static void getData() {

			std::cout << "Demo::getData" << std::endl;
		}
};

class Child: public Demo {

	public:
		static void getData() {

			std::cout << "DemoChild::getData" << std::endl;
		}
};

int main() {
	
	Demo *obj = new Child();

	obj -> getData();

	return 0;
}
