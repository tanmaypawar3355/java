#include<iostream>

class Demo {

	int x;
	int y;

	public:
		
		Demo(int x,int y) {

			this -> x = x;
			this -> y = y;
		}

		int getX() const {

			return x;
		}

		int getY() const {

			return y;
		}

		friend int operator<(const Demo& obj1,const Demo& obj2) {

			return obj1.getX() < obj2.getY();
		}
};

int main() {

	Demo obj1(10,20);
	Demo obj2(40,50);

	std::cout << (obj1 < obj2) << std::endl;

	return 0;	
}
