#include<iostream>

class Employee {

	std::string empName = "Kanha";
	int empId = 255;

	public:

	Employee() {

		std::cout << "Emp constructor" << std::endl;
	}

	void getInfo() {

		std::cout << empName << "  " << empId << std::endl;
	}

	~Employee() {
		
		std::cout << "Emp destructor" << std::endl;
	}
};

class Company {

	std::string cName = "Veritas";
	int strEmp = 1000;

	Employee obj;          // scope is instance

	public:

	Company (std::string cName,int strEmp = 0) {

		std::cout << "Cmp constructor" << std::endl;

		this -> cName = cName;
		this -> strEmp = strEmp;
	}

	void getInfo() {

		std::cout << cName << " " << strEmp << std::endl;
		obj.getInfo();
		//this -> obj.getInfo();
	}

	~Company() {

		std::cout << "Cmp destructor" << std::endl;
	}
};

int main() {

	Company obj("Pubmatic",5000);
	obj.getInfo();

	return 0;
}



