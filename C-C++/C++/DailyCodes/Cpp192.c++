#include<iostream>

void add(int x, int y) {

	std::cout << x + y << std::endl;
}
void sub(int x, int y) {

	std::cout << x - y << std::endl;
}

void mul(int x, int y) {

	std::cout << x * y << std::endl;
}

int main() {
	
	std::cout << "add" << std::endl;
	std::cout << "sub" << std::endl;
	std::cout << "mul" << std::endl;
	
	int ch;
	std::cout << "Enter your choice" << std::endl;
	std::cin >> ch;

	// void *funptr (int,int) = {add,sub,mul};
	void (*funPtr) (int,int) = NULL;

	switch(ch) {

		case 1 :
			funPtr = add;
			break;

		case 2 :
			funPtr = sub;
			break;

		case 3 :
			funPtr = mul;
			break;
	}

	funPtr(10,20);

	return 0;
}

	
