#include<iostream>

class Demo {

	int x = 10;
	static int y;

	public :
	void fun() {

		std::cout << x << std::endl;
		std::cout << y << std::endl;
	}
};

int main() {

	Demo obj;
	obj.fun();
	return 0;
}
