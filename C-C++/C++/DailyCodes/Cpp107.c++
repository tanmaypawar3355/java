#include<iostream>

class Demo {

	public:
		int x = 10;

		Demo() {
				
			this -> x = 80;
			std::cout << "In constr" << std::endl;
		}

		void getData() {

			std::cout << x << std::endl;
		}
};

int main() {

	const Demo obj;
	
	std::cout << obj.x << std::endl;

	return 0;
}

