#include<iostream>

class Demo {

	public:
		Demo() {

			Demo(this);
			std::cout << "No-args" << std::endl;
		}

		Demo(int x) {

			Demo();
			std::cout << "Para" << std::endl;
		}

		Demo(Demo &xyz) {
			
			std::cout << "Copy" << std::endl;
		}
};

int main() {

	Demo obj1(10);
	Demo obj3(obj1);

	return 0;
}
