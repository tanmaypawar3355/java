#include<iostream>

class Demo {
	
	int x = 10;
	
	public:
	int getX() {
	
		return ++x;
	}
};

int operator++(Demo& obj) {

	obj.getX();
}

int main() {

	Demo obj;

	std::cout << ++obj << std::endl;

	return 0;
}
