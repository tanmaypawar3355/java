#include<iostream>

class Demo {

	public:
		Demo() {
			
			std::cout << "No-args constructor" << std::endl;
		}

		Demo(int x) {
			
			std::cout << "Para constructor" << std::endl;
			std::cout << x << std::endl;
		}

		Demo(Demo &xyz) {
			
			std::cout << "Copy constructor" << std::endl;
		}
};

int main() {

	Demo obj1;
	
	Demo obj2(10);

	Demo obj3(obj1);

	return 0;
}
