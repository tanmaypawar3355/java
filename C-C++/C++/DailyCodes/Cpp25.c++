#include<iostream>

int main() {

	int x = 10;  // Copy initialization

	int y(20);   // Direct initialization
		     

	std::cout << x << std::endl;
	std::cout << y << std::endl;
	
	
	int z{30};   // List initialization
	
	std::cout << z << std::endl;

	return 0;
}
