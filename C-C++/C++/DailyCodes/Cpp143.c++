#include<iostream>

class Demo {

	int x = 10;

	public:
	Demo() {

		std::cout << "Constructor" << std::endl;
	}

	~Demo() {

		std::cout << "Destructor" << std::endl;
	}
};

int main() {

	std::cout << "Start main" << std::endl;

	{
		Demo obj;
	}
	
	std::cout << "End main" << std::endl;

	return 0;
}


