#include<iostream>

class Demo {
	
	public :
		int *ptr = NULL;

		Demo() {

			ptr = new int();
			*ptr = 50;
			std::cout << *ptr << std::endl;
			std::cout << "constr" << std::endl;
		}

		~Demo() {

			delete ptr;
			std::cout << "In destructor" << std::endl;
		}
};

int main() {

	Demo obj;

	return 0;
}
