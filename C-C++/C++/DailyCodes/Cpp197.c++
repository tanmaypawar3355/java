#include<iostream>

class Demo {

	public:
		virtual void fun1() = 0;    //pure virtual function
		virtual void fun2() = 0;    //pure virtual function
};

class DemoChild1 : public Demo {

	public:
		void fun1() {

		
		}
};

class DemoChild2 : public Demo {

	public:
		void fun2() {
			

		}
};

int main() {

	DemoChild1 *obj1 = new DemoChild1();
	DemoChild2 *obj2 = new DemoChild2();

	return 0;
}
