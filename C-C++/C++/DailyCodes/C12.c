#include<stdio.h>

void fun(int arr[],int);

void main() {

	int arr[] = {10,20,30,40,50};

	int size = sizeof(arr) / sizeof(int);

	fun(arr,size);

	for(int i = 0 ; i < size ; i++) {

		printf("| %d |",arr[i]);
	}
}

void fun(int arr[],int size){

	for(int i = 0 ; i < size ; i++) {

		arr[i] = arr[i] + 10;
	}
}

