#include<iostream>

class Demo {

	public:
		int x = 10;

		Demo() {
			
			std::cout << "No-args" << std::endl;
		}

		Demo(int x) {
			
			this();		
			std::cout << "Para" << std::endl;
		}

		Demo(Demo &xyz) {
			
			std::cout << "Copy" << std::endl;
		}
};

int main() {

	Demo obj1(10);

	return 0;
}
