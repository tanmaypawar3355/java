#include<iostream>

class Parent {

	public:
		Parent() {

			std::cout << "Constructor parent" << std::endl;
		}

		~Parent() {
			
			std::cout << "Destructor parent" << std::endl;
		}
};

class Child : public Parent {

	public:
		Child() {
			
			std::cout << "Constructor child" << std::endl;
		}

		~Child() {
			
			std::cout << "Destructor child" << std::endl;
		}

		void* operator new(size_t size) {

			std::cout << "new child" << std::endl;
			void* ptr = malloc(size);
			return ptr;
		}

		void operator delete(void *ptr) {

			std::cout << "delete child" << std::endl;
			free(ptr);
		}
};

int main() {

	Child *obj1 = new Child();
	delete obj1;

	return 0;
}
