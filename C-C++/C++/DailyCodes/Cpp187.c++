#include<iostream>

class Parent {

	public :

		virtual void getData() final {

			std::cout << "Parent getData" << std::endl;
		}
};

class Child final : public Parent {

	public:

		void getData() {
			
			std::cout << "Child getData" << std::endl;
		}
};

class Child2 : public Child {


};

int main() {

	Parent *obj = new Child();
	obj -> getData();

	return 0;
}


