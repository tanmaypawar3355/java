#include<iostream>

class Demo {

	int x = 10;
	int y = 10;

	public:
	
	int getData() const {

		return x;
	}
};

std::ostream& operator<<(std::ostream& out,const Demo& obj) {

	out << obj.getData();
	// ostream& operator<<(ostream& out,int obj.x);
	return out;
}

int main() {

	Demo obj1;

	std::cout << obj1 << std::endl;

	return 0;
}

