#include<iostream>

class Demo {

	public:

		int x = 10;

		void getData() {

			std::cout << "Demo::getData" << std::endl;
		}
};

class Child1 : virtual public Demo {

};

class Child2 : virtual public Demo {

};


class ChildBal : public Child1,public Child2 {

};

int main() {

	ChildBal obj = ChildBal();

	obj.getData();

	return 0;
}
