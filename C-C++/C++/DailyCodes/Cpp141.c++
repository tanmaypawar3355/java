#include<iostream>

class Demo {

	int x = 10;

	public:
		Demo(int x) {

			this -> x = x;
		}

		void getData() const {

			std::cout << x;
		}
};




std::ostream& operator<<(std::ostream& out,const Demo& obj) {

	obj.getData();
	return out;
			
}


int main() {

	Demo obj(50);

	std::cout << obj << std::endl;

	return 0;
}
