#include<iostream>

class Player {
	
	public:
	int jerNo;
	std::string name;

	void info() {

		std::cout << this << std::endl;
		std::cout << this-> jerNo << std::endl;   // (*this).x
		std::cout << this-> name << std::endl;   // (*this).y
	}
};

int main() {

	Player obj {18,"Virat"};
	obj.info();

	// Player obj2 = new Player() {7,"MSD"};
	
	Player *obj2 = new Player();
	obj2 -> jerNo = 7;
	obj2 -> name = "MSD";

	obj2->info();
	return 0;
}
