#include<iostream>

class Parent {

	int x = 10;

	protected:
	int y = 20;

	public:
	int z = 30;

	void getData() {

		std::cout << x << y << z << std::endl;
	}
};

class Child : public Parent {           // This is privare inheritance
				 
	public:
	void getInfo() {

		getData();
	}
};

int main() {

	Child obj;
	obj.getInfo();
	
	return 0;
}

