#include<stdio.h>
#include<stdlib.h>

struct BSTreeDemo {

	int data;
	struct BSTreeDemo *left;
	struct BSTreeDemo *right;
};

struct BSTreeDemo* createNode(struct BSTreeDemo *root,int data) {

	struct BSTreeDemo *newNode = malloc(sizeof(struct BSTreeDemo));

	if(root == NULL) {

		newNode -> data = data;
		newNode -> left = NULL;
		newNode -> right = NULL;
		return newNode;
	}

	if(root -> data > data) {

		root -> left = createNode(root -> left,data);

	}else{

		root -> right = createNode(root -> right,data);
	}

	return root;
}

void printTree(struct BSTreeDemo *root) {

	if(root == NULL) {
		return;
	}

	printTree(root -> left);
	printf("%d  ",root -> data);
	printTree(root -> right);
}

int flag = 0;

int searchInBST(struct BSTreeDemo *root,int data) {

	if(root == NULL) {
		return;
	}

	if(root -> data == data) {
		printf("found");	
		flag = 1;
		return 0;
	}

	if(root -> data > data) {
		searchInBST(root -> left,data);
	}else{
		searchInBST(root -> right,data);
	}
}

void insertInBST(struct BSTreeDemo *root,int data) {

	if(root == NULL) {
		
		struct BSTreeDemo *newNode = malloc(sizeof(struct BSTreeDemo));
		root = newNode;
		newNode -> data = data;
		newNode -> left = NULL;
		newNode -> right = NULL;
		return;
	}

	if(data > root -> data) {

		insertInBST(root -> right,data);
	
	}else{
		insertInBST(root -> left,data);
	}
}



void main() {

	struct BStreeDemo *root = NULL;

	int num,data;
	printf("Enter how many nodes you want\n");
	scanf("%d",&num);

	for(int i = 0 ; i < num ; i++) {

		scanf("%d",&data);
		root = createNode(root,data);	
		printf("%p\n",root);
	}

	printTree(root);
	int search;
	printf("\nEnter which node you want\n");
	scanf("%d",&search);
	searchInBST(root,search);
	if(flag == 0) {
		printf("Not found");
	}

	int add;
	printf("\nWhich node you want to add\n");
	scanf("%d",&add);

	insertInBST(root,add);

	printTree(root);
}
