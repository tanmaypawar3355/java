#include <stdio.h>

int equilibriumArr(int arr[] ,int size){
    
    int size1 = size/2;

    int sum1 = 0;
    for(int i = 0 ; i < size1 ; i++){
        sum1 = sum1 + arr[i];
    }
    
    int sum2 = 0;
    for(int j = size1+1 ; j < size ; j++){
        sum2 = sum2 + arr[j];
    }
    
    if(sum1 == sum2){
        return 0;
    }else{
        return -1;
    }
    }

void main(){

        int arr[7] = {-7, 1, 5, 2, -4, 3, 0};
        
        int size = sizeof(arr)/sizeof(int);
        
        int ret = equilibriumArr(arr,size);
        
        if(ret == 0){
            printf("Their is equilibrium index\n");
        }else{
            printf("There is no such index\n");
        }
        }

/*

   Q2 Equilibrium index of the array
- You are given an array A of integers of size N.
- Your task is to find the equilibrium index of the given array
- The equilibrium index of an array is an index such that the sum of
elements at lower indexes are equal to the sum of elements at higher indexes.
- If there are no elements that are at lower indexes or at higher indexes,
then the corresponding sum of elements is considered as 0.
Note:
Array indexing starts from 0.
If there is no equilibrium index then return -1.
If there is more than one equilibrium index then return the minimum index.

Problem Constraints
1 <= N <= 105
-105 <= A[i] <= 105

Input 1:
A = [-7, 1, 5, 2, -4, 3, 0]
Output 1:
3
Explanation 1:
i      Sum of elements at lower indexes    Sum of elements at higher indexes
0                     0                                   7
1                    -7                                   6
2                    -6                                   1
3                    -1					 -1
4 		      1					  3
5 		     -3 				  0
6 		      0					  0

- 3 is an equilibrium index, because:
    A[0] + A[1] + A[2] = A[4] + A[5] + A[6]


Input 2:
A = [1, 2, 3]
Output 2:
-1
Explanation 2:
i   Sum of elements at lower indexes    Sum of elements at higher indexes
0 		0 					5
1 		1					3
2 		3					0
Thus, there is no such index
*/
