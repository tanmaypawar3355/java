#include<stdio.h>

int productArr(int arr[],int size){

	int pArr[size];

	for(int i = 0 ; i < size ; i++){
		int product = 1;
		for(int j = 0 ; j < size ; j++){

			if(j != i){

				product = product * arr[j];

			}}

				pArr[i] = product;
	}
	for(int l = 0 ; l < size ; l++){
                printf("|%d|",arr[l]);
	}

		printf("\n");

	for(int k = 0 ; k < size ; k++){
		printf("|%d|",pArr[k]);
	}

		printf("\n");
}

void main(){

	int size = 5;

	int arr[5] = {1,2,3,4,5};

	productArr(arr,size);


}


/* 
Q3. Product Array puzzle
- Given an array of integers A, find and return the product array of the same size
where the ith element of the product array will be equal to the product of all the elements
divided by the ith element of the array.
Note: It is always possible to form the product array with integer (32-bit) values.
Solve it without using the division operator.
- Return the product array.
Constraints
2 <= length of the array <= 1000
1 <= A[i] <= 10
Input 1:
A = [1, 2, 3, 4, 5]
Output 1:
[120, 60, 40, 30, 24]
Input 2:
A = [5, 1, 10, 1]
Output 2:
[10, 50, 5, 50]

*/
