#include<stdio.h>

int leaderArr(int arr[],int size){
    
    int count = 0;
    int maxEle = arr[size-1];
    
    for(int i = size-1 ; i > 0 ; i--){
        if(arr[i] > maxEle){
            maxEle = arr[i];
            count++;
        }}
        printf("Count = %d\n",count);
        
        int leadArr[count+1];
        
        leadArr[0] = arr[size-1];
        int num = 0;
        for(int i = size-1; i > 0 ; i--){
            if(arr[i] > leadArr[num]){
                num++;
                leadArr[num] = arr[i];
            }}
            for(int k = 0 ; k <= count ; k++){
                printf("|%d|",leadArr[k]);
            }}
      
void main(){

        int arr[] = {16,17,4,3,5,2};
        
        int size = sizeof(arr)/sizeof(int);

        leaderArr(arr,size);
}

/*

   Q2. Leaders in Array :
Problem Description :

	- Given an integer array A containing N distinct integers, you have to find
	  all the leaders in array A.
	- An element is a leader if it is strictly greater than all the elements to
	  it's on the right side.

NOTE: The rightmost element is always a leader.
NOTE: Ordering in the output doesn't matter.

Problem Constraints
1 <= N <= 105
1 <= A[i] <= 108

Example Input : A = [16, 17, 4, 3, 5, 2]
Example Output : [17, 2, 5]

Example Explanation
- Element 17 is strictly greater than all the elements on the right side to it.
- Element 2 is strictly greater than all the elements on the right side to it.
- Element 5 is strictly greater than all the elements on the right side to it.
- So we will return this three elements i.e [17, 2, 5], we can also return
  [2, 5, 17] or [5, 2, 17] or any other ordering.

*/
