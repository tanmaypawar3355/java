#include<stdio.h>

int reverseArray(int arr[],int N,int B,int C){

        int count = N / 2;
        //printf("%d\n",count);

        while(count){

                int temp = arr[B];
                arr[B] = arr[C];
                arr[C] = temp;
		
		B++;
		C--;
                count--;
        }
}





void main(){
        int arr[] = {1,2,3,4,5,6,7,8,9};

        int N = sizeof(arr)/sizeof(int);

        int B = 0;
        int C = N-1;

        int ret = reverseArray(arr,N,B,C);

        for(int i = 0 ; i < N ; i++){
                printf("|%d|",arr[i]);
        }
}

/*
 
Q4. Reverse the array
Problem Description :
- You are given a constant array A.
- You are required to return another array which is the reversed form of the
input array.
- Return an integer array.
Problem Constraints :
1 <= A.size() <= 10000
1 <= A[i] <= 10000
Example Input :
Input 1:
A = [1,2,3,2,1]
Output 1:
[1,2,3,2,1]
Input 2:
A = [1,1,10]
Output 2:
[10,1,1]
Example Explanation :
Explanation 1:
- Reversed form of input array is same as original array
Explanation 2:
- Reverse of [1,1,10] is [10,1,1]


*/
