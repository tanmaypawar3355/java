#include<stdio.h>

int arr1[5];

void main(){

	int arr[] = {1,2,3,4,5};
	int N = sizeof(arr)/sizeof(int);
	arr1[0] = arr[0];

	for(int i = 1 ; i < N ; i++){

		arr1[i] = arr[i] + arr1[i-1];
	}
	
	for(int i = 0 ; i < N ; i++){

		printf("|%d|",arr1[i]);
	}
}

/*

   Q17. In place prefix sum
Problem Description
- Given an array A of N integers.
- Construct the prefix sum of the array in the given array itself.
- Return an array of integers denoting the prefix sum of the given array.
Example Input
Input 1:
A = [1, 2, 3, 4, 5]
Input 2:
A = [4, 3, 2]
Example Output
Output 1:
[1, 3, 6, 10, 15]
Output 2:
[4, 7, 9]
Example Explanation
Explanation 1:
The prefix sum array of [1, 2, 3, 4, 5] is [1, 3, 6, 10, 15].
Explanation 2:
The prefix sum array of [4, 3, 2] is [4, 7, 9].

*/



