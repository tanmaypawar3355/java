#include<stdio.h>

int reverseRange(int arr[],int N,int B,int C){
	
	int count = (B + C) / 3;
	printf("%d\n",count);

	while(count){

		int temp = arr[B]; 
		arr[B] = arr[C];
		arr[C] = temp;
	
		B++;
		C--;
		count--;
	}
}   
			
void main(){
	int arr[] = {1,2,3,4,5,6,7,8,9};

	int N = sizeof(arr)/sizeof(int);

	int B = 2;
	int C = 6;

	int ret = reverseRange(arr,N,B,C);
	
	for(int i = 0 ; i < N ; i++){
		printf("|%d|",arr[i]);
	}
}

/*
 

Q3. Reverse in a range
Problem Description :
- Given an array A of N integers.
- Also given are two integers B and C.
- Reverse the array A in the given range [B, C]
- Return the array A after reversing in the given range.
Problem Constraints :
1 <= N <= 105
1 <= A[i] <= 109
0 <= B <= C <= N - 1
Example Input
Input 1:
A = [1, 2, 3, 4]
B = 2
C = 3
Output 1:
[1, 2, 4, 3]
Input 2:
A = [2, 5, 6]
B = 0
C = 2
Output 2:
[6, 5, 2]
Example Explanation :
Explanation 1:
We reverse the subarray [3, 4].
Explanation 2:
We reverse the entire array [2, 5, 6].

*/
