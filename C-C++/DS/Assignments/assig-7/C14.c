#include<stdio.h>

int row = 3;
int column = 3;

int sameMatrices(int arr1[][3],int arr2[][3]){

	for(int i = 0 ; i < row ; i++){

		for(int j = 0 ; j < column ; ){

			if(arr1[i][j] == arr2[i][j]){

				j++;
			
			}else{
			       	return 0;
			}
		}
	}
	return 1;
}

void main(){

	int arr1[][3] = {1,2,3,4,5,6,7,8,9};
	int arr2[][3] = {1,2,3,4,5,6,7,8,9};

	int ret = sameMatrices(arr1,arr2);

	printf("%d\n",ret);

}

/*
 


   Q14. Are Matrices the same?
Problem Description :
- You are given two matrices A & B of equal dimensions and you have to
check whether the two matrices are equal or not.
- Return 1 if both matrices are equal or return 0.
NOTE: Both matrices are equal if A[i][j] == B[i][j] for all i and j
in the given range.
Example Input :
Input 1:
A = [[1, 2, 3],[4, 5, 6],[7, 8, 9]]
B = [[1, 2, 3],[4, 5, 6],[7, 8, 9]]
Output 1:
1
Input 2:
A = [[1, 2, 3],[4, 5, 6],[7, 8, 9]]
B = [[1, 2, 3],[7, 8, 9],[4, 5, 6]]
Output 2:
0
Example Explanation:
Explanation 1:
- All the elements of both matrices are equal at respective positions.
Explanation 2:
- All the elements of both matrices are not equal at their respective positions.

*/

