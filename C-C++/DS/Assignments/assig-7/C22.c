#include<stdio.h>
#include<string.h>

void main(){

	char *str = "ABCGAG";

	int N = strlen(str); 
	
	int count = 0;

	for(int i = 0 ; i <= N ; i++){

		if(str[i] == 'A'){

			for(int j = i+1 ; j <= N ; j++){

				if(str[j] == 'G'){

					count++;
				}
			}
		}
	}
	printf("%d\n",count);
}

/*


   Q22. Special Subsequences "AG"
Problem Description:
- You have given a string A having Uppercase English letters.
- You have to find how many times the subsequence "AG" is there in the given
string.
- Return the count of(i,j) such that,
a)i<j
b) s[i] = 'A' and s[j] = 'G'
Example Input
Input 1:
A = "ABCGAG"
Input 2:
A = "GAB"
Example Output
Output 1:
3
Output 2:
0
Example Explanation
Explanation 1:
- Subsequence "AG" is 3 times in a given string
Explanation 2:
- There is no subsequence "AG" in the given string.


*/
