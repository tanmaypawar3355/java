#include<stdio.h>

void main(){

	int arr[] = {2,5,1,4,8,0,8,1,3,8};

	int N = sizeof(arr)/sizeof(int);

	int count = 0;

	for(int i = 0 ; i < N ; i++){

		for(int j = 0 ; j < N ; j++){

			if(arr[j] > arr[i]){
				
				count++;
				break;
			}
		}
	}
	printf("Count = %d\n",count);
}

/*
 

Q6.
Given an Integer array of size of N.
Count the number of elements having at least 1 element greater than itself.
Input:
int arr[] = {2,5,1,4,8,0,8,1,3,8};
Output:
7

*/
