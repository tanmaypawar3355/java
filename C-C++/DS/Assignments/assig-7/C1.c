#include<stdio.h>


void main(){

	int arr[] = {3,1,2};

	int N = sizeof(arr)/sizeof(int);
	
	int count = 0;

	for(int i = 0 ; i < N ; i++){

		for(int j = 0 ; j < N ; j++){

			if(arr[i] < arr[j]){
			count++;
			break;
			}
		}
	}
	printf("%d\n",count);
}

/*


   Q1. Count of elements
	Problem Description:
	- Given an array A of N integers.
	- Count the number of elements that have at least 1 element greater than itself.

	Example Input
	Input 1: A = [3, 1, 2]
  	Output: 2
	Explanation:- The elements that have at least 1 element greater than itself are 1 and 2

	Input 2: A = [5, 5, 3]
	Output: 1
	Explanation:- The element that has at least 1 element greater than itself is 3.


*/


