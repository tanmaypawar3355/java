#include<stdio.h>

void main(){

        int row = 3;
        int col = 4;
        int arr[3][4] = {1,2,3,4,5,6,7,8,9,2,3,4};

        int N = sizeof(arr)/sizeof(int);
        int arr1[row];

        for(int i = 0 ; i < row ; i++){

                int sum = 0;

                for(int j = 0 ; j < col ; j++){

                        sum = sum + arr[i][j];
                }
                arr1[i] = sum;
        }


        for(int k = 0 ; k < row ; k++){

                printf("|%d|",arr1[k]);
        }
	printf("\n");
}

/*


Q8: Row sum
Problem Description :
- You are given a 2D integer matrix A, and return a 1D integer array containing
row-wise sums of the original matrix.
- Return an array containing row-wise sums of the original matrix.
Input 1:
[1,2,3,4]
[5,6,7,8]
[9,2,3,4]
Output 1:
[10,26,18]
Explanation :
Row 1 = 1+2+3+4 = 10
Row 2 = 5+6+7+8 = 26
Row 3 = 9+2+3+4 = 18

*/
