#include<stdio.h>

void main(){

	int row = 3;
	int col = 4;
	int arr[3][4] = {{1,2,3,4},{5,6,7,8},{9,2,3,4}};
	
	int N = sizeof(arr)/sizeof(int);
	int arr1[col];

	for(int i = 0 ; i < col  ; i++){

		int sum = 0;
				
		for(int j = 0 ; j < row ; j++){

			sum = sum + arr[j][i];
		}
		arr1[i] = sum;
	}
	

	for(int k = 0 ; k < col ; k++){

		printf("|%d|",arr1[k]);
	}
	printf("\n");
}

/*
 
Q7. Column Sum
Problem Description :
- You are given a 2D integer matrix A, and return a 1D integer array containing
column-wise sums of the original matrix.
- Return an array containing column-wise sums of the original matrix.
Input :
[1,2,3,4]
[5,6,7,8]
[9,2,3,4]
Output :
{15,10,13,16}
Example Explanation
Column 1 = 1+5+9 = 15
Column 2 = 2+6+2 = 10
Column 3 = 3+7+3 = 13
Column 4 = 4+8+4 = 16

*/
