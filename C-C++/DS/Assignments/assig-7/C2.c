#include<stdio.h>

int goodPair(int arr[],int N,int B){

	for(int i = 0 ; i < N ; i++){

		for(int j = 0 ; j < N ; j++){

			if(arr[i] + arr[j] == B){

				return 1;
			}
		}
	}
}

void main(){

        int arr[] = {1,2,3,4};

        int N = sizeof(arr)/sizeof(int);

        int B = 7;

	int ret = goodPair(arr,N,B);

	if(ret == 1){
		printf("Array has a good pair\n");
	}else{
		printf("Array do not have a good pair\n");
	}}


/*
 

Q2. Good Pair
Problem Description :
	- Given an array A and an integer B.
	- A pair(i, j) in the array is a good pair if i != j and (A[i] + A[j] == B).
	- Check if any good pair exist or not.
	- Return 1 if good pair exist otherwise return 0.
	
	Example Input
	Input 1: A = [1,2,3,4]
	B = 7
	Output 1: 1
	
	Input 2: A = [1,2,4]
	B = 4
	Output 2: 0

	Input 3: A = [1,2,2]
	B = 4
	Output 3: 1

	Example Explanation :
	Explanation 1:
	(i,j) = (3,4)
	
	Explanation 2:
	No pair has a sum equal to 4.

	Explanation 3:
	(i,j) = (2,3)


*/


