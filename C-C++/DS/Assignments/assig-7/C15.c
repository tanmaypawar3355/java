#include<stdio.h>

int arr3[3][3];

void main(){

	int arr1[][3] = {1,2,3,4,5,6,7,8,9};

	int arr2[][3] = {9,8,7,6,5,4,3,2,1};

	for(int i = 0 ; i < 3 ; i++){

                for(int j = 0 ; j < 3; j++){

                        arr3[i][j] = arr1[i][j] + arr2[i][j];
       				printf("|%d|",arr3[i][j]);

                }
                printf("\n");
        }
}
/*
 

   Q15. Add the matrices
Problem Description:
- You are given two matrices A & B of the same size, you have to return
another matrix which is the sum of A and B.
Example Input :
Input :
A = [[1, 2, 3],
[4, 5, 6],
[7, 8, 9]]
B = [[9, 8, 7],
[6, 5, 4],
[3, 2, 1]]
Output :
[[10, 10, 10],
[10, 10, 10],
[10, 10, 10]]
Example Explanation:
A + B = [[1+9, 2+8, 3+7],[4+6, 5+5, 6+4],[7+3, 8+2, 9+1]] = [[10, 10, 10],
[10, 10, 10], [10, 10, 10]].


*/

