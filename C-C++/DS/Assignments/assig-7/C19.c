#include<stdio.h>

void main(){

	int arr[] = {1,2,3,4,5};

	int N = sizeof(arr)/sizeof(arr[0]);

	int pArr[N];

	for(int i = 0 ; i < N ; i++){

		int product = 1;

		for(int j = 0 ; j < N ; j++){

			if(i != j){

				product = product * arr[j];
			}
			pArr[i] = product;
		}
		printf("|%d|",pArr[i]);
	}
}

/*

Q19. Product Array puzzle
- Given an array of integers A, find and return the product array of the
same size where the ith element of the product array will be equal to the product
of all the elements divided by the ith element of the array.
Note: It is always possible to form the product array with integer (32-bit) values.
Solve it without using the division operator.
- Return the product array.
Input 1:
A = [1, 2, 3, 4, 5]
Output 1:
[120, 60, 40, 30, 24]
Input 2:
A = [5, 1, 10, 1]
Output 2:
[10, 50, 5, 50]


*/
