#include<stdio.h>

void main(){

        int arr[] = {16,17,4,3,5,2};

        int N = sizeof(arr)/sizeof(int);

        for(int i = 0 ; i < N ; i++){

                int num = arr[i];

                for(int j = i + 1 ; j < N ; j++){

                        if(num < arr[j]){
                                printf("%d ",arr[j]);
                        }
                        break;
                }
        }
        printf("%d ",arr[N-1]);
}

/*
 
   Q23. Leaders in Array:
Problem Description:
- Given an integer array A containing N distinct integers, you have to find
all the leaders in array A.
- An element is a leader if it is strictly greater than all the elements to
its right side.
NOTE: The rightmost element is always a leader.
NOTE: Ordering in the output doesn't matter.
Example Input
A = [16, 17, 4, 3, 5, 2]
Example Output
[17, 2, 5]
Example Explanation
- Element 17 is strictly greater than all the elements on the right side to it.
- Element 2 is strictly greater than all the elements on the right side to it.
- Element 5 is strictly greater than all the elements on the right side to it.
- So we will return these three elements i.e [17, 2, 5], we can also return
[2, 5, 17] or [5, 2, 17] or any other ordering.

*/
