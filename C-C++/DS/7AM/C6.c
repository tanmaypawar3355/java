#include<stdio.h>
#include <stdlib.h>

typedef struct Node{

    int data;
    struct Node *next;
}Node;

Node *head1 = NULL;
Node *head2 = NULL;

Node *createNode(){
    Node *newNode = (Node*)malloc(sizeof(Node));

    printf("Enter data\n");
    scanf("%d",&newNode -> data);

    newNode -> next = NULL;
    return newNode;
}

void addNode(Node **head){
    Node *newNode = createNode();

    if(*head == NULL){
        *head = newNode;

    }else{
        Node *temp = *head;
        while(temp -> next != NULL){
            temp = temp -> next;
        }
        temp -> next = newNode;
    }}

void printLL(Node *head){

        Node *temp = head;
        while(temp -> next != NULL){
            printf("|%d|->",temp -> data);
            temp = temp -> next;
    }
    printf("|%d|",temp -> data);
}

int countNode(){
        int count = 0;
        Node *temp = head2;

        while(temp -> next != NULL){
                count++;
                temp = temp -> next;
        }
        return count;
}

void concatNLL(int num){

        //int count = countNode();

	Node *temp1 = head1;
    	while(temp1 -> next != NULL){
        	temp1 = temp1 -> next;
   	}
    
    	int val = countNode(head2) - num;
	
        Node *temp2 = head2;
	
	while(val){
                temp2 = temp2 -> next;
                val--;
        }

    temp1 -> next = temp2; 
}

void main(){
    int nCount1;

    printf("Enter Node count\n");
    scanf("%d",&nCount1);

    for(int i = 1 ; i <= nCount1 ; i++){
        addNode(&head1);
    }

    int nCount2;

    printf("Enter Node count\n");
    scanf("%d",&nCount2);

    for(int i = 1 ; i <= nCount2 ; i++){
        addNode(&head2);
    }

    //printLL(head1);
    //printLL(head2);
    int num = 2;
    concatNLL(num);
    printLL(head1);

}
