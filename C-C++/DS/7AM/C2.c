#include<stdio.h>
#include<stdlib.h>

typedef struct Node {

        int data;
        struct Node *next;

}Node;

Node *head1 = NULL;
Node *head2 = NULL;

Node *createNode(){

        Node* newNode = (Node*)malloc(sizeof(Node));

        printf("Enter the data\n");
        scanf("%d",&newNode -> data);

        newNode -> next = NULL;

        return newNode;

}

void addNode(Node **head){

        Node *newNode = createNode();

        if(*head == NULL){
                *head = newNode;

        }else{
                Node *temp = *head;
        while(temp -> next != NULL){
                temp = temp -> next;
        }

        temp -> next = newNode;
        }}

int concatLL(){

	Node *temp1 = head2;

	while(temp1 -> next -> next != NULL){
		temp1 = temp1 -> next;
	}

	Node *temp2 = head1;
	while(temp2 -> next != NULL){
		temp2 = temp2 ->  next;
	}
	temp2 -> next = temp1;
}

void  printSLL(Node *head){

        Node *temp = head;

        while(temp -> next != NULL){
                printf("|%d|->",temp -> data);
                temp = temp -> next;
        }

                printf("|%d|\n",temp -> data);
}

void main(){
        int nCount;

        printf("Enter node count(LL1)\n");
        scanf("%d",&nCount);

        for(int i = 1 ; i <= nCount ; i++){
                addNode(&head1);
        }

        printf("Enter node count(LL1)\n");
        scanf("%d",&nCount);

        for(int i = 1 ; i <= nCount ; i++){
                addNode(&head2);
        }

        printSLL(head1);
        printSLL(head2);

	concatLL();
	printSLL(head1);
	
}
