#include<stdio.h>
#include<stdlib.h>

typedef struct Node {

	int data;
	struct Node *next;

}Node;

Node *head1 = NULL;
Node *head2 = NULL;

Node *createNode(){

	Node* newNode = (Node*)malloc(sizeof(Node));

	printf("Enter the data\n");
	scanf("%d",&newNode -> data);

	newNode -> next = NULL;

	return newNode;

}

void addNode(Node **head){

	Node *newNode = createNode();
	
	if(*head == NULL){
		*head = newNode;
	
	}else{
		Node *temp = *head;
		while(temp -> next != NULL){
			temp = temp -> next;
	}

			temp -> next = newNode;
	}}

void  printSLL(Node *head){

	Node *temp = head;

	while(temp -> next != NULL){
		printf("|%d|->",temp -> data);
		temp = temp -> next;
	}
	
		printf("|%d|\n",temp -> data);
}



void main(){
	int nCount;

	printf("Enter node count(LL1)\n");
	scanf("%d",&nCount);

	for(int i = 1 ; i <= nCount ; i++){
		addNode(&head1);
	}
	
	printf("Enter node count(LL2)\n");
	scanf("%d",&nCount);

	for(int i = 1 ; i <= nCount ; i++){
		addNode(&head2);
	}

	printSLL(head1);
	printSLL(head2);


}

	

