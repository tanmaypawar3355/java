#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int data;
	struct Node *next;

}Node;

Node* createNode(){

	Node* newNode = (Node*)malloc(sizeof(Node));

	printf("Enter data\n");
	scanf("%d",&newNode -> data);

	newNode -> next = NULL;

	return newNode;
}

void main(){

	createNode();
	createNode();

}
