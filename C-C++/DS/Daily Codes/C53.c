#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

        struct Node *prev;
        int data;
        struct Node *next;

}Node;

Node *head = NULL;

Node* createNode(){

        Node *newNode = (Node*)malloc(sizeof(Node));

        newNode -> prev = NULL;

        printf("Enter data\n");
        scanf("%d",&newNode -> data);

        newNode -> next = NULL;

        return newNode;
}

void addNode(){    

        Node *newNode = createNode();

        if(head == NULL){

                head = newNode;
                newNode -> prev = head;
                newNode -> next = head;

        }else{
                Node *temp = head;

                while(temp -> next != head){

                        temp = temp -> next;
                }

                temp -> next = newNode;
                newNode -> prev = temp;
                newNode -> next = head;
                head -> prev = newNode;

        }}

int countNode(){

        if(head == NULL){
                printf("No node is present\n");

        }else{

                Node *temp = head;
                int count = 0;

                while(temp -> next != head){
                        count++;

                        temp = temp -> next;
        }
                        count++;
                        return count;
	}}

void addFirst(){            //with temp

	Node *newNode = createNode();

	if(head == NULL){
		head = newNode;
		newNode -> next = head;
		newNode -> prev = head;

	}else{ 
		newNode -> next = head;
		
		Node *temp = head;

		while(temp -> next != head){
			temp = temp -> next;
		}

		newNode -> prev = temp;
		temp -> next = newNode;
		head = newNode;
		
	}}


void printCLL(){

        if(head == NULL){
                printf("Nothing to print\n");

        }else{
                Node *temp = head;

                        while(temp -> next != head){

                                printf("|%d|->",temp -> data);
                                temp = temp -> next;
                
			}
                                printf("|%d|\n",temp -> data);
	}}
void main(){

      int node;
      printf("Enter no of node\n");
      scanf("%d",&node);
      for(int i=1;i<=node;i++){
      addNode();
      }
	
      printCLL();
      addFirst();
      printCLL();

}
    
