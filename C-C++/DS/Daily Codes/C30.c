#include<stdio.h>
#include<stdlib.h>

typedef struct Node {

        int data;
        struct Node *next;

}Node;

Node *head = NULL;

Node* createNode(){

        Node *newNode = (Node*)malloc(sizeof(Node));

        printf("Enter data\n");
        scanf("%d",&newNode -> data);

        newNode -> next = NULL;

        return newNode;

}

void addNode(){

        Node *newNode = createNode();

        if(head == NULL){
                head = newNode;

        }else{
                Node *temp = head;

                while(temp -> next != NULL){
                        temp = temp -> next;
                }
                temp -> next = newNode;
        }}

int countNode(){
        int count = 0;
        Node *temp = head;

        while(temp != NULL){
                count++;
                temp = temp -> next;
        }
        return count;
}

void searchNode(){
	Node *newNode;

	int wNode,flag = 0;

	printf("Enter which node do you want\n");
	scanf("%d",&wNode);

	Node* temp = head;
	while(temp != NULL){
		if(temp -> data == wNode){
			flag = 1;
		}temp = temp -> next;
	}

	if(flag == 1){
		printf("Node is present\n");
	}else{
		printf("Node is not present\n");
	}}


void main(){
        int num;

        printf("Enter how many nodes\n");
        scanf("%d",&num);

        for(int i = 1 ; i <= num ; i++){
                addNode();
        }
	
   	searchNode();

}
