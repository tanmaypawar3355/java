#include<stdio.h>

int ceilingNoBinarySearch(int arr[],int size,int key){

        int start = 0;
        int end = size - 1;
        int store = -1;

        while(start <=  end){

                int mid = start + end % 2;

                if(arr[mid] == key){
                        return arr[mid];
                }
                if(arr[mid] < key){	
   			start = mid + 1;
                }
                if(arr[mid] > key){
                        store = arr[mid];
                        end = mid - 1;
                }}
        return store;
}

void main(){

        int arr[] = {2,4,5,7,11,17,21};
        int size = sizeof(arr)/sizeof(int);

        int key = 9;

        int ret = ceilingNoBinarySearch(arr,size,key);

        if(ret == -1){
                printf("Fl");
        }else{
                printf("Ceiling No is %d\n",ret);

        }}
