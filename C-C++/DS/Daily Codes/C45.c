#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

        int data;
        struct Node *next;

}Node;

Node *head = NULL;

Node* createNode(){

        Node *newNode = (Node*)malloc(sizeof(Node));

        printf("Enter data\n");
        scanf("%d",&newNode -> data);

        newNode -> next = NULL;

        return newNode;
}

void addNode(){

        Node *newNode = createNode();

        if(head == NULL){
                head = newNode;
                newNode -> next = head;

        }else{
                Node *temp = head;

                while(temp -> next != head){

                temp = temp -> next;
                }
                temp -> next = newNode;
                newNode -> next = head;

        }}

void printSLL(){

        Node *temp = head;

        while(temp -> next != head){

                printf("|%d|->",temp -> data);
                temp = temp -> next;
        }

                printf("|%d|\n",temp -> data);
}

void addFirst(){
	
	Node* newNode = createNode();

	if(head == NULL){
		head = newNode;
		newNode -> next = head;

	}else{
		Node *temp = head;
		while(temp -> next != head){
	
			temp = temp -> next;
		}

		temp -> next = newNode;

		newNode -> next = head;
		head = newNode;

		//Node *temp = head;
		//temp -> next = newNode;
		//head = newNode;
	}}

void main(){

        addNode();
        addNode();
        addNode();
        addNode();
        printSLL();
	addFirst();
        printSLL();
}
