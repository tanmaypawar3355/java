#include<stdio.h>

int myStrCmp(char *str1,char *str2,int size){

	if(size > 0){
		if(*str1 == *str2){
			return myStrCmp(str1+1,str2+1,size-1);
		}else{
			return 1;
		}}
	return 0;
}

void main(){

        char *str1 = "shashi";
        char *str2 = "shashikant";

        int size = 6;

        int diff = myStrCmp(str1,str2,size);

        if(diff == 0){
                printf("Strings are equal \n");
        }else{
                printf("Strings are not equal \n");
        }}
