#include<stdio.h>
#include<stdbool.h>

bool palli(char arr[],int start,int end){

        if(arr[start] != arr[end]){
                return false;
        }

        if(start >= end){
                return true;
        }

	if(arr[start] == arr[end] && palli(arr,start+1,end-1)){
		return true;
	}}

void main(){

        char arr[] = {'M','L','D','A','M'};

        int size = 5;
        int start = arr[0];
        int end = arr[size-1];

        bool ret = palli(arr,start,end);

        if(ret == true){
                printf("String is pallindrome\n");
        }else{
                printf("String is not pallindrome\n");
        }}
