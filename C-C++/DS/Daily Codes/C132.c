#include <stdio.h>

int interpoletionSearch(int arr[],int size,int key){
    int start = 0;
    int end = size -1;
    
    int mid;

    while(start <= end){
	    
	    mid = start + (((key - arr[start]) * (end - start)) / (arr[end] - arr[start]));
	    
	    if(arr[mid] == key){
		    return mid;
            }
	
	    if(arr[mid] > key){
		    end = mid - 1;
	    
	    }else{
		    start = mid + 1;
	    }}
    return -1;
}

void main(){
  
    int arr[] = {1,3,5,7,9,11,13};
    int size = sizeof(arr)/sizeof(int);
    int key = 11;
    
    int inter = interpoletionSearch(arr,size,key);
    
    if(inter == -1){
	    printf("No. Not Found\n");
    }else{
	    printf("No. is at %d position\n",inter);
	    
}}
