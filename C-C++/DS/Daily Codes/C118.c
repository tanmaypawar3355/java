#include<stdio.h>

int countStep(int num){

	static int count = 0;

	if(num > 0){

		if(num % 2 == 0){
			num = num / 2;
			count ++;

		}
		
		if(num % 2 != 0){
			count++;
			return countStep(num - 1);
		}}
	return count;
}


void main(){

	int num = 14;

	int ret = countStep(num);

	printf("Steps = %d\n",ret);

}
