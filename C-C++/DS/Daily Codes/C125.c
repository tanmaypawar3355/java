#include<stdio.h>
#include<stdbool.h>

bool isSorted(int arr[],int size){

	if(size > 1){
		if(arr[size] < arr[size+1] && arr[size] > arr[size-1]){
			return isSorted(arr,size-1);

		}else{
			return false;
		}}

	return true;
}





void main(){

        int arr[] = {1,5,12,9,12,56,78};

        int size = sizeof(arr)/sizeof(arr[0]);

        bool ret = isSorted(arr,size-2);

        if(ret == true){
                printf("Array is sorted\n");
        }else{
                printf("Array is not sorted\n");
        }}
