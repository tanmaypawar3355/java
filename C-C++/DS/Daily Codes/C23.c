#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

        int data;
        struct Demo *next;

}Demo;

Demo *head = NULL;

Demo* createNode(){

        Demo *newNode = (Demo*)malloc(sizeof(Demo));

        printf("Enter data : ");
        scanf("%d",&newNode -> data);

        newNode -> next = NULL;
        return newNode;
}

void addNode(){

        Demo *newNode = createNode();

        if(head == NULL){
                head = newNode;

        }else{
                Demo *temp = head;

                while(temp -> next != NULL){
                        temp = temp -> next;
                }
                temp -> next = newNode;
        }}

void addFirst(){

        printf("\nAdd At First Position\n");
        Demo *newNode = createNode();

        if(head == NULL){
                head = newNode;

        }else{
                newNode -> next = head;
                head = newNode;
        }}

void addLast(){
	
	Demo *temp = head;

	while(temp -> next != NULL){
		temp = temp -> next;
	}
	addNode();
}


void addAtPos(int pos){
        
	Demo *newNode = createNode();

        Demo *temp = head;

        while(pos-2){
                temp = temp -> next;
                pos--;
        }
        newNode -> next = temp -> next;
        temp -> next = newNode;
}

void printLL(){

        Demo *temp = head;

        while(temp != NULL){

                printf("Id = %d\n",temp -> data);
                temp = temp -> next;
        }}

void main(){

        int count = 0;

        printf("Enter how many nodes you want\n");
        scanf("%d",&count);

        getchar();

        for(int i = 1 ; i<= count ; i++){
                addNode();
        }

        printLL();
        addFirst();
        printLL();

	int pos;
	printf("Enter at what position you want to add\n");
	scanf("%d",&pos);

	addAtPos(pos);
	printLL();

}
