#include<stdio.h>
#include<stdbool.h>

int flag;

bool isSorted(int arr[],int size){
	
	for(int i = 1 ; i < size-1; i++){
		if(arr[i] < arr[i+1] && arr[i] > arr[i-1]){
			flag = 1;
		}else{
			return false;
		}}
	return true;
}

void main(){

	int arr[] = {1,5,8,9,12,56,78};

	int size = sizeof(arr)/sizeof(arr[0]);

	bool ret = isSorted(arr,size);

	if(ret == true){
		printf("Array is sorted\n");
	}else{
		printf("Array is not sorted\n");
	}}
