#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

        struct Node *prev;
        int data;
        struct Node *next;

}Node;

Node *head = NULL;

Node* createNode(){

        Node *newNode = (Node*)malloc(sizeof(Node));

        newNode -> prev = NULL;

        printf("Enter data\n");
        scanf("%d",&newNode -> data);

        newNode -> next = NULL;

        return newNode;
}

void addNode(){

        Node *newNode = createNode();

        if(head == NULL){

                head = newNode;
                newNode -> prev = head;
                newNode -> next = head;

        }else{
                Node *temp = head;

                while(temp -> next != head){

                        temp = temp -> next;
                }

                temp -> next = newNode;
                newNode -> prev = temp;
                newNode -> next = head;
                head -> prev = newNode;

        }}

int countNode(){

        if(head == NULL){
                printf("No node is present\n");

        }else{

                Node *temp = head;
                int count = 0;

                while(temp -> next != head){
                        count++;

                        temp = temp -> next;
        }
                        count++;
                        return count;
        }}

void addFirst1(){            //withot temp

        Node *newNode = createNode();

        if(head == NULL){
                head = newNode;
                newNode -> next = head;
                newNode -> prev = head;

        }else{
                newNode -> next = head;
                head -> prev -> next = newNode;
                newNode -> prev = head -> prev;
                head -> prev = newNode;
                head = newNode;
        }}

int addAtPos(int pos){

	int count = countNode();

	if(pos <= 0 || pos > count +1){
		printf("Invalid Position\n");

	}else{
		if(pos == 1){
			addFirst1();

		}else if(pos == count){
			addNode();

		}else{
			Node *newNode = createNode();
			Node *temp = head;

			while(pos-2){
				temp = temp -> next;
				pos--;
			}

			newNode -> next = temp -> next;
			temp -> next -> prev = newNode;
			newNode -> prev = temp;
			temp -> next = newNode;
		}}}




void printCLL(){

        if(head == NULL){
                printf("Nothing to print\n");

        }else{
                Node *temp = head;

                        while(temp -> next != head){

                                printf("|%d|->",temp -> data);
                                temp = temp -> next;

                        }
                                printf("|%d|\n",temp -> data);
        }}

void main(){

      addNode();
      addNode();
      addNode();
      addNode();

      printCLL();

      int pos;
      printf("Enter the position you want to add\n");
      scanf("%d",&pos);
      addAtPos(pos) ;

      printCLL();
}
