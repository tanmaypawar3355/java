#include<stdio.h>

int countZero(int num){

	int count = 0;
	
	while(num){
		if(num % 10 == 0){
			count++;
		}
		num = num / 10;
	}
	return count;
}

void main(){
	
	int num = 1005060;

	int count = countZero(num);

	printf("No. of 0's in the number = %d\n",count);
	
}
