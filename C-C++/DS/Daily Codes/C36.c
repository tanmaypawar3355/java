#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

        struct Node *prev;
        int data;
        struct Node *next;
}Node;

Node *head = NULL;

Node* createNode(){

        Node *newNode = (Node*)malloc(sizeof(Node));

        newNode -> prev = NULL;

        printf("Enter data\n");
        scanf("%d",&newNode -> data);

        newNode -> next = NULL;

        return newNode;
}

void addNode(){

        Node *newNode = createNode();

        if(head == NULL){
                head = newNode;

        }else{
                Node *temp = head;

                while(temp -> next != NULL){
                        temp = temp -> next;
                }
                        temp -> next =  newNode;
                        newNode -> prev = temp;
        }}

void addFirst(){
        Node *newNode = createNode();

        if(head == NULL){
                head = newNode;
        }
        else{
                newNode -> next = head;
                head -> prev = newNode;
                head = newNode;
        }}

int countNode(){
        Node *temp = head;
        int count = 0;

        while(temp != NULL){
                temp = temp -> next;
                count++;
        }}

void addAtPos(int pos){

        int count  = countNode();

        if(pos <= 0 || pos >= count+2){
                printf("Invalid position\n");
        }
        else{
                if(pos == 1){
                        addFirst();

                }else if(pos == count){
                        addNode();

                }else{
                        Node *newNode = createNode();
                        Node *temp = head;

                        while(pos-2){
                                temp = temp -> next;
                                pos--;
                        }

                        newNode -> prev = temp;
                        newNode -> next = temp -> next;
                        temp -> next = newNode;
                        temp -> next -> next -> prev = newNode;
                }}}
int delFirst(){
        int count = countNode();

        if(head == NULL){
                printf("Nothing to delete\n");
                return -1;

        }else if(count == 1){

                free(head);
                head = NULL;

        }else{
                head = head -> next;
                free(head -> prev);
                head -> prev = NULL;
        }}

int deleteLast(){
	int count = countNode();

	if(head == NULL){
		printf("Nothing to delete\n");
		return -1;

	}else{
		if(count == 1){
			free(head);
			head = NULL;
		
		}else{
			Node *temp = head;

			while(temp -> next -> next != NULL){
				temp = temp -> next;
			}

			free(temp -> next);
			temp -> next = NULL;
		}}}




void main(){

        addNode();
        addNode();
        addNode();
        addNode();
        countNode();

        int pos;
        printf("\nEnter on which position do you want to add\n");
        scanf("%d",&pos);
        addAtPos(pos);

        delFirst();
	deleteLast();
}
