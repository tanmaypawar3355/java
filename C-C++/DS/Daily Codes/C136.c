#include<stdio.h>

int firstOccurBinarySearch(int arr[],int size,int key){

        int start = 0;
        int end = size - 1;
        int store = -1;
        int mid;

        while(start <=  end){

                mid = start + end % 2;

                if(arr[mid] == key){
                        store = mid;

                        if(arr[mid-1] != key){
                                return mid;
                }}
         
                if(arr[mid] < key){
                        start = mid + 1;
                }
                if(arr[mid] > key){
                      end = mid - 1;
                }}
        return store;
}

void main(){

        int arr[] = {2,2,2,3,3,3,3,4,4,5,6,8,11,18};
        int size = sizeof(arr)/sizeof(int);

        int key = 3;

        int ret = firstOccurBinarySearch(arr,size,key);

        if(ret == -1){
                printf("Fl");
        }else{
                printf("First occurance is at %d\n",ret);

        }}
