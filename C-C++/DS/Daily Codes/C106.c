#include<stdio.h>

int sumArray(int arr[],int size){

	int sum = 0;

	for(int i = 0 ; i < size ; i++){
		sum = sum + arr[i];
	}
	return sum;
}


void main(){

	int size = 5;
	int arr[] = {10,20,30,40,50};

	int ret = sumArray(arr,size);
	printf("%d\n",ret);
}
