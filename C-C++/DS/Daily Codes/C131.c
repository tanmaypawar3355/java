#include<stdio.h>

int binarySearch (int arr[], int size, int key){  
	int start = 0;
	int end = size - 1;
	int mid;

	while (start <= end){
		
		mid = start + end / 2;
		
		if(arr[mid] == key){
			return mid;
		}
		if(arr[mid] < key){
			start = mid + 1;
		}
		if (arr[mid] > key){ 
			end = mid - 1;
		}}
	return -1;
}

void main(){
	int arr[] = { 3, 7, 11, 13, 14, 17, 19, 25, 35, 50 };
  
	int size = sizeof(arr)/sizeof(int);
  
	int key = 7;
  
	int ret = binarySearch (arr, size, key);
  
	if(ret == -1){
		printf ("Element in not present\n");
	}else{   
		printf ("Index = %d\n", ret);
	}}

