#include<stdio.h>

int count = 0;

int countZero(int num){

	if(num > 0){
		if(num % 10 == 0){
			count ++;
		}
		
		countZero(num/10);
	}

	return count;
}

void main(){

        int num = 1005060;

        int count = countZero(num);

        printf("No. of 0's in the number = %d\n",count);

}
