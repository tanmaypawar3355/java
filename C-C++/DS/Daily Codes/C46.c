#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

        int data;
        struct Node *next;

}Node;

Node *head = NULL;

Node* createNode(){

        Node *newNode = (Node*)malloc(sizeof(Node));

        printf("Enter data\n");
        scanf("%d",&newNode -> data);

        newNode -> next = NULL;

        return newNode;
}

void addNode(){

        Node *newNode = createNode();

        if(head == NULL){
                head = newNode;
                newNode -> next = head;

        }else{
                Node *temp = head;

                while(temp -> next != head){

                temp = temp -> next;
                }
                temp -> next = newNode;
                newNode -> next = head;

        }}

void printSLL(){

        Node *temp = head;

        while(temp -> next != head){

                printf("|%d|->",temp -> data);
                temp = temp -> next;
        }

                printf("|%d|\n",temp -> data);
}

void addFirst(){

        Node* newNode = createNode();

        if(head == NULL){
                head = newNode;
                newNode -> next = head;

        }else{
                Node *temp = head;
                while(temp -> next != head){

                        temp = temp -> next;
                }

                temp -> next = newNode;

                newNode -> next = head;
                head = newNode;

         }}

int countNode(){

	Node *temp = head;
	int count = 0;

	while(temp -> next != head){
		count++;

		temp = temp -> next;
	}
   		count++;
		return count;
}

int addAtPos(int pos){
	
	int count = countNode();

	if(pos <= 0 || pos >= count + 2){
		printf("Invalid Positin\n");
		return -1;

	}else{
		if(pos == 1){
			addFirst();
		
		}else if(pos == count +1){
			addNode();

		}else{
			Node *newNode = createNode();
			Node *temp = head;
			while(pos-2){
				temp = temp -> next;
				pos--;
			}
			
			newNode -> next = temp -> next;
			temp -> next = newNode;
		}}
		return 0;
	}	



void main(){

        addNode();
        addNode();
        addNode();
        addNode();
	
        printSLL();
        
 	int pos;
	printf("Enter on which position you want to add\n");
	scanf("%d",&pos);
	addAtPos(pos);

	printSLL();
}
