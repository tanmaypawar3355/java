#include<stdio.h>
#include<stdlib.h>

typedef struct Movie{

        char mName[20];
        float imdb;
        struct Movie *next;

}Mov;

Mov *head = NULL;

void addNode(){

        Mov *newNode = (Mov*)malloc(sizeof(Mov));

        //getchar();
        printf("Enter Movie Name : ");
        char ch;
        int i = 0;

        while((ch = getchar()) != '\n'){
                (*newNode).mName[i] = ch;
                i++;
        }

        printf("Enter IMDB rating : ");
        scanf("%f",&newNode -> imdb);
        getchar();

        newNode -> next = NULL;

        if(head == NULL){
                head = newNode;

        }else{
                Mov *temp = head;

                while(temp -> next != NULL){
                        temp = temp -> next;
                }
                temp -> next = newNode;
        }}

void printLL(){

        Mov *temp = head;

        while(temp != NULL){

                printf("Name = %s\n",temp -> mName);
                printf("Imdb = %f\n",temp -> imdb);
                temp = temp -> next;
        }}

void countNode(){
	
	int count = 0;
	Mov *temp = head;

	while(temp != NULL){
		count++;
		temp = temp -> next;
	}
	printf("count = %d\n",count);
}

void main(){

        int count = 0;

        printf("Enter how many nodes you want\n");
        scanf("%d",&count);

        getchar();

        for(int i = 1 ; i<= count ; i++){
                addNode();
        }

        printLL();
	countNode();
}
