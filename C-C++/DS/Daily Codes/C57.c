#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

        struct Node *prev;
        int data;
        struct Node *next;

}Node;

Node *head = NULL;

Node* createNode(){

        Node *newNode = (Node*)malloc(sizeof(Node));

        newNode -> prev = NULL;

        printf("Enter data\n");
        scanf("%d",&newNode -> data);

        newNode -> next = NULL;

        return newNode;
}

void addNode(){

        Node *newNode = createNode();

        if(head == NULL){

                head = newNode;
                newNode -> prev = head;
                newNode -> next = head;

        }else{
                Node *temp = head;

                while(temp -> next != head){

                        temp = temp -> next;
                }

                temp -> next = newNode;
                newNode -> prev = temp;
                newNode -> next = head;
                head -> prev = newNode;

        }}

int countNode(){

        if(head == NULL){
                printf("No node is present\n");

        }else{

                Node *temp = head;
                int count = 0;

                while(temp -> next != head){
                        count++;

                        temp = temp -> next;
        }
                        count++;
                        return count;
        }}

void deleteLast(){

        if(head == NULL){
                printf("Nothing to delete\n");

        }else if(head -> next == head){
                free(head);
                head = NULL;

        }else{
		head -> prev = head -> prev -> prev;
		free(head -> prev -> next);
		head -> prev -> next = head;
	}}
                

void printCLL(){

        if(head == NULL){
                printf("Nothing to print\n");

        }else{
                Node *temp = head;

                        while(temp -> next != head){

                                printf("|%d|->",temp -> data);
                                temp = temp -> next;

                        }
                                printf("|%d|\n",temp -> data);
        }}

void main(){

      addNode();
      addNode();
      addNode();
      addNode();

      printCLL();
      deleteLast();
      printCLL();

}
