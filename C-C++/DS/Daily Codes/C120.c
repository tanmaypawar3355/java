#include<stdio.h>

int reverseNo(int num){
	
	int rem,reverse = 0;

	while(num != 0){

		rem = num % 10;
		reverse = reverse * 10 + rem;
		num = num / 10;
	}
	printf("Reversed No :- %d\n",reverse);
}


void main(){

	int num;

	printf("Enter the number :- ");
	scanf("%d",&num);
	
	reverseNo(num);
}
