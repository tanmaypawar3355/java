#include <stdio.h>

int search(int arr[],int size,int key){
    
    for(int i = 0 ; i < size ; i++){
        if(arr[i] == key){
            return i;
        }else{
            return -1;
        }
    }
}

int lastOccur(int arr[],int size,int key){
    int store = -1;  
    for(int i = 0 ; i < size ; i++){
        if(arr[i] == key){
            store = i;
        }
    }
    return store;
}

int secondLastOccur(int arr[],int size,int key){
    int store=-1;
    int secondLast = -1;
    for(int i = 0 ; i < size ; i++){
        if(arr[i] == key){
            secondLast = store;
            store = i;
        }
    }return secondLast;
}


void main(){
    
    int arr[] = {10,20,30,40,50,30,60,70};
    int key1 = 31;
    int size = 8;
    //1
    int ret1 = search(arr,size,key1);
    if(ret1 == -1){
        printf("Key Not found\n");
    }else{
        printf("Key fount at - %d\n",ret1);
    }
    //2
    int key2 = 80;
    int ret2 = lastOccur(arr,size,key2);
    if(ret2 == -1){
        printf("No occurance\n");
    }else{
        printf("Last occurance is at -> %d\n",ret2);
    }

    //3
    int key3 = 30;
    int ret3 = secondLastOccur(arr,size,key3);
    
    if(ret3 != 0){
        printf("second Last occurance is at -> %d\n",ret3);
    }else{
        printf("No occurance\n");
    }
}
