#include <stdio.h>

int size;
int top1 = 0;
int top2 = 0;
int flag;

int push1(int arr[]){
    
    if(top1 == top2-1){
        return -1;
    
    }else{
        
        top1++;
        printf("Enter data\n");
        scanf("%d",&arr[top1]);
        
        return 0;
    }}
    
int push2(int arr[]){
    
    if(top1 == top2-1){
        return -1;
    
    }else{
        top2--;
        printf("Enter data\n");
        scanf("%d",&arr[top2]);
        
        return 0;
    }}
    
int pop1(int arr[]){
    
    if(top1 == top2-1){
        flag = 0;
        return -1;
    
    }else{
        int val = arr[top1];
        top1--;
        flag = 1;
        return val;
    }}
    
int pop2(int arr[]){
    
    if(top1 == top2-1){
        flag = 0;
        return -1;
    
    }else{
        int val = arr[top1];
        top2++;
        flag = 1;
        return val;
    }}
    
void main(){
    
    printf("Enter size of stack\n");
    scanf("%d",&size);
    
    int arr[size];
    
    top1 = -1;
    top2 = size;
    
    char ch;
    
    do{
        printf("1.Push1\n");
        printf("2.Push2\n");
        printf("3.Pop1\n");
        printf("4.Pop2\n");
        
        int choice;
        printf("Enter your choice\n");
        scanf("%d",&choice);
        
        switch(choice){
            
            case 1:{
                    /*for(int i = 0 ; i < size ; i++){*/
                        int ret = push1(arr);
                        if(ret == -1){
                            printf("Overflow\n");
                        }}
                    break;
                    
            case 2:{
                    int ret1 = push2(arr);
                    if(ret1 == -1){
                            printf("Overflow\n");
                        }}
                    break;
                    
            case 3:{
                    int ret2 = pop1(arr);
                    if(flag == 0){
                            printf("Underflow\n");
                        }else{
                            printf("%d popped\n",ret2);
                        }}
                    break;
                    
            case 4:{
                    int ret3 = pop1(arr);
                    if(flag == 0){
                            printf("Underflow\n");
                        }else{
                            printf("%d popped\n",ret3);
                        }}
                    break;
                    
            default:
                    printf("Wrong choice\n");
        }
        getchar();
        printf("Do you want to continue\n");
        scanf("%c",&ch);
        }
        while(ch == 'y' || ch == 'Y');
        }
