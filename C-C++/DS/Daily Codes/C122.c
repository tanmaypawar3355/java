#include<stdio.h>
#include<stdbool.h>

bool isVerified(int arr[],int size){

	int count = 0;

	for(int i = 0 ; i < size ; i++){

		if(arr[i] % 2 == 0){
			count++;
		}}
	if(count > 2){
		return true;
	}else{
		return false;
	}
}


void main(){

	int arr[] = {1,2,3,5,9};

	int size = sizeof(arr)/sizeof(arr[0]);

	bool ret = isVerified(arr,size);

	if(ret == true){
		printf("There are more than 2 even numbers\n");
	}else{
		printf("There are no more than 2 even numbers\n");
	}}
