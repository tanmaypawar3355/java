#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

        struct Node *prev;
        int data;
        struct Node *next;

}Node;

Node *head = NULL;

Node* createNode(){

        Node *newNode = (Node*)malloc(sizeof(Node));

        newNode -> prev = NULL;

        printf("Enter data\n");
        scanf("%d",&newNode -> data);

        newNode -> next = NULL;

        return newNode;
}

void addNode(){

        Node *newNode = createNode();

        if(head == NULL){

                head = newNode;
                newNode -> prev = head;
                newNode -> next = head;

        }else{
                Node *temp = head;

                while(temp -> next != head){

                        temp = temp -> next;
                }

                temp -> next = newNode;
                newNode -> prev = temp;
                newNode -> next = head;
                head -> prev = newNode;

        }}

void deleteLast(){

        if(head == NULL){
                printf("Nothing to delete\n");

        }else if(head -> next == head){
                free(head);
                head = NULL;

        }else{
                head -> prev = head -> prev -> prev;
                free(head -> prev -> next);
                head -> prev -> next = head;
        }}

void deleteFirst(){

        if(head == NULL){
                printf("Nothing to delete\n");

        }else if(head -> next == head){
                free(head);
                head = NULL;

        }else{
                head -> next -> prev = head -> prev;
                head -> prev -> next = head -> next;

                Node *temp = head;
                head = head -> next;
                free(temp);
        }}

int countNode(){

        if(head == NULL){
                printf("No node is present\n");

        }else{

                Node *temp = head;
                int count = 0;

                while(temp -> next != head){
                        count++;

                        temp = temp -> next;
        }
                        count++;
                        return count;
        }}

int deleteAtPos(int pos){

	int count = countNode();

	if(pos <= 0 || pos > count + 1){
		printf("Invalid Position\n");
		return -1;

	}else{
		if(pos == 1){
			deleteFirst();

		}else if(pos == count){
			deleteLast();
		
		}else{
			Node *temp = head;

			while(pos-2){

				temp = temp -> next;
				pos--;
			}

			temp -> next = temp -> next -> next;
			free(temp -> next -> prev);
			temp -> next -> prev = temp;
		}}}


      

void printCLL(){

        if(head == NULL){
                printf("Nothing to print\n");

        }else{
                Node *temp = head;

                        while(temp -> next != head){

                                printf("|%d|->",temp -> data);
                                temp = temp -> next;

                        }
                                printf("|%d|\n",temp -> data);
        }}

void main(){

      addNode();
      addNode();
      addNode();
      addNode();

      printCLL();
	
      int pos;
      printf("Enter position at which do you want to delete\n");
      scanf("%d",&pos);
      deleteAtPos(pos);

      printCLL();
}
