#include<stdio.h>

int sum = 0;

int sumA(int arr[],int N){

	if(N == 0){
		return 0;
	}

	return sumA(arr,(N-1)) + arr[N-1];
}

void main(){

	int N = 5;

	int arr[] = {10,20,30,40,50};

	int ret = sumA(arr,N);

	printf("Sum = %d\n",ret);

}
