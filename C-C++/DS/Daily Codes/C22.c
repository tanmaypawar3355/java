#include<stdio.h>
#include<stdlib.h>

typedef struct Employee{

        char Name[20];
        int id;
        struct Employee *next;

}Emp;

Emp *head = NULL;

Emp* createNode(){

        Emp *newNode = (Emp*)malloc(sizeof(Emp));

        printf("Enter Employee Name : ");
        char ch;
        int i = 0;

        while((ch = getchar()) != '\n'){
                (*newNode).Name[i] = ch;
                i++;
        }

        printf("Enter Id : ");
        scanf("%d",&newNode -> id);
        getchar();

        newNode -> next = NULL;
        return newNode;
}

void addNode(){

        Emp *newNode = createNode();

        if(head == NULL){
                head = newNode;

        }else{
                Emp *temp = head;

                while(temp -> next != NULL){
                        temp = temp -> next;
                }
                temp -> next = newNode;
        }}

void addFirst(){

	printf("\nAdd At First Position\n");
        Emp *newNode = createNode();

        if(head == NULL){
                head = newNode;

        }else{
                newNode -> next = head;
                head = newNode;
        }}

void addAtPos(){
	printf("\nAdd At Position\n");
	Emp *newNode = createNode();

	int pos = 3;
	Emp *temp = head;

	while(pos-2){
		temp = temp -> next;
		pos--;
	}
	newNode -> next = temp -> next;
	temp -> next = newNode;
}

void printLL(){

        Emp *temp = head;

        while(temp != NULL){

                printf("Name = %s\n",temp -> Name);
                printf("Id = %d\n",temp -> id);
                temp = temp -> next;
        }}

void main(){

        int count = 0;

        printf("Enter how many nodes you want\n");
        scanf("%d",&count);

        getchar();

        for(int i = 1 ; i<= count ; i++){
                addNode();
        }

        printLL();
        addFirst();
        printLL();

}
