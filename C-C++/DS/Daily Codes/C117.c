#include<stdio.h>

int countStep(int num){

	int count = 0;

	while(num){

		if(num % 2 == 0){
			num = num / 2;
			count++;
		}

		if(num % 2 != 0){
			num = num - 1;
			count++;
		}}
	return count;
}

void main(){

	int num = 14;

	int ret = countStep(num);

	printf("Steps = %d\n",ret);
}
