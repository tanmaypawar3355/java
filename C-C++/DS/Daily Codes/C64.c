#include<stdio.h>
#include<stdbool.h>

int top = -1;
int size;

bool isEmpty(){

	if(top == -1){
		return true;
	}else{
		return false;
	}}

int pop(int stack[]){

	if(isEmpty){
		return -1;
	}else{
		int val = stack[top];
		top--;
		return val;
	}}

void main(){	
	int stack[5];

	int ret = pop(stack);
	if(ret == -1){
		printf("stack underflow\n");
	}else{
		int ret = pop(stack);
		printf("%d popped\n",ret);
	}}	
