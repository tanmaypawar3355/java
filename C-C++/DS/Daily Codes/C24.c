#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

        int data;
        struct Demo *next;

}Demo;

Demo *head = NULL;

Demo* createNode(){

        Demo *newNode = (Demo*)malloc(sizeof(Demo));

        printf("Enter data : ");
        scanf("%d",&newNode -> data);

        newNode -> next = NULL;
        return newNode;
}

void addNode(){

        Demo *newNode = createNode();

        if(head == NULL){
                head = newNode;

        }else{
                Demo *temp = head;

                while(temp -> next != NULL){
                        temp = temp -> next;
                }
                temp -> next = newNode;
        }}

void addFirst(){

        printf("\nAdd At First Position\n");
        Demo *newNode = createNode();

        if(head == NULL){
                head = newNode;

        }else{
                newNode -> next = head;
                head = newNode;
        }}

void addLast(){

        Demo *temp = head;

        while(temp -> next != NULL){
                temp = temp -> next;
        }
        addNode();
}


void addAtPos(int pos){

        Demo *newNode = createNode();

        Demo *temp = head;

        while(pos-2){
                temp = temp -> next;
                pos--;
        }
        newNode -> next = temp -> next;
        temp -> next = newNode;
}

void printLL(){

        Demo *temp = head;

        while(temp != NULL){

                printf("Id = %d\n",temp -> data);
                temp = temp -> next;
        }}

void main(){

	int num;
	char choice;

	do {
		printf("1.addnode\n");
		printf("2.addfirst\n");
		printf("3.addLast\n");
		printf("4.addAtPos\n");
		printf("5.printLL\n");

		printf("Enter your choice\n");
		scanf("%d",&num);

	switch(num){
		case 1:
			{
			int node;
			printf("Enter no of node\n");
			scanf("%d",&node);
			for(int i=1;i<=node;i++){
				addNode();
			}
			}
			break;
		case 2:
			addFirst();
			break;
		case 3:
			addLast();
			break;
		case 4:
			{
                        int pos;
                        printf("Enter position \n");
                        scanf("%d",&pos);
                        addAtPos(pos);
                        }
                        break;
		case 5:
			printLL();
			break;

		default:
			{
			printf("Wrong choice\n");
			}

	}
	getchar();
	printf("Do you want to continue\n");
	scanf("%c",&choice);
	}
	while(choice =='y' || choice =='Y');
}
