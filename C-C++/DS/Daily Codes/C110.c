#include<stdio.h>
#include<stdbool.h>

bool palli(char arr[],int size){

	int start = 0;
	int end = size - 1;

	while(start < end){

		if(arr[start] == arr[end]){
			start++;
			end--;
		}else{
			return false;
		}}
	return true;
}

void main(){

	int size = 5;

	char arr[] = {'M','A','D','A','M'};

	bool ret = palli(arr,size);

	if(ret == 1){
		printf("String is pallindrome\n");
	}else{
		printf("String is not pallindrome\n");
	}}
