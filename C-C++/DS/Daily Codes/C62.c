#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
    
    int data;
    struct Node *next;
}Node;

Node *head = NULL;

Node* createNode(){
    
    Node *newNode = (Node*)malloc(sizeof(Node));
    
    printf("Enter data - ");
    scanf("%d",&newNode -> data);
    
    newNode -> next = NULL;
    
    return newNode;
}

void push(){
    
    Node *newNode = createNode();
    
    if(head == NULL){
        head = newNode;
        
    }else{
        
        Node *temp = head;
        
        while(temp -> next != NULL){
            temp = temp -> next;
        }
        temp -> next = newNode;
    }}

int pop(){
    
    if(head == NULL){
        return -1;
        
    }else{
        Node *temp = head;
        
        while(temp -> next -> next != NULL){
            temp = temp -> next;
        }
        
        int data = temp -> next -> data;
        free(temp -> next);
        temp -> next = NULL;
        return data;
}}

int peek(){
    
    if(head == NULL){
        printf("Stack is overflow\n");
        return -1;
        
    }else{
        Node *temp = head;
        
        while(temp -> next != NULL){
            temp = temp -> next;
        }
        
        int data = temp -> data;
        return data;
}}

void main(){
    char ch;
    
    do{
        printf("1.Push\n");
        printf("2.Pop\n");
        printf("3.Peek\n");
        
        int choice;
        printf("Enter your choice\n");
        scanf("%d",&choice);
        
        switch(choice){
            
            case 1:
                    int size;
                    printf("Enter size of stack\n");
                    scanf("%d",&size);
                    if(size <= 0 || size > 5){
                        printf("Invalid size, plz enter between 1-5\n");
                    }else{
                        for(int i = 0 ; i < size ; i++){
                            push();
                    }}
                    break;
                    
            case 2:
                    int ret = pop();
                    if(ret == -1){
                        printf("stack is underflow\n");
                    }else{
                        printf("%d popppped\n",ret);
                    }
                    break;
                    
            case 3:
                    int print = peek();
                    printf("Value  at top = %d\n",print);
                    
                    break;
                
            default:
                    printf("Wrong choice\n");
        }
        getchar();
        printf("Do you want to continue\n");
        scanf("%c",&ch);
        }
        while(ch == 'y' || ch == 'Y');
        }
