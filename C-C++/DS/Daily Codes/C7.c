#include<stdio.h>
#include<string.h>

typedef struct Company {

        int empCount;
        char Name[20];
        float revenue;
        struct Company *next;

}com;

void main(){

        com obj1,obj2,obj3;

        struct Company *head = &obj1;

        head -> empCount = 10;
        strcpy(head -> Name , "c2w");
        head -> revenue = 50.00;
        head -> next = &obj2;

        head -> next -> empCount = 15;
        strcpy(head -> next -> Name , "Biencaps");
        head -> next -> revenue = 75.00;
        head -> next -> next  = &obj3;

        head -> next -> next -> empCount = 10;
        strcpy(head -> next -> next -> Name , "Boompanda");
        head -> next -> next -> revenue = 9.00;
        head -> next -> next = NULL;

}
