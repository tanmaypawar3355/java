#include<stdio.h>
#include<stdbool.h>

bool isVerified(int arr[],int size){

	static int count = 0;

	if(size > 0){

		if(arr[size] % 2 == 0){
			count++;
		}
		return isVerified(arr,size-1);
	}

	if(count > 2){
                return true;
        }else{
                return false;
        }
}

void main(){
	
	int arr[] = {10,20,30,50,90};

        int size = sizeof(arr)/sizeof(arr[0]);

        bool ret = isVerified(arr,size);

        if(ret == true){
                printf("There are more than 2 even numbers\n");
        }else{
                printf("There are no more than 2 even numbers\n");
        }}
