#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int data;
	int priority;
	struct Node *next;

}Node;

Node *head = NULL;

Node* createNode(){
	Node *newNode=(Node*)malloc(sizeof(Node));
	
	printf("Enter Data\n");
	scanf("%d",&newNode -> data);
	
	printf("Enter Priority\n");
	scanf("%d",&newNode -> priority);
	
	newNode -> next = NULL;

	return newNode;
}

int enque(){
	Node *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}
	else{
		Node *temp = head;
		while(temp-> next != NULL){
			if(temp -> priority > newNode -> priority){
				int temp1 = temp -> data;
				temp -> data = newNode -> data;
				newNode -> data = temp1;
			}
			temp = temp -> next;
		} 

		if(temp -> priority > newNode -> priority){
			int temp1 = temp -> data;
			temp -> data = newNode -> data;
			newNode -> data = temp1;
		}
		temp -> next = newNode;
	}
}
void printQ(){
	Node *temp = head;
	while(temp -> next != NULL){
		printf("|%d|->",temp -> data);
		temp = temp -> next;
	}
	printf("|%d|\n",temp -> data);
}
void main(){
	char ch;
	do{
		printf("1.Enque\n");
		printf("2.PrintQue\n");

		int num;
		printf("Enter Your Choice: ");
		scanf("%d",&num);

		switch(num){
			case 1:{
				int nodeCount;
				printf("Enter how many nodes\n");
				scanf("%d",&nodeCount);
				for(int i = 1 ; i <= nodeCount ; i++){
					enque();
				}}
				break;
			case 2: 
				printQ();
				break;  		
		}
		getchar();
		printf("Do you want to continue: ");
		scanf("%c",&ch);
}while(ch=='Y'||ch=='y');
}
