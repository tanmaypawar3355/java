#include<stdio.h>

int fibo(int N){


	if(N == 0){
		return 0;
	}
	if(N == 1){
		return 1;
	}

	return fibo(N-1) + fibo(N-2);
}

void main(){

	int num;
	printf("Enter the number from which you want to fibo\n");
	scanf("%d",&num);

	for(int i = 0 ; i < num ; i++){
		printf("%d\t",fibo(i));
		
	}
}
