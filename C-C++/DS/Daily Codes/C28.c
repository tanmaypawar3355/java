#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

        int data;
        struct Demo *next;

}Demo;

Demo *head = NULL;

Demo* createNode(){

        Demo *newNode = (Demo*)malloc(sizeof(Demo));

        printf("Enter data : ");
        scanf("%d",&newNode -> data);

        newNode -> next = NULL;
        return newNode;
}

void addNode(){

        Demo *newNode = createNode();

        if(head == NULL){
                head = newNode;

        }else{
                Demo *temp = head;

                while(temp -> next != NULL){
                        temp = temp -> next;
                }
                temp -> next = newNode;
        }}

int countNode(){
        int count = 0;
        Demo *temp = head;

        while(temp != NULL){
                count++;
                temp = temp -> next;
        }
        return count;
}

void addFirst(){

        printf("\nAdd At First Position\n");
        Demo *newNode = createNode();

        if(head == NULL){
                head = newNode;

        }else{
                newNode -> next = head;
                head = newNode;
        }}

void deleteFirst(){

        Demo *temp = head;
        head = temp -> next;
        free(temp);
}

void addLast(){

        Demo *temp = head;

        while(temp -> next != NULL){
                temp = temp -> next;
        }
        addNode();
}

void deleteLast(){

        Demo *temp = head;

        while(temp -> next -> next != NULL){

                temp = temp -> next;
        }

        free(temp -> next);
        temp -> next = NULL;
}

int addAtPos(int pos){

        int count = countNode();

        if(pos <= 0 || pos > count+1){
                printf("Inavalid Position\n");
                return -1;
        }else{
                if(pos == 1){
                        addFirst();
                }else if(pos == count+1){
                        addNode();
                }else{
                        Demo *newNode = createNode();
                        Demo *temp = head;

                        while(pos-2){
                                temp = temp -> next;
                                pos--;
                        }
                        newNode -> next = temp -> next;
                        temp -> next = newNode;
                }
                return 0;
        }}

int deleteAtPos(int pos){

        int count = countNode();

        if(pos <= 0 || pos > count+1){
                printf("Invalis posiyion\n");
                return -1;

        }else{
                if(pos == count+1){
                        deleteLast();
             
	     	}else if(pos == 1){
                        deleteFirst();
             
	     	}else{
                        Demo *temp1 = head;
                        Demo *temp2 = head;

                        while(pos-2){
                                temp1 = temp1 -> next;
                                pos--;
                        }

                        while(pos-1){
				temp2 = temp2 -> next;
				pos--;
			}
                        temp1 -> next = temp1 -> next -> next;
                        free(temp2);
                        temp2 = NULL;
                }}
        return 0;
}

void printLL(){

        Demo *temp = head;

        while(temp != NULL){

                printf("Id = %d\n",temp -> data);
                temp = temp -> next;
        }}

void main(){

        int num;
        char choice;

        do {
                printf("1.addNode\n");
                printf("2.countNode\n");
                printf("3.addFirst\n");
                printf("4.deleteFirst\n");
                printf("5.addLast\n");
                printf("6.deleteLast\n");
                printf("7.addAtPos\n");
                printf("8.deleteAtPos\n");
                printf("9.printLL\n");

                printf("Enter your choice\n");
                scanf("%d",&num);

        switch(num){
                case 1:
                        {
                        int node;
                        printf("Enter no of node\n");
                        scanf("%d",&node);
                        for(int i=1;i<=node;i++){
                                addNode();
                        }
                        }
                        break;
                case 2:
                        countNode();
                        break;
                case 3:
                        addFirst();
                        break;
                case 4:
                        deleteFirst();
                        break;
                case 5:
                        addLast();
                        break;
                case 6:
                        deleteLast();
                        break;
                case 7:
                        {
                        int pos;
                        printf("Enter position \n");
                        scanf("%d",&pos);
                        addAtPos(pos);
                        }
                        break;
                case 8:
                        {
                        int pos;
                        printf("Enter position \n");
                        scanf("%d",&pos);
                        deleteAtPos(pos);
                        }
                        break;
		case 9:
			printLL();
			break;

                default:
                        {
                        printf("Wrong choice\n");
                        }

        }
        getchar();
        printf("Do you want to continue\n");
        scanf("%c",&choice);
        }
        while(choice =='y' || choice =='Y');
}
