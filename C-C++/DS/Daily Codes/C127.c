#include<stdio.h>
#include<string.h>


int myStrCmp(char *str1,char *str2){

	if(*str1 != '\0'){

		if(*str1 == *str2){
			return myStrCmp(str1+1,str2+1);
		}
		else{
			return 1;
		}}
	return 0;
}

void main(){

        char *str1 = "tanmay";
        char *str2 = "tanMay";

        int diff;

        if(strlen(str1) == strlen (str2)){
                diff = myStrCmp(str1,str2);
        }

        if(diff == 0){
                printf("Strings are equal\n");
        }else{
                printf("Strings are not equal\n");
        }}
