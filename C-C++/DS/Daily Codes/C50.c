#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

        struct Node *prev;
        int data;
        struct Node *next;

}Node;

Node *head = NULL;

Node* createNode(){

        Node *newNode = (Node*)malloc(sizeof(Node));

        newNode -> prev = NULL;

        printf("Enter data\n");
        scanf("%d",&newNode -> data);

        newNode -> next = NULL;

        return newNode;
}

void addNode(){

        Node *newNode = createNode();

        if(head == NULL){

                head = newNode;
                newNode -> prev = head;
                newNode -> next = head;

        }else{
                Node *temp = head;

                while(temp -> next != head){

                        temp = temp -> next;
                }

                temp -> next = newNode;
                newNode -> prev = temp;
                newNode -> next = head;
                head -> prev = newNode;

        }}

int countNode(){

        if(head == NULL){
                printf("No node is present\n");

        }else{

                Node *temp = head;
                int count = 0;

                while(temp -> next != head){
                        count++;

                        temp = temp -> next;
        }
                        count++;
                        return count;
}



void printCLL(){

        if(head == NULL){
                printf("Nothing to print\n");
        }else{
                Node *temp = head;

                        while(temp -> next != head){

                                printf("|%d|-> ",temp -> data);
                                temp = temp -> next;
                }

                                printf("|%d|",temp -> data);
}

void main(){

        char ch;

	do{
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.countNode\n");
		printf("4.addLast\n");
		printf("5.deleteLast\n");
		printf("6.deleteFirst\n");
		printf("7.addAtPos\n");
		printf("8.deleteAtPos\n");
		printf("9.printCLL\n");
		
		int num;
		printf("Enter your choice\n");
		scanf("%d",&num);

	switch(num){
                case 1:
                        {
                        int node;
                        printf("Enter no of node\n");
                        scanf("%d",&node);
                        for(int i=1;i<=node;i++){
                                addNode();
                        }
                        }
                        break;
                case 2:
                        addFirst();
                        break;
		case 3: 
			countNode();
			break;
		case 4:
                        addLast();
                        break;
		case 5:
                        deleteLast();
                        break;
		case 6:
                        addFirst();
                        break;
                case 7:
                        {
                        int pos;
                        printf("Enter position \n");
                        scanf("%d",&pos);
                        addAtPos(pos);
                        }
                        break;
                case 8:
                        {
                        int pos;
                        printf("Enter position \n");
                        scanf("%d",&pos);
                        deleteAtPos(pos);
                        }
                        break;
                case 9:
                        printLL();
                        break;

                default:
                        {
                        printf("Wrong choice\n");
                        }

        }
        getchar();
        printf("Do you want to continue\n");
        scanf("%c",&choice);
        }
        while(choice =='y' || choice =='Y');
}
}
