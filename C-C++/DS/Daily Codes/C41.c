#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

        int data;
        struct Node *next;
}Node;

Node *head = NULL;

Node* createNode(){

        Node *newNode = (Node*)malloc(sizeof(Node));

        printf("Enter data\n");
        scanf("%d",&newNode -> data);

        newNode -> next = NULL;

        return newNode;
}

void addNode(){

        Node *newNode = createNode();

        if(head == NULL){
                head = newNode;

        }else{
                Node *temp = head;

                while(temp -> next != NULL){
                        temp = temp -> next;
                }
                        temp -> next =  newNode;
        }}



int countNode(){
        Node *temp = head;
        int count = 0;

        while(temp != NULL){
                temp = temp -> next;
                count++;
        }
        printf("\ncount = %d\n",count);
}

void printLL(){

        if(head == NULL){
                printf("Nothing to print\n");

        }else{
                Node *temp = head;

                while (temp != NULL){
                        printf("|%d| ->",temp -> data);
                        temp = temp -> next;
                }}}

void reverseLL(){


	 if(head == NULL){
		 printf("Nothing to reverse here\n");

	 }else{
		Node *temp1 = head;
	      //  Node *temp2 = head;

        	 int count = countNode();
         	 int cnt = count/2;

		 while(cnt){

			 Node *temp2 = head;

			 int i = 1;

			 while(i != count){

				 i++;
				 temp2 = temp2 -> next;
			 }

			 int temp = temp2 -> data;
			 temp2 -> data = temp1 -> data;
			 temp1 -> data = temp;

			 temp1 = temp1 -> next;		
			 cnt--;
		 }}}



        
void main(){

        addNode();
        addNode();
        addNode();
        addNode();

        printLL();
        reverseLL();
        printLL();

}
