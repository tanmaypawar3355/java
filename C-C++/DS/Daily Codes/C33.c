#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

        struct Node *prev;
        int data;
        struct Node *next;
}Node;

Node *head = NULL;

Node* createNode(){

        Node *newNode = (Node*)malloc(sizeof(Node));

        newNode -> prev = NULL;

        printf("Enter data\n");
        scanf("%d",&newNode -> data);

        newNode -> next = NULL;

        return newNode;
}

void addNode(){

        Node *newNode = createNode();

        if(head == NULL){
                head = newNode;

        }else{
                Node *temp = head;

                while(temp != NULL){
                        temp = temp -> next;
                }
                        temp -> next =  newNode;
                        newNode -> prev = temp;
        }}

void addFirst(){
	Node *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}
	else{
		newNode -> next = head;
		head -> prev = newNode;
		head = newNode;
	}}

void main(){
}


