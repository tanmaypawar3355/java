#include <stdio.h>
#include<stdbool.h>

int top = -1;
int size;
int flag;

bool isEmpty(){
    
    if(top == -1){
        return true;
    }else{
        return false;
    }
}


int peek(int arr[]){
    
    if(isEmpty()){
        flag = 0;
        return -1;
    
    }else{
        int val = arr[top];
        flag = 1;
        return val;
    }}
    
void main(){
    
    printf("Enter the size\n");
    scanf("%d",&size);
    
    int arr[size];

    int ret = peek(arr);
    
    if(flag == 0){
        printf("Stack is empty\n");
    }else{
        printf("%d",ret);
    }
}
