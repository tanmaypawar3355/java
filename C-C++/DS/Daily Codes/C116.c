#include<stdio.h>

int count = 0;

int countZero(int num){

	if(num % 10 == 0){
		count++;
	}

	if(num == 0){
		return count;
	}

	return countZero(num / 10);
}

void main(){

        int num = 1005060;

        int count = countZero(num);

        printf("No. of 0's in the %d = %d\n",num,count);

}
