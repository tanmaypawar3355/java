
#include<string.h>
#include<stdio.h>
#include<stdlib.h>

struct company {

	char cName[20];
	int employee;
	float revenue;

};

void main(){

	struct company *cptr = (struct company*)malloc(sizeof(struct company));

	strcpy(cptr -> cName,"Veritas");
	cptr -> employee = 700;
	(*cptr).revenue = 150.00;

	printf("Company name = %s ",cptr -> cName);
	printf("Employees = %d ",cptr -> employee);
	printf("Revenue = %f ",cptr -> revenue);

}
