#include<stdio.h>

int firstOccurBinarySearch(int arr[],int size,int key){

        int start = 0;
        int end = size - 1;
        int store = -1;
        int mid;

        while(start <=  end){

                mid = start + end % 2;

                if(arr[mid] == key){
                        store = mid;

                        if(arr[mid+1] != key){
                                return mid;
                }}

                if(arr[mid] < key){
                        start = mid + 1;
                }
                if(arr[mid] > key){
                      end = mid - 1;
                }}
        return store;
}

void main(){

        int arr[] = {2,3,4,5,5,5,7,7,8,9,9,9,11,12};
        int size = sizeof(arr)/sizeof(int);

        int key = 5;

        int ret = firstOccurBinarySearch(arr,size,key);

        if(ret == -1){
                printf("Fl");
        }else{
                printf("last occurance is at %d\n",ret);

        }}
