#include <stdio.h>
#include<stdlib.h>
#include<stdbool.h>

typedef struct Node{
    
    int data;
    struct Node *next;

}Node;

int countNode = 0;
Node *head = NULL;
int flag;

int eleCount(){
    
    struct Node *temp = head;
    int count = 0;
    
    while(temp != NULL){
        
        count++;
        temp = temp -> next;
    }
    return count;
}

bool isFull(){
    
    if(eleCount() == countNode){
        return true;
    
    }else{
        return false;
    }}
    
Node *createNode(){
    
    Node *newNode = (Node*)malloc(sizeof(Node));
    
    printf("Enter data\n");
    scanf("%d",&newNode -> data);
    
    newNode -> next = NULL;
    
    return newNode;
}

void addNode(){
    
    Node *newNode = createNode();
    
    if(head == NULL){
        head = newNode;
    
    }else{
        Node * temp = head;
        
        while(temp -> next != NULL){
            temp = temp -> next;
        }
        temp -> next = newNode;
    }}
    
int push(){
    
    if(isFull()){
        return -1;
        
    }else{
        addNode();
        return 0;
    }}
    
int pop(){
    
    if(head == NULL){
        flag = 0;
        
    }else{
        
        Node *temp = head;
        
        while(temp -> next != NULL){
            temp = temp -> next;
        }
    
        int data;
        data = temp -> next -> data;
        free(temp -> next);
        temp -> next = NULL;
        return data;
    }}
    
    void main(){
        
        for(int i = 0 ; i < 4 ; i++){
            push();
        }
        
        for(int i = 0 ; i < 4 ; i++){
            pop();
        }
    }
        


