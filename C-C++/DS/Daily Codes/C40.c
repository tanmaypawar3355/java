#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

        struct Node *prev;
        int data;
        struct Node *next;
}Node;

Node *head = NULL;

Node* createNode(){

        Node *newNode = (Node*)malloc(sizeof(Node));

        newNode -> prev = NULL;

        printf("Enter data\n");
        scanf("%d",&newNode -> data);

        newNode -> next = NULL;

        return newNode;
}

void addNode(){

        Node *newNode = createNode();

        if(head == NULL){
                head = newNode;

        }else{
                Node *temp = head;

                while(temp -> next != NULL){
                        temp = temp -> next;
                }
                        temp -> next =  newNode;
                        newNode -> prev = temp;
        }}

void addFirst(){
        Node *newNode = createNode();

        if(head == NULL){
                head = newNode;
        }
        else{
                newNode -> next = head;
                head -> prev = newNode;
                head = newNode;
        }}

int countNode(){
        Node *temp = head;
        int count = 0;

        while(temp != NULL){
                temp = temp -> next;
                count++;
        }
        printf("\ncount = %d\n",count);
}

void printDLL(){

        if(head == NULL){
                printf("Nothing to print\n");

        }else{
                Node *temp = head;

                while (temp != NULL){
                        printf("|%d| ->",temp -> data);
                        temp = temp -> next;
                }}}

void reverseDLL(){

         int count = countNode();

	Node *temp1 = head;
	Node *temp2 = head;

	while(temp2 -> next != NULL){
		temp2 = temp2 -> next;
	}

	int cnt = count/2;

	while(cnt){
		int temp = temp2 -> data;

		temp2 -> data = temp1 -> data;

		temp1 -> data = temp;
		
		temp1 = temp1 -> next;
		temp2 = temp2 -> prev;

		cnt--;
	}}

void main(){

        addNode();
        addNode();
        addNode();
        addNode();     

        printDLL();
        reverseDLL();
        printDLL();

}
