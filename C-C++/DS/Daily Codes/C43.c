#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

        int data;
        struct Node *next;

}Node;

Node *head = NULL;

Node* createNode(){

        Node *newNode = (Node*)malloc(sizeof(Node));

        printf("Enter data\n");
        scanf("%d",&newNode -> data);

        newNode -> next = NULL;

        return newNode;
}

void addNode(){

	Node *newNode = createNode();

	if(head == NULL){
		head = newNode;
		newNode -> next = head;

	}else{
		Node *temp = head;

		while(temp -> next != head){		
	
		temp -> next = newNode;
		newNode -> next = head;
		temp = temp -> next;
	}}}

void main(){

	addNode();
	addNode();
	addNode();
	addNode();	
}
