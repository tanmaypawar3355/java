#include<stdio.h>

int charArr(char arr[],int size,char search){

	for(int i = 0 ; i < size  ; i++){

		if(arr[i] == search){

			return 1;
		}}
	return 0;
}

void main(){

	char arr[] = {'A','B','C','D','E'};

	int size = 5;

	char search = 'D';

	int ret = charArr(arr,size,search);

	if(ret == 1){
		printf("Character is present\n");

	}else{
		printf("Character is not present\n");
	}}
