/*	input = {1,2,3,4,5,6};
	num = 10;

	output = 4+6 (so the o/p - the array has a pair which has a sum 10)
*/

#include<stdio.h>

int fun(){

	int arr[] = {1,2,3,4,5,6};
	int num = 10;
	int size = 6;
	int flag = 0;

	for(int i = 6 ; i >= 0 ; i--){

		for(int j = 0 ; j <= size-1 ; j++){

				if(arr[i] != arr[j] && arr[i] + arr[j] == num){

					flag = 1;

				}}}
				return flag;

}

void main(){

	int ret = fun();

	if(ret == 1){

		printf("Array has a good pair\n");
	}else{
		printf("Array do not have a good pair\n");
	}}


