#include<stdio.h>

int fun(int (*ptr)[4],int row,int column){
	
	int arr2[3];

	for(int i = 0 ; i < row; i++){

		int sum = 0;

		for(int j = 0 ; j < column ; j++){

			sum = sum + ptr[i][j]; 
		}
		arr2[i] = sum;
}
	for(int k = 0 ; k <= 2 ; k++){
		printf("|%d|",arr2[k]);
	}}


void main(){
	
	int row = 3;
	int column = 4;

	int arr[3][4] = {1,2,3,4,5,6,7,8,9,2,3,4};

	fun(arr,row,column);

}


/* Q2: Row sum
Problem Description :
- You are given a 2D integer matrix A and return a 1D integer array containing
row-wise sums of the original matrix.
- Return an array containing row-wise sums of the original matrix.
Problem Constraints :
1 <= A.size() <= 103
1 <= A[i].size() <= 103
1 <= A[i][j] <= 103
Input 1:
[1,2,3,4]
[5,6,7,8]
[9,2,3,4]
Output 1:
[10,26,18]
Explanation :
Row 1 = 1+2+3+4 = 10
Row 2 = 5+6+7+8 = 26
Row 3 = 9+2+3+4 = 18
*/
