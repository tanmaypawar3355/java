#include<stdio.h>

int fun(int (*ptr)[4],int row,int column){

	int num = 0;
	int arr2[4];

	for(int i = 0 ; i <= row ; i++){
		
		int sum = 0;

		for(int j = i ; j < row*column ; j = j + column){
	

				sum = sum + *(*ptr + j); 
		}
	
		arr2[i] = sum;
	}

	for(int i = 0 ; i < 4 ; i++){
                printf("|%d| ",arr2[i]);
	
	}

		printf("\n");
}

void main(){

	int row = 3;
	int column = 4;

	int arr[3][4] = {1,2,3,4,5,6,7,8,9,2,3,4};

	fun(arr,row,column);
}

/* Q1. Column Sum
Problem Description :
- You are given a 2D integer matrix A and return a 1D integer array containing
column-wise sums of the original matrix.
- Return an array containing column-wise sums of the original matrix.
Problem Constraints :
1 <= A.size() <= 103
1 <= A[i].size() <= 103
1 <= A[i][j] <= 103
Input :
[1,2,3,4]
[5,6,7,8]
[9,2,3,4]
Output :
{15,10,13,16}
Example Explanation
Column 1 = 1+5+9 = 15
Column 2 = 2+6+2 = 10
Column 3 = 3+7+3 = 13
Column 4 = 4+8+4 = 16
*/
