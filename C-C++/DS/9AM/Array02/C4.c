#include<stdio.h>

int fun(int (*ptr)[3],int row,int column){

	int sum = 0;
	int N = 3;
	
	for(int i = 0 ; i < row ; i++){

		for(int j = 0 ; j < column ; j++){

			if (i + j == 2)
				
				sum = sum + ptr[i][j];
     }}	
		printf("Sum = %d\n",sum);
	}

void main(){
	
	int row = 3;
	int column = 3;
	int arr[3][3] = {1,-2,-3,-4,5,-6,-7,-8,9};

	fun(arr,row,column);
}

/*

Q4. Minor Diagonal Sum
Problem Description :
- You are given a N X N integer matrix.
- You have to find the sum of all the minor diagonal elements of A.
- Minor diagonal of a M X M matrix A is a collection of elements
A[i, j] such that i + j = M + 1 (where i, j are 1-based).
- Return an integer denoting the sum of minor diagonal elements.
Problem Constraints :
1 <= N <= 103
-1000 <= A[i][j] <= 1000
Input 1:
A = [[1, -2, -3],
[-4, 5, -6],
[-7, -8, 9]]
Output 1:
-5
Input 2:
A = [[3, 2],
[2, 3]]
Output 2:
4
Example Explanation :
Explanation 1:
A[1][3] + A[2][2] + A[3][1] = (-3) + 5 + (-7) = -5
Explanation 2:
A[1][2] + A[2][1] = 2 + 2 = 4


*/

	
