#include<stdio.h>

int fun(int (*ptr)[3],int row,int column){

        int sum;
        int arr2[3][3];

        for(int i = 0 ; i < row ; i++){

               int num = 0;

                for(int j = i ; j < row*column ; j = j + column){


                                sum = *(*ptr + j);

                		arr2[i][num] = sum;
				num++;
               }}

        for(int k = 0 ; k < row ; k++){

        	for(int m = 0 ; m < column ; m++){

	
                printf("|%d|",arr2[k][m]);
	}
	printf("\n");
}}

void main(){

        int row = 3;
        int column = 3;

        int arr[3][3] = {1,2,3,4,5,6,7,8,9};

        fun(arr,row,column);
}

/*
 
   Q6. Matrix Transpose
Problem Description :
- You are given a matrix A, and you have to return another matrix which is the
transpose of A.
- You have to return the Transpose of this 2D matrix.
NOTE: Transpose of a matrix A is defined as - AT[i][j] = A[j][i] ; Where 1 ≤ i ≤ col
and 1 ≤ j ≤ row. The transpose of a matrix switches the element at (i, j)th index to (j, i)th
index, and the element at (j, i)th index to (i, j)th index.
Problem Constraints :
1 <= A.size() <= 1000
1 <= A[i].size() <= 1000
1 <= A[i][j] <= 1000
Input :
A = [[1, 2, 3],[4, 5, 6],[7, 8, 9]]
Output :
[[1, 4, 7], [2, 5, 8], [3, 6, 9]]
Explanation :
- after converting rows to columns and columns to rows of
[[1, 2, 3],[4, 5, 6],[7, 8, 9]]
we will get [[1, 4, 7], [2, 5, 8], [3, 6, 9]].

*/
