#include<stdio.h>

int fun(int (*ptr)[3],int row,int column){


	int sum = 0;

	for(int i = 0 ; i < row ; i++){

		//int sum = 0;

		for(int j = 0 ; j < column ; j++){

			if(i == j){

				sum = sum + ptr[i][j];
			}}}

	printf("Sum = %d\n",sum);


}


void main(){

	int row = 3;
        int column = 3;

        int arr[3][3] = {1,2,3,4,5,6,7,8,9};

        fun(arr,row,column);
}

/* 

Q3: Main Diagonal sum
Problem Description :
- You are given a N X N integer matrix.
- You have to find the sum of all the main diagonal elements of A.
- The main diagonal of a matrix A is a collection of elements A[i, j]
such that i = j.
- Return an integer denoting the sum of main diagonal elements.

Problem Constraints :
1 <= N <= 103
-1000 <= A[i][j] <= 1000

Input 1:
3 3 1 -2 -3 -4 5 -6 -7 -8 9
Output 1:
15

Input 2:
2 2 3 2 2 3
Output 2:
6

Example Explanation :
Explanation 1:
A[1][1] + A[2][2] + A[3][3] = 1 + 5 + 9 = 15
Explanation 2:
A[1][1] + A[2][2] = 3 + 3 = 6

*/
