#include<stdio.h>

int fun(int (*ptr1)[3],int (*ptr2)[3],int row,int column){

	int arr2[3][3];
        for(int i = 0 ; i < row ; i++){

                for(int j = 0 ; j < column; j++){

                        int store = ptr1[i][j] + ptr2[i][j];

			arr2[i][j] = store;

		}}

	for(int l = 0 ; l < row ; l++){

                for(int k = 0 ; k < column; k++){

			printf("|%d| ",arr2[l][k]);
		
		}printf("\n");
	}}

void main(){

        int arr1[3][3] = {1,2,3,4,5,6,7,8,9};

        int arr2[3][3] = {9,8,7,6,5,4,3,2,1};

        int row = 3;
        int column = 3;

        int ret = fun(arr1,arr2,row,column);
}

/*
 
Q9. Add the matrices
Problem Description :
- You are given two matrices A & B of same size, you have to return
another matrix which is the sum of A and B.
Problem Constraints :
1 <= A.size(), B.size() <= 1000
1 <= A[i].size(), B[i].size() <= 1000
1 <= A[i][j], B[i][j] <= 1000
Example Input :
Input :
A = [[1, 2, 3],
[4, 5, 6],
[7, 8, 9]]
B = [[9, 8, 7],
[6, 5, 4],
[3, 2, 1]]
Output :
[[10, 10, 10],
[10, 10, 10],
[10, 10, 10]]
Example Explanation :
A + B = [[1+9, 2+8, 3+7],[4+6, 5+5, 6+4],[7+3, 8+2, 9+1]] = [[10, 10, 10],
[10, 10, 10], [10, 10, 10]].

*/
