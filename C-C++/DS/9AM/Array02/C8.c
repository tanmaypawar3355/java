#include<stdio.h>

int fun(int (*ptr1)[3],int (*ptr2)[3],int row,int column){


	for(int i = 0 ; i < row ; i++){

		for(int j = 0 ; j < column; ){

			if(ptr1[i][j] == ptr2[i][j]){

				j++;

			}else{
				return -1;
			}}}}

void main(){

	int arr1[3][3] = {1,2,3,4,5,6,7,8,9};

	int arr2[3][3] = {1,2,3,4,5,6,7,8,9};

	int row = 3;
	int column = 3;

	int ret = fun(arr1,arr2,row,column);

	if(ret == -1){

		printf("Array are not same\n");

	}else{
		printf("Array are same\n");
	}
}

/*

Q8. Are Matrices the same?
Problem Description :
- You are given two matrices A & B of equal dimensions and you have to
check whether the two matrices are equal or not.
- Return 1 if both matrices are equal or return 0.
NOTE: Both matrices are equal if A[i][j] == B[i][j] for all i and j
in the given range.
Problem Constraints :
1 <= A.size(), B.size() <= 1000
1 <= A[i].size(), B[i].size() <= 1000
1 <= A[i][j], B[i][j] <= 1000
Example Input :
Input 1:
A = [[1, 2, 3],[4, 5, 6],[7, 8, 9]]
B = [[1, 2, 3],[4, 5, 6],[7, 8, 9]]
Output 1:
1
Input 2:
A = [[1, 2, 3],[4, 5, 6],[7, 8, 9]]
B = [[1, 2, 3],[7, 8, 9],[4, 5, 6]]
Output 2:
0
Example Explanation :
Explanation 1:
- All the elements of both matrices are equal at respective positions.
Explanation 2:
- All the elements of both matrices are not equal at respective positions.

*/
