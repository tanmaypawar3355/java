/*     1  b  2  d
          1  b  2
             1  b
	        2     */



#include<stdio.h>
void main(){
	int rows;

	printf("Enter rows\n");
	scanf("%d",&rows);

	for(int i=1 ; i<=rows ; i++){
		for(int sp=1 ; sp<=i-1 ; sp++){
			printf("  ");
		}
		for(int j=1 ; j<=rows-i+1 ; j++){
			printf("$ ");
		}
		printf("\n");
	}
}
