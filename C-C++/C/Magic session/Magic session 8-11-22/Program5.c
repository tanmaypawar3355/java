// WAP that accepts 2 strings from the user & concat the strings and print the concated string - Use myStrcat()
// I\p - Enter string 1 : yes i can
//       Enter string 2 : do it
// O\p - yes i can do it

#include<stdio.h>

   char* myStrcat(char* str1,char* str2){

	   while(*str1 != '\0'){
		   str1++;
	   }
	   while(*str2 != '\0'){
		   *str1 = *str2;
		   str1++;
		   str2++;
   }
	   *str1 = '\0';

	    return str1;
  }

void main(){
	char str1[20];
	char str2[20];

	printf("Enter string 1 : \n");
	gets(str1);
	printf("Enter string 2: \n");
	gets(str2);

	myStrcat(str1,str2);

 	
	puts(str1); 
}

		
