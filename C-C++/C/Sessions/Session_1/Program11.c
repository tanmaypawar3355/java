/* 1 2 3 4
   a b c d
   5 6 7 8
   e f g h  */

#include<stdio.h>
void main(){

	int rows;

	int num = 1;
	char ch = 97;

	printf("Enter rows\n");
	scanf("%d",&rows);

	for(int i = 0 ; i < rows ; i++){
		for(int j = 0 ; j < rows ; j++){

			if(i%2==0){
				printf("%d ",num);
				num++;
			}
			else{
				printf("%c ",ch);
				ch++;
			}	}
			printf("\n");
		}}
