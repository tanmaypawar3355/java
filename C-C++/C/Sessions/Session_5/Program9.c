/*                      4
                3       6
        2       4       6
1       2       3       4           */

#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows\n");
	scanf("%d",&rows);

	int num = rows;

	for(int i = 1 ; i <= rows ; i++){
		int temp = num;
		
		for(int sp = rows ; sp > i ; sp--){
			printf("\t");
		}
		for(int j = 1 ; j <= i ; j++){
			printf("%d\t",temp);
			temp = temp + num ;
		}
		printf("\n");
		num--;
		}}


