/*                          1
                    5       9
            13      17      21
    25      29      33      37               */


#include<stdio.h>
void main(){

	int rows;

        printf("Enter rows\n");
        scanf("%d",&rows);

	int num = 1;

	for(int i = 1 ; i <= rows ; i++){
		for(int sp = rows ; sp > i ; sp--){
			printf("\t");
		}
		for(int j = 1 ;j <=i ; j++){
			printf("%d\t",num);
			num = num + rows;
		}
		printf("\n");
	}}
