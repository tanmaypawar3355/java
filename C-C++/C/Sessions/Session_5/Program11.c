/*   Enter first number
     4
     Enter second number
     5
     Addition of factorial 4 & 5 is 144      */


#include<stdio.h>
void main(){

	int num1,num2;

	printf("Enter first number\n");
	scanf("%d",&num1);

	printf("Enter second number\n");
        scanf("%d",&num2);

	int fact1 = 1;

	for(int i = 1 ; i <= num1 ; i++){
		fact1 = fact1 * i;
	}
	
	int fact2 = 1;

        for(int i = 1 ; i <= num2 ; i++){
                fact2 = fact2 * i;
        }
       

	printf("Addition of factorial %d & %d is %d\n",num1,num2,fact1+fact2);
}
