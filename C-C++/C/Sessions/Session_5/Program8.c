/*          1
        A   b
    1   2   3
A   b   C   d    */

#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows\n");
	scanf("%d",&rows);


	for(int i = 1 ; i <= rows ; i++){
		int num = 1;
		char ch1 = 'A';
		char ch2 = 'a';

		for(int sp = rows ; sp > i ; sp--){
			printf("    ");
		}

		for(int j = 1 ; j <= i ; j++){
			if(i%2==1){
				printf("%d   ",num);
			}else if(j%2==1){
				printf("%c   ",ch1);
			}else{
				printf("%c   ",ch2);
			}
			ch1++;
			num++;			
			ch2++;
		}
			printf("\n");
	}}
