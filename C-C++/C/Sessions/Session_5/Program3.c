/*            d
           c  c
        b  b  b
     a  a  a  a            */

#include<stdio.h>
void main(){
	
	int rows;

        printf("Enter rows\n");
        scanf("%d",&rows);
	
	char ch = rows + 96;
	for(int i = 1 ; i <= rows ; i++){
		for(int sp = rows ; sp > i ; sp--){
			printf("   ");
		}
		for(int j = 1 ; j <= i ; j++){
			printf("%c  ",ch);
		
		}
		ch--;
		printf("\n");
	}}
