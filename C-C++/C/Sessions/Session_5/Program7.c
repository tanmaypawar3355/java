/*                                   A
                             b       a
                     C       E       G
             d       c       b       a
     E       G       I       K       M              */


#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows\n");
	scanf("%d",&rows);

	char ch1 = 'a';
	char ch2 = 'A';

	for(int i = 1 ; i <= rows ; i++){
		ch1='a'+i-1;
		ch2='A'+i-1;
		for(int sp = rows ; sp > i ; sp--){
			printf("\t");
		}
		for(int j = 1 ; j <= i ; j++){
			if(i%2==0){
				printf("%c\t",ch1);
				ch1--;		
			}else{
				printf("%c\t",ch2);
				ch2++;
				ch2++;
			}}
		printf("\n");		
	}}
