/*  D       D       D       D
            c       c       c
                    B       B
                            a             */


#include<stdio.h>
void main(){

        int rows;

        printf("Enter rows\n");
        scanf("%d",&rows);

	char ch1 = rows+96;
	char ch2 = rows+64;

        for(int i =  1; i <= rows; i ++){
                for(int sp = 1 ; sp <= i-1 ; sp++){
                        printf("\t");
                }
                for(int j = rows ; j>=i ; j--){
                        if(i%2==0){
				printf("%c\t",ch1);
			}else{
				printf("%c\t",ch2);
			}
                        }
                printf("\n");
		ch1--;
		ch2--;
        }}
