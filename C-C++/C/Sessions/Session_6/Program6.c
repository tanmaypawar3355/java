/*   1  2  3  4
        2  3  4
           3  4
              4            */

#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows\n");
	scanf("%d",&rows);

	for(int i =  1; i <= rows; i ++){
		int num = i;
		for(int sp = 1 ; sp <= i-1 ; sp++){
			printf("   ");
		}
		for(int j = rows ; j>=i ; j--){
			printf("%d  ",num);
			num++;
		}
		printf("\n");
	}}
