/*  1       2       3       4
    1       2       3
    1       2
    1                             */


#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows\n");
	scanf("%d",&rows);

	for(int i = rows ; i >= 1 ; i--){
		int num = 1;

		for(int j = 1 ; j <= i ; j++){
			printf("%d\t",num);

		num++;
		}
		printf("\n");
	}}
