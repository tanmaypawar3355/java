/* D       C       B       A
   C       B       A
   B       A
   A                               */


#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows\n");
	scanf("%d",&rows);
	
	char ch = rows + 64;

	for(int i = rows ; i >= 1 ; i--){
		for(int j = 1 ; j <= i ; j++){
			printf("%c\t",ch);
			ch--;
		}
		printf("\n");
		ch = ch + i -1;
	}
}
