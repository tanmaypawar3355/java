/*   factorial of 1 is 1
     factorial of 2 is 2
     factorial of 3 is 6
     factorial of 4 is 24
     factorial of 5 is 120     */



#include<stdio.h>
void main(){

	int start;
	int end;

	printf("Enter starting number\n");
	scanf("%d",&start);

	printf("Enter ending number\n");
	scanf("%d",&end);

	int fact = 1;

	for(int i = start; i <= end ; i++){
	 fact = fact*i;
		
	printf("factorial of %d is %d\n",i,fact);
	}}
