/*  1       2       3       4
    4       5       6
    6       7
    7                                  */


#include<stdio.h>
void main(){
	int rows;

	printf("Enter rows\n");
	scanf("%d",&rows);
	
	int num = 1;

	for(int i = rows ; i >= 1 ; i--){
		for(int j = 1 ; j <= i ; j++){

			printf("%d\t",num);
			num++;
		}
		printf("\n");
		num = num-1;
	}}
