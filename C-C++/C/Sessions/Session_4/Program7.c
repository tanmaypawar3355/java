/*  1     2     3     4     5
    2     3     4     5
    3     4     5
    4     5
    5                               */



#include<stdio.h>
void main(){

	int rows;

	printf("Enter rows\n");
	scanf("%d",&rows);

	int num = 1;

	for(int i = rows ; i >= 1 ; i--){
		for(int j = 1 ; j <= i ; j++){
			printf("%d     ",num);
		num++;
		}
		printf("\n");
		num=num-i+1;
	}}
