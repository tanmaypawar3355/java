/*       4
      3  3  3
   2  2  2  2  2
1  1  1  1  1  1  1    */


#include<stdio.h>
void main(){

        int rows;

        printf("Enter rows\n");
        scanf("%d",&rows);

	int num = rows;

	for(int i = 1 ; i <= rows ; i++){
		for(int sp = rows-1 ; sp >= i ; sp--){
			printf("   ");
		}
		for(int j = 1 ; j <= 2*i-1 ; j++){
			printf("%d  ",num);
		}
		printf("\n");
		num--;
	}}
