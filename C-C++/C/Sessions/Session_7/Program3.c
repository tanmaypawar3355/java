/*            1
           1  2  1
        1  2  3  2  1
     1  2  3  4  3  2  1           */


#include<stdio.h>
void main(){

        int rows;

        printf("Enter rows\n");
        scanf("%d",&rows);

	int num = 1;

	for(int i = 1 ; i <= rows ; i++){
		int num = 1;
		for(int sp = rows-1 ; sp >= i ; sp--){
			printf("   ");
		}
		for(int j = 1 ; j <= 2*i-1 ; j++){
			if(j<i){
				printf("%d  ",num);
				num++;
			}else{
				printf("%d  ",num);
				num--;
			}
		}
		printf("\n");
	}}
