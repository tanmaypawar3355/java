/*            d
           C  C  C
        b  b  b  b  b
     A  A  A  A  A  A  A            */

#include<stdio.h>
void main(){

        int rows;

        printf("Enter rows\n");
        scanf("%d",&rows);

	char ch1 = rows + 96;
	char ch2 = rows + 64;

	for(int i = 1 ; i <= rows ; i++){
		for(int sp = rows-1 ; sp >= i ; sp--){
			printf("   ");
		}
		for(int j = 1 ; j <= 2*i-1 ; j++){
			if(i%2==1){
				printf("%c  ",ch1);
			}else{
				printf("%c  ",ch2);
			}}
		printf("\n");
		ch1--;
		ch2--;
	}}
