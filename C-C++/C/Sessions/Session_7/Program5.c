/*            d
           c  c  c
        b  b  b  b  b
     a  a  a  a  a  a  a       */

#include<stdio.h>
void main(){

        int rows;

        printf("Enter rows\n");
        scanf("%d",&rows);

	char ch1 = rows + 96;

	for(int i = 1 ; i <= rows ; i++) {
		for(int sp = rows-1 ; sp >= i ; sp--){
			printf("   ");
		}
		for(int j = 1 ; j <= i*2-1 ; j++){
			printf("%c  ",ch1);
		}
		printf("\n");
		ch1--;
	}}

