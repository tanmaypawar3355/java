/*  3. WAP that dynamically allocates a 2-D Array of integers, takes value from
       the user, and prints it. [use malloc()]
       rows=3
       column=4
       Draw a diagram on a notebook         */


#include<stdio.h>
#include<stdlib.h>

void main(){
        int row=3,col=4,num=1;

        int (*ptr)[col] = malloc((sizeof(int))*row*col);

        int temp;

        printf("Enter marks\n");

        for(int i = 0 ; i < row ; i++){

                for(int j = 0 ; j < col ; j++){

                        *(*(ptr+i)+j)=num++;

                }}

        for(int i = 0 ; i < row ; i++){

                for(int j = 0 ; j < col ; j++){

                        printf("%d\t",ptr[i][j]);

        }
                printf("\n");

        }
}

