/*  4. WAP that dynamically allocates a 2-D Array of integers, takes value from
       the user, and prints it. [use malloc()]
       plane=2
       rows=3
       column=4
       Draw a diagram on a notebook             */


#include<stdio.h>
#include<stdlib.h>

void main(){
        int plane=2,row=3,col=4,num=1;

        int (*ptr)[plane][col] = malloc((sizeof(int))*row*col);

        int temp;

        printf("Enter marks\n");

        for(int i = 0 ; i < plane ; i++){

                for(int j = 0 ; j < row ; j++){

			for(int k = 0 ; k < col ; k++){

                        *(*(*(ptr+i)+j)+k)=num++;

			}}}

        for(int i = 0 ; i < plane ; i++){

                for(int j = 0 ; j < row ; j++){

			for(int k = 0 ; k < col ; k++){

                        printf("%d\t",ptr[i][j][k]);

        }
                printf("\n");

        }}}
