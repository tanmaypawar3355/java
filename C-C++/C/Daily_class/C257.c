//   1 . strlen (string length)
//   

/* USING LIBRARY FUNCTION
 

#include<stdio.h>
#include<string.h>

void main(){
	char name[]= {'M','s','d','h','o','n','i','\0'};

	char *str = "Virat Kohli";

	int lenname = strlen(name);
	int lenstr  = strlen(str);

	printf("%d\n",lenname);
	printf("%d\n",lenstr);
}

*/
// WITHOUT USING LIBRARY FUNCTION


#include<stdio.h>

int mystrlen(char *str){

	int count = 0;

	while(*str != '\0'){
		str++;
		count++;
	}
	return count;
}

void main(){
	char name[]= {'M','s','d','h','o','n','i','\0'};

	char *str = "Virat Kohli";

	mystrlen(name);

	printf("%d\n",mystrlen(name));

	mystrlen(str);

	printf("%d\n",mystrlen(str));

}
