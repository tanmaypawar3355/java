#include<stdio.h>
void callbyAddress(int*);

void main(){
	int x = 10;
	printf("%d\n",x);

	callbyAddress(&x);

	printf("%d\n",x);
}

void callbyAddress(int *ptr){

	printf("%p\n",ptr);
	printf("%d\n",*ptr);

	*ptr = 50;

	printf("%d\n",*ptr);
}


