#include<stdio.h>
void pointer(int *,int *);

void main(){
	int x = 10;
	int y = 20;

	printf("x = %d\n",x);
	printf("y = %d\n",y);

	pointer(&x,&y);

	printf("x = %d\n",x);
	printf("y = %d\n",y);
}

void pointer(int *x,int *y){

	printf("x = %d\n",*x);
	printf("y = %d\n",*y);

	int temp;

	temp = *x;
	*x = *y;
	*y = temp;

	printf("x = %d\n",*x);
	printf("y = %d\n",*y);
}
	
