// Arr kadhich akkha eka veles function mdhi pthvta yet nahi. Tyala 2 ways astat
// 1 . Araay chi size dyaavi lagel  
// 2 . &arr dyav lagel
 
#include<stdio.h>

// Way2
void printArr1(int *ptr ,int arrsize){

	for(int i = 0 ; i < arrsize ; i++){
		printf("%d\n",*(ptr+i));
	}
}

//Way1
void printArr2(int arr[ ] , int arrsize){

	for(int i = 0 ; i < arrsize ; i ++){
		printf("%d\n",arr[i]);
	}
}

void main(){

	int arr[] = {10,20,30,40,50};

	int arrsize = sizeof(arr)/sizeof(int);

	printArr1(arr,arrsize);
	printArr2(arr,arrsize);
}	


