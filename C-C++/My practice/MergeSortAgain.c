#include<stdio.h>


void sort(int arr[],int start,int mid,int end){

	int size1 = mid - start + 1;
	int size2 = end - mid;

	int somya[size1];
	int gomya[size2];
	
	for(int i = 0 ; i < size1 ; i++){

		somya[i] = arr[start + i];
	}

	for(int j = 0 ; j < size2 ; j++){

		gomya[j] = arr[mid + 1 + j];
	}

	int itr1 = 0, itr2 = 0, itr3 = start;

	while(itr1 < size1 && itr2 < size2){

		if(somya[itr1] < gomya[itr2]){

			arr[itr3] = somya[itr1];
			itr1++;
		
		}else{
			arr[itr3] = gomya[itr2];
			itr2++;
		}
		itr3++;
	}

	while(itr1 < size1){

		arr[itr3] = somya[itr1];
		itr1++;
		itr3++;
	}

	while(itr2 < size2){
		
		arr[itr3] = gomya[itr2];
		itr2++;
		itr3++;
	}
}

void rec(int arr[],int start,int end){

        if(start < end){

                int mid = (start + end) / 2;

                rec(arr,start,mid);
                rec(arr,mid+1,end);
                sort(arr,start,mid,end);
        }
}

void main(){

	int arr[] = {8,5,6,-3,-8,-7,-1,0,4,9,2,4};

	int N = sizeof(arr)/sizeof(int);

	int start = 0;
	int end  = N - 1;

	rec(arr,start,end);

	for(int i = 0 ; i < N ; i++){
		printf("|%d|",arr[i]);
	}
	printf("\n");

}
