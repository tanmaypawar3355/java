/*
Q5 write a program to print the following pattern
 
Row =4

0
1 1
2 3 5
8 13 21 34

USE THIS FOR LOOP STRICTLY for the outer loop
Int row;
Take the number of rows from user
	for(int i =1;i<=row;i++){
}

*/

import java.io.*;

class Demo{

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                int row = Integer.parseInt(br.readLine());

		int a = 0;
		int b = 1;
		int c;

		for(int i = 1 ; i<= row ; i++){

			for(int j = 1 ; j <= i ; j++){

				System.out.print(a + "   ");
				c = a + b;
				a = b;
				b = c;
			}
			System.out.println();
		}
	}
}
