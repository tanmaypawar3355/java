/*
Q3 write a program to print the following pattern

5 4 3 2 1
8 6 4 2
9 6 3
8 4
5

USE THIS FOR LOOP STRICTLY for the outer loop
Int row;
Take the number of rows from user
	for(int i =1;i<=row;i++){
}

*/
import java.io.*;

class Demo{

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                int row = Integer.parseInt(br.readLine());

		int num = 1;

		for(int i = row ; i >= 1 ; i--){

			int num1 = num * i;

			for(int j = 1 ; j <= i ; j++){

				System.out.print(num1+ "   ");
				num1 = num1 - num;
			}
			System.out.println();
			num++;
		}
	}
}
