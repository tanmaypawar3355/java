/*

Q9 Write a program to take a number as input and print the Addition of Factorials of each
   digit from that number.
	
	Input: 1234
	Output: Addition of factorials of each digit from 1234 = 33

*/

import java.io.*;

class Demo{

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                int num = Integer.parseInt(br.readLine());
		int sum = 0;

		while(num != 0){

			int rem = num % 10;
			int mult = 1;

			for(int i = rem ; i >= 1 ; i--){

				mult = mult * i;
			}

			sum = sum + mult;
			num = num / 10;
		}

		System.out.println(sum);
	}
}
			



