class SumMulti{
	
	public static void main(String[] args){

	int sum = 0;
	int multi = 1;

	int num = 1;

	while(num != 11){

		if(num % 2 == 0){

			sum = sum + num;
		
		}else{
			multi = multi * num;
		}
		num++;
	}
	System.out.println("Sum of all even numbers is = "+sum);
	System.out.println("Miltiplication of odd numbers is " +multi);
	}
}
/*

Q.6 Write a program to print the sum of all even numbers & multi of all odd numbers betwn 1-10.

	o/p = Sum of even betwn 1 to 10 = 30
	      Multi of odd betwn 1 to 10 = 945
*/
