class Square{

	public static void main(String[] args){

		int num = 942111423;

		while(num > 0){

			int rem = num % 10;

			if(rem % 2 == 0){

				System.out.print(rem * rem);
			}
			num = num / 10;
		}
	}
}

/*
 
Q.5 Write a program to print the square of even digits of the given number.
   i/p = 942111423
   o/p = 4  16  4  16

*/
