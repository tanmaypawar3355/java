class OddDigits{

	public static void main(String[] args){

		int num = 942111423;
		int count = 0;

		while(num != 0){

			int rem = num % 10;

			if(rem % 2 == 1){
				count++;
			}
			num = num /10;
		}
		System.out.println("Count = "+count);
	}
}

/*
 
   Q.4 Write a program to count the odd digits of the given number.
  	i/p = 942111423
        o/p = 5

*/	
