class Pali{

	public static void main(String[] args){

		int num = 2332;
		int pali = num;
		int sum = 0;

		while(num != 0){

			int rem = num % 10;

			sum = sum * 10 + rem;

			num = num / 10;
		}

		if(pali == sum){

			System.out.println("Number is palindrome");

		}else{
			System.out.println("Number is not a palindrome");
		}
	}
}

/*
 
   Q.10 Write a program to check palindrome or not

   i/p = 2332

*/
