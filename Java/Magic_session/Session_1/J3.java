class Digit{

        public static void main(String[] args){

                int num = 942111423;
                int count = 0;

                while(num != 0){                // num >= 0

                        count++;
                        num = num / 10;
                }
                System.out.println("Count = "+count);
        }
}

/*

Q.3 count the digits of the given number.
   i/p = 942111423
   o/p = 9

*/
