class Reverse{

	public static void main(String[] args){

		int num = 7;
		int x = num;

		while(x >= 1){                         // x != 0 nhi chlnar karan 1 nantr x chi value -1 hoti ani to infinite loop mdhi jato

			if(num % 2 == 0){
				
				System.out.println(x);
				x--;
			
			}else{
				System.out.println(x);
				x = x - 2;
			}
		}
	}
}

/*
 
Q.7 Write a program which takes number from user if number is even print that no. in reverse or if number is odd print 
   that no. in reverse order by the diff. of 2.

   i/p = 6
   o/p = 6 5 4 3 2 1

   i/p = 7
   o/p = 7 5 3 1

*/
