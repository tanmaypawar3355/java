class Reverse{

	public static void main(String[] args){

		int num = 942111423;
		int sum = 0;

		while(num != 0){

			int rem = num % 10;
			sum = sum * 10 + rem;

			num = num / 10;
		}
		System.out.println("Number in reverse = " +sum);
	}
}

/*
Q.9 Write a program to reverse the number.

	i/p = 942111423
	o/p = 324111249

*/
			
