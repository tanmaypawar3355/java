/*
 
Q4
Write a program to take a range as input from the user and print perfect cubes between that range.
Input: Enter start: 1
Enter end: 100
Output: perfect cubes between 1 and 100

*/


import java.io.*;

class RangeCompositeNumber {

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Enter lower limit : ");
                int start = Integer.parseInt(br.readLine());

                System.out.print("Enter upper limit : ");
                int end = Integer.parseInt(br.readLine());


                for(int i = start ; i <= end ; i++){

                        System.out.print(i*i*i + "  ");
                }
        }
}
