/*    2. WAP to reverse the string without using inbuilt method.   */

import java.io.*;

class StringBufferDemo {

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String str1 = br.readLine();

		char arr[] = str1.toCharArray();

		int num1 = 0;
		int num2 = arr.length-1;
		int count = arr.length / 2;

		while(count != 0){

			char temp = arr[num1];
			arr[num1] = arr[num2];
			arr[num2] = temp;
			num1++;
			num2--;
			count--;
		}
		for(int i = 0 ; i < arr.length ; i++) {

			System.out.print(arr[i]+ " ");
		}
	}
}
