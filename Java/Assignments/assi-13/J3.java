/*

3. WAP to count the characters in given string. 
Without using in built method.
Ex. "CORE2WEB"
COUNT : 8

*/

import java.io.*;

class StringBufferDemo {

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                String str1 = br.readLine();

                char arr[] = str1.toCharArray();

		int count = 0;

		for(int i = 0 ; i < arr.length ; i++) {

			count++;
		}

		System.out.println(count);
	}
}
