/*  1. WAP to take input from user in StringBuffer and print it.     */

import java.io.*;

class StringBufferDemo {

	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		StringBuffer str1 = new StringBuffer(br.readLine());

		System.out.println(str1);
	}
}
