/*

3. Search Insert Position (LeetCode-35)
Given a sorted array of distinct integers and a target value, return the index
if the target is found. If not, return the index where it would be if it were
inserted in order.
You must write an algorithm with O(log n) runtime complexity.
Example 1:
Input: nums = [1,3,5,6], target = 5
Output: 2
Example 2:
Input: nums = [1,3,5,6], target = 2
Output: 1
Example 3:
Input: nums = [1,3,5,6], target = 7
Output: 4
Constraints:
1 <= nums.length <= 104
-104 <= nums[i] <= 104
nums contains distinct values sorted in ascending order.
-104 <= target <= 104



*/

import java.io.*;

class Output {

	static int method(int arr[],int target) {

		//if(target > arr[arr.length-1]) 
			//return arr.length;

		for(int i = 0 ; i < arr.length ; i++) {

			for(int j = i ; j <= i+1 ; j++) {

				if(arr[0] > target) 
					
					return 0;
				
				else if(arr[i] == target) 

					return i;
				
				else if(arr[j] == target) 

					return j;
				
				
				else if(arr[i] < target && arr[j] > target) 

					return j;
			}
		}

		return arr.length;
	}
}

class SearchInsertPosition {

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of array");
                int num = Integer.parseInt(br.readLine());

                int arr[] = new int[num];

                System.out.println("Enter array elements");

                for(int i = 0 ; i < arr.length ; i++) {

                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("Enter target");
                int target = Integer.parseInt(br.readLine());

		int num1 = -1;

		try {
			num1 = Output.method(arr,target);
		} catch(ArrayIndexOutOfBoundsException obj) {

			System.out.println("Index would be at " + arr.length);
		}

		if(num1 != -1) 
			System.out.println("Index would be at " + num1);
	}
}
