/*

1. Reverse Integer (Leetcode:- 7)
Given a signed 32-bit integer x, return x with its digits reversed. If reversing
x causes the value to go outside the signed 32-bit integer range [-231, 231
- 1], then return 0.
Assume the environment does not allow you to store 64-bit integers (signed
or unsigned).
Example 1:
Input: x = 123
Output: 321
Example 2:
Input: x = -123
Output: -321
Example 3:
Input: x = 120
Output: 21
Constraints:
-231 <= x <= 231 - 1

*/


import java.io.*;

class Output {

	static int method(int num) {

		int num1 = num;
		int sum = 0;

		while(num != 0) {

			int rem = num % 10;
			sum = (sum * 10) + rem;
			num = num / 10;
		}

		System.out.println("Sum = " + sum);

		if(sum <= -231 && sum >= 231 - 1) {

			return 0;

		} else {
			return 1;
		}
	}
}

class ReverseInteger {

	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int num = Integer.parseInt(br.readLine());

		Output.method(num);
	}
}

