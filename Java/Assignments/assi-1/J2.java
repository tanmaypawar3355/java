class LessGreater{

	public static void main(String[] args){

		int var = 10;

		if(var > 10){

			System.out.println(var + " is greater than 10");

		}else if(var < 10){

			System.out.println(var + " is less than 10");

		}else{
			System.out.println(var + " is same means 10");
		}
	}
}

/*

  Program 2: Write a java program, take a number, and print whether is is less than 10 or gr		 eater than 10.

  	     Input: var = 5
	     Output: 5 is less than 10
	     Input: var = 16
             Output: 16 is greater than 10
	     Input: var = 10
             Output: ???


*/

