class DayOfWeek{

	public static void main(String[] args){

		int x = 6;

		if(x == 1){

			System.out.println("Monday");

		}else if(x == 2){

                        System.out.println("Tuesday");

                }else if(x == 3){

                        System.out.println("Wednesday");

                }else if(x == 4){

                        System.out.println("Thursday");

                }else if(x == 5){

                        System.out.println("Friday");

                }else if(x == 6){

                        System.out.println("Saturday");

                }else if(x == 7){

                        System.out.println("Sunday");

                }else{
			System.out.println("Invalid input");
		}
	}
}

/*
 

   Program 8: Write a prgram to check day number(1-7) and print the corresponding day of week

   	     Input1 : 1
	     Output : Monday 
   	     
	     Input1 : 6
	     Output : Saturday 
   	     
	     Input1 : 8
	     Output : ????

*/ 
