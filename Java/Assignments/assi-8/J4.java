class NestedFor{

        public static void main(String[] args){

                int N = 4;

                for(int i = 1 ; i <= N ; i++){
                	
			int num = i;

                        for(int j = 1 ; j <= N-i+1 ; j++){

                                System.out.print(num++ +"  ");
                        }
                        System.out.println();
                }
        }
}
/*

Q4 write a program to print the following pattern

1 2 3 4
2 3 4
3 4
4

	int row=4;
	for(int i =1;i<=row;i++){
	
	}

*/
