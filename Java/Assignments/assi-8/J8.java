class NestedFor{

        public static void main(String[] args){

                int N = 4;
                int num = N*(N+1)/2;
		int ch = 64 + N*(N+1)/2;

                for(int i = 1 ; i <= N ; i++){
 

                        for(int j = 1 ; j <= i ; j++){

                                if(i % 2 == 0){

                                        System.out.print((char)(ch) +"  ");

                                }else{
                                        System.out.print(num +"  ");
                                }
				ch--;
				num--;
                        }
                        System.out.println();
                }
        }
}

/*

Q8 write a program to print the following pattern

10
I H
7 6 5
D C B A

	USE THIS FOR LOOP STRICTLY for the outer loop
	
	Int row=4;
	
	for(int i =1;i<=row;i++){
	
	}

*/
