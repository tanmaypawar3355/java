/*

Q3
Write a program in which user should enter two numbers if both the numbers are positive
multiply them and provide to switch case to verify number is even or odd, if user enters any
negative number program should terminate saying “Sorry negative numbers not allowed”

*/



import java.io.*;

class SwitchDemo {

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                int num1 = Integer.parseInt(br.readLine());
                int num2 = Integer.parseInt(br.readLine());

		int num3 = 0;

		if(num1 > 0 && num2 > 0){
			num3 = num1 * num2;
		}
		
		if(num1 < 0 || num2 < 0){
			System.out.println("Sorry negative number sare not allowed\n");
		}

		int num4 = num3 % 2;
		
		switch(num4){

			case 1 :
				System.out.println("Number is even\n");
				break;

			default :
				System.out.println("Number is odd\n");
				break;
		}
	}
}


	
