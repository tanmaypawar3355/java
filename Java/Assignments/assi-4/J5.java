/*

5. Write a real time time example which shows a index like above code
Also solve all codes using if else ladder

*/

import java.io.*;

class SwitchDemo {

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter your destination");
		String str = br.readLine();

		switch(str){

			case "Shivajinagar" :
				System.out.println("Available routes are");				
				System.out.println("1.katraj-balajinagar-swargate-shivaji-maharaj-nagar");
				System.out.println("2.katraj-warje-kothrud-shivaji-maharaj-nagar");
				break;

			case "katraj" :

				System.out.println("Available routes are");					
				System.out.println("1.shivaji-maharaj-nagar-swargate-balajinagar-katraj");
				System.out.println("2.shivaji-maharaj-nagar-kothrud-warje-katraj");
				break;

			default :

				System.out.println("We dont provide servies for this destination");
				break;
		}
	}
}
						



