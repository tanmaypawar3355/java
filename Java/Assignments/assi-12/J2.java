import java.io.*;

class ReverseEle{

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array elements");

                for(int i = 0 ; i < arr.length ; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

		reverseElement(arr);
	}

	static void reverseElement(int arr[]){

		for(int i = 0 ; i < arr.length ; i++){

			int num = arr[i];
			int ret = 0;
	
			while(num != 0){

				int rem = num % 10;
				ret = ret * 10 + rem;
				num = num / 10;
			}
			System.out.println(ret + "  ");
		}
	}
}

