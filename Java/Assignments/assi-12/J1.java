import java.io.*;

class CountDigit{

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array elements");

                for(int i = 0 ; i < arr.length ; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

		countDigit(arr);

	}

	static void countDigit(int arr[]){

		for(int i = 0 ; i < arr.length ; i++){

			int num = arr[i];
			int count = 0;
			//int count1 = 0;

			while(num != 0){
				count++;
				num = num / 10;                            
			}
			System.out.println(count + "  ");
		}
	}
}








