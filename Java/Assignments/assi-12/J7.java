/*
Program 7
WAP to find a Strong number from an array and return its index.
Take size and elements from the user
	Input: 10 25 252 36 564 145
	Output: Strong no 145 found at index: 5

*/

import java.io.*;

class StrorngNumber{

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array elements");

                for(int i = 0 ; i < arr.length ; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

		strongNumber(arr);
	}

	static void strongNumber(int arr[]){

		for(int i = 0 ; i < arr.length ; i++){

			int num = arr[i];
			int sum = 0;

			while(num != 0){
				int rem = num % 10;
				int product = 1;
	
				for(int j = 1 ; j <= rem ; j++){

					product = product * j;
				}
				sum = sum + product;
				num = num /10;
			}
			if(sum == arr[i]){
				System.out.println("Strong no " +arr[i]+" found at index " +i);
			}
		}
	}
}
