/*
Program 9
Write a program to print the second max element in the array
	Input: Enter array elements: 2 255 2 1554 15 65
	Output: 255

*/

import java.io.*;

class SecondMaxEle{

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array elements");

                for(int i = 0 ; i < arr.length ; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

		secondMaxElement(arr);
	}

	static void secondMaxElement(int arr[]){

		int max = 0;
		int secondMax = 0;

		for(int i = 0 ; i < arr.length ; i++){

			//int num = arr[i-1];

			if(max < arr[i]){
				secondMax = max;
				max = arr[i];
			}
		}

		System.out.println("Second max element = "+ secondMax);
	}
}


