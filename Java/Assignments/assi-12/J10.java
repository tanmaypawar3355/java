import java.io.*;

class SecondMaxEle{

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array elements");

                for(int i = 0 ; i < arr.length ; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

                secondMinElement(arr);
        }

        static void secondMinElement(int arr[]){

		for(int i = 0 ; i < arr.length ; i++){

			int count = 0;
			int take = arr[i];

			for(int j = 0 ; j < arr.length ; j++){

				if(arr[j] < take){
					count++;
				}
			}
			if(count == 1){
				System.out.println("Second minimun element = " + arr[i]);
			}
		}		
	}
}

