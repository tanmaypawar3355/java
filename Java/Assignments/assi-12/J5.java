/*
Program 5
WAP to find a Perfect number from an array and return its index.
Take size and elements from the user
	Input: 10 25 252 496 564
	Output: Perfect no 496 found at index: 3
*/
import java.io.*;

class PerfectNumber{

	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter array size");

		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements");

		for(int i = 0 ; i < arr.length ; i++){

			arr[i] = Integer.parseInt(br.readLine());
		}
		System.out.println("In function");
		perfectNo(arr);
	}

	static void perfectNo(int arr[]){
	

		for(int i = 0 ; i < arr.length ; i++){

			int num = arr[i];
			int sum = 0;

			for(int j = 1 ; j < num ; j++){

				if(num % j == 0){

					sum = sum + j;
				}
			}
			if(sum == arr[i]){
				System.out.println("Perfect no " + arr[i] + " found at index " + i);
			}
		}
	}
}
	

