import java.io.*;

class PallindromeNo{

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size");

                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array elements");

                for(int i = 0 ; i < arr.length ; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

                for(int i = 0 ; i < arr.length ; i++){

                        int num = arr[i];
                        int rem = 0;


                        while(num != 0){

                                int ret = num % 10;
                                rem = rem * 10 + ret;
                                num = num / 10;
                        }
                        if(arr[i] == rem){
                                System.out.println("Pallindrome string " + arr[i] + " found at index " + i);
                        }
                }
        }
}

