/*
 
6. WAP to take input using string class and count the vowels and consonants in the given string.

*/

import java.io.*;

class Pattern {

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter string");
                String str = br.readLine();

		char arr1[] = str.toCharArray();

		int count = 0;

		char arr2[] = {'a','e','i','o','u','A','E','I','O','U'};

		for(int i = 0 ; i < arr1.length ; i++) {

			for(int j = 0 ; j < arr2.length ; j++) {

				if(arr1[i] == arr2[j]) {
				       
					count++;
					break;
				}
			}
		}
		
		System.out.println("Count of vowels = " + count);
		System.out.println("Count of consonants = " + (arr1.length-count));
	}
}

