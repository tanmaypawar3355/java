import java.io.*;

class Pattern {

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter how many lines you want");
                int num = Integer.parseInt(br.readLine());

		int num2 = (num /2) + 1;

		for(int i = 0 ; i <= num ; i++) {

			for(int j = 0 ; j < i ; j++) {

				System.out.print("*   ");
			}
			System.out.println();
		}
	}
}

