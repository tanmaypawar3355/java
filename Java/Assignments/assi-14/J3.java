/*

3. WAP to print the pattern-
 a
 A  B
 a  b  c
 A  B  C  D
 
*/

import java.io.*;

class Pattern {

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter how many lines you want");
		int num = Integer.parseInt(br.readLine());
		
		
		for(int i = 0 ; i <= num ; i++) {

			char ch1 = 'a';
                	char ch2 = 'A';

			for(int j = 0 ; j < i ; j++) {

				if(i % 2 != 0){
	
					System.out.print(ch1++ + "  ");
				}else{
					System.out.print(ch2++ + "  ");
				}
			}
			System.out.println();
		}
	}
}

