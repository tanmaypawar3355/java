/*
2. WAP to take input from user to check whether given string is palindrome string or not.
*/
import java.io.*;

class StringPalindrome {

	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));


				String str = br.readLine();

				char arr[] = str.toCharArray();

				int count1 = (arr.length) - 1;
				int count2 = count1 / 2;

				int i = 0; 
				int j = (arr.length) - 1;
				int flag= 0;

				while(count2 != 0) {

					if(arr[i] == arr[j] || (arr[i] - arr[j]) == 32 || (arr[i] - arr[j] == -32)){
						count2--;
					}else{
						flag = 1;
						break;
					}
				}
				
				if(flag == 0){
					System.out.println("String is palindrome");
				}else{
					System.out.println("String is not palindrome");
				}

	}
}

