/*
Program 4
WAP to search a specific element from an array and return its index.
	Input: 1 2 4 5 6
	Enter element to search: 4
	Output: element found at index: 2

*/

import java.io.*;

class searchEle{

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of array");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];
                
                System.out.println("Enter array elements");
                for(int i = 0 ; i < arr.length ; i++){

                        arr[i] = Integer.parseInt(br.readLine());

		}

		System.out.println("Enter element to search");
		int ele = Integer.parseInt(br.readLine());
		
		searchEle obj = new searchEle();	
		int ret = obj.searchElement(arr,ele);

		System.out.println("element found at index = " +ret);
	}

	int searchElement(int arr[],int ele){

		int num = 0;

		for(int i = 0 ; i < arr.length ; i++){

			if(arr[i] == ele){
				num = i;
				break;
			}
		}
		return num;
	}
}


