class NestedFor{

	public static void main(String[] args){

		//char ch = 'F';

		for(int i = 1 ; i<= 6 ; i++){
			
			char ch = 'F';

			for(int j = 1 ; j <= 6 ; j++){

				if(j % 2 == 1){
		
					System.out.print(ch + "   ");
				
				}else{
					System.out.print(ch-64 + "    ");
				}
				ch--;
			}
			System.out.println();
		}
	}
}

/* Q10 write a program to print the following pattern
	F 5 D 3 B 1
	F 5 D 3 B 1
	F 5 D 3 B 1
	F 5 D 3 B 1
	F 5 D 3 B 1
	F 5 D 3 B 1
	
	USE THIS FOR LOOP STRICTLY
		
	for(int i =1;i<=6;i++){
		
		for(j=1;j<=6;j++){
		
		}
	}

*/
