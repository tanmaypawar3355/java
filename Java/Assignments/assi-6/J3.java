class NestedFor{

	public static void main(String[] args){

		for(int i = 1 ; i <= 4 ; i++){

			for(int j = 1 ; j <= 4 ; j++){

				System.out.print(j+12+i +"    ");
			}
			System.out.println();
		}
	}
}

/*

Q3 write a program to print the following pattern
	14 15 16 17
	15 16 17 18
	16 17 18 19
	17 18 19 20
	
	USE THIS FOR LOOP STRICTLY
	
	for(int i =1;i<=4;i++){
		
		for(j=1;j<=4;j++){
	
		}
	}	

*/
