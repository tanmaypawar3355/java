class NestedFor{

	public static void main(String[] args){

		for(int i = 1 ; i <= 4 ; i++){

			int num = i;

			for(int j = 0001 ; j <= 4 ; j++){
				
				System.out.print(num++ +"  ");
			}
			System.out.println();
		}
	}
}


/*
 
Q10 write a program to print the following pattern
	1 2 3 4
	2 3 4 5
	3 4 5 6
	4 5 6 7
	
	USE THIS FOR LOOP STRICTLY
	for(int i =1;i<=4;i++){
		
		for(j=1;j<=4;j++){

		}			
	}
	HINT: row start and values of i are the same

*/
