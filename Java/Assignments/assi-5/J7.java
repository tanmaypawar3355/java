class NestedFor{

        public static void main(String[] args){

                for(int i = 1 ; i <= 3 ; i++){

                        for(int j = 1 ; j <= 3 ; j++){

                                System.out.print("D  ");
                        }

                        System.out.println();
                }
        }
}


/*

Q7 write a program to print the following pattern
	
	D D D D
	D D D D
	D D D D
	D D D D

*/
