class Count{

	public static void main(String[] args){

		int num = 942111423;
		int count = 0;

		while(num != 0){

			count++;			
			num = num / 10;
		}
		System.out.println(count);
	}
}

/*
 

   Program 3: Write a program to count the digits of the given number.
   Input: 942111423
   Output: count of digits = 9


*/
 
   
