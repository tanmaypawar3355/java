class EvenOdd{

	public static void main(String[] args){

		int num = 7;

		for(int i = num ; i >= 1 ;    ){

			if(num % 2 == 0){

				System.out.print(i + " ");
				i--;
			
			}else{
				System.out.print(i + " ");
				i = i - 2;
			}
		}
	}
}

/*
 
Program 7: Write a program which take’s number from user’s if number is even
print that number in reverse order or if number is odd print that number in
reverse order by difference two?
Input:6
output:6 5 4 3 2 1
Input:7
output:7 5 3 1.


*/
