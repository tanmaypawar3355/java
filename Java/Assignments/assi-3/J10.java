class Pali{

	public static void main(String[] args){

		int num = 2332;

		int temp = num;
		int rev = 0;

		while(num != 0){

                        int rem = num % 10;
                        rev = rev * 10 + rem;
                        num = num / 10;
                }

		if(rev == temp){

			System.out.println(temp+ " is a palindrome string");

		}else{
			System.out.println(temp+ " is not a palindrome string");
		}
	}
}

/*

Program 10: Write a program to check whether the number is a Palindrome
number or not. (2332)
Output: 2332 is a palindrome number

*/


