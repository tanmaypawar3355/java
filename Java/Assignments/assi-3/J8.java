class Assignment{

	public static void main(String[] args){

		int day = 7;

		for(int i = day ; i >= 1 ; i--){

			System.out.println(i +" days remaining");
		}
		
		System.out.println("0 days Assignment is Overdue");
	}
}

/*

Program 8: Write a program to print the countdown of days to submit the
assignment
Input : day = 7
Output: 7 days remaining
6 days remaining
5 days remaining
.
.
1 days remaining
0 days Assignment is Overdue


*/
