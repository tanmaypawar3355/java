class NestedFor{

        public static void main(String[] args){

                int N = 4;
                int num = N*(N+1)/2;

                for(int i = 1 ; i <= N ; i++){

                        for(int j = 1 ; j <= i ; j++){

                                System.out.print(num-- + "  ");
                        }
                        System.out.println();
                }
        }
}

/*

Q3 write a program to print the following pattern

10
9 8
7 6 5
4 3 2 1

	USE THIS FOR LOOP STRICTLY for the outer loop
	
	int row=4;
	int num=10;
	
	for(int i =1;i<=4;i++){
	
	}

*/
