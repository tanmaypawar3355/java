class NestedFor{

	public static void main(String[] args){

		int num = 2;
		int N = 4;

		for(int i = 1 ; i <= N ; i++){

			num--;

			for(int j = 1 ; j <= N-i+1 ; j++){

				System.out.print(num++ +"  ");
			}
			System.out.println();
		}
	}
}
/*

Q10 write a program to print the following pattern

1 2 3 4
4 5 6
6 7
7

	USE THIS FOR LOOP STRICTLY
	
	for(int i =1;i<=4;i++){
	
	}

*/
