class NestedFor{

        public static void main(String[] args){

                int N = 6;
		int ch = 64 + N;

                for(int i = 1 ; i <= N ; i++){

			int temp = ch;

                        for(int j = 1 ; j <= i ; j++){

                                System.out.print((char)(temp++) +"  ");
                        }
                        System.out.println();
			ch--;
  		}
        }
}

/*

Q7 write a program to print the following pattern

F
E F
D E F
C D E F
B C D E F
A B C D E F

	USE THIS FOR LOOP STRICTLY for the outer loop
	int row =6;
	
	for(int i =1;i<=row;i++){
	
	}

*/
