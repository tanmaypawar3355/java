/*
Rotate Array
Given an unsorted array arr[] of size N. Rotate the array to the left (counter-clockwise direction) by D steps, where D is a positive integer.

Example 1:

Input:
N = 5, D = 2
arr[] = {1,2,3,4,5}
Output: 3 4 5 1 2
Explanation: 1 2 3 4 5  when rotated
by 2 elements, it becomes 3 4 5 1 2.

Example 2:

Input:
N = 10, D = 3
arr[] = {2,4,6,8,10,12,14,16,18,20}
Output: 8 10 12 14 16 18 20 2 4 6
Explanation: 2 4 6 8 10 12 14 16 18 20 
when rotated by 3 elements, it becomes 
8 10 12 14 16 18 20 2 4 6.

Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)

 

Constraints:
1 <= N <= 106
1 <= D <= 106
0 <= arr[i] <= 105

https://practice.geeksforgeeks.org/problems/rotate-array-by-n-elements-1587115621/1?page=2&category[]=Arrays&sortBy=submissions
*/

class RotateArray {

	public static void main(String[] args) {

		//int arr[] = {1,2,3,4,5};
		int arr[] = {2,4,6,8,10,12,14,16,18,20};

		int D = 3;

		while(D != 0){

			int temp = 0;

			for(int i = 0 ; i < arr.length-1 ; i++){

				temp = arr[i];
				arr[i] = arr[i+1];
				arr[i+1] = temp;
			}
			D--;
		}

		for(int i = 0 ; i < arr.length ; i++){

			System.out.print("| " + arr[i] + " |");
		}
	}
}

