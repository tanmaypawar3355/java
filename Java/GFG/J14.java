/*
Trapping Rain Water

Given an array arr[] of N non-negative integers representing the height of blocks. If width of each block is 1, compute how much water can be trapped between the blocks during the rainy season.

Example 1:

Input:
N = 6
arr[] = {3,0,0,2,0,4}
Output:
10
Explanation:
Bars of input {3,0,0,2,0,4}
Total trapped water = 3+3+1+3 = 10

Example 2:

Input:
N = 4
arr[] = {7,4,0,9}
Output:
10
Explanation:
Water trapped by above
block of height 4 is 3 units and above
block of height 0 is 7 units. So, the
total unit of water trapped is 10 units.

Input:
N = 3
arr[] = {6,9,9}
Output:
0
Explanation:
No water will be trapped.

Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)


Constraints:
3 < N < 106
0 < Ai < 108

https://practice.geeksforgeeks.org/problems/trapping-rain-water-1587115621/1?page=1&category[]=Arrays&sortBy=submissions

*/

class TrappingRainWater {

	public static void main(String[] args){

		//int arr[] = {3,0,0,2,0,4};
		//int arr[] = {7,4,0,9};
		int arr[] = {6,9,9};

		int num = arr[0];
		int sum = 0;
		int sub = 0;

		for(int i = 0 ; i < arr.length-1 ; i++){

			for(int j = i+1 ; j <= i+1 ; j++){

				if(num > arr[j]){
					sub = num - arr[j];
					sum = sum + sub;
				}
			}
		}
		if(sum == 0){
			System.out.println("No water will be trapped");
		}else{
			System.out.println(sum);
		}
	}
}

					
