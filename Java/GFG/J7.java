/*
Kth smallest element
Given an array arr[] and an integer K where K is smaller than size of array, the task is to find the Kth smallest element in the given array. It is given that all array elements are distinct.

Note :-  l and r denotes the starting and ending index of the array.

Example 1:

Input:
N = 6
arr[] = 7 10 4 3 20 15
K = 3
Output : 7
Explanation :
3rd smallest element in the given 
array is 7.

Example 2:

Input:
N = 5
arr[] = 7 10 4 20 15
K = 4
Output : 15
Explanation :
4th smallest element in the given 
array is 15.

Expected Time Complexity: O(n)
Expected Auxiliary Space: O(log(n))
Constraints:
1 <= N <= 105
1 <= arr[i] <= 105
1 <= K <= N

https://practice.geeksforgeeks.org/problems/kth-smallest-element5635/1?page=1&category[]=Arrays&sortBy=submissions

*/

class KthSmallestEle {

	public static void main(String[] args) {

		int arr[] = {7,10,4,3,20,15};

		int k = 3;

		for(int i = 0 ; i < arr.length ; i++){

			int count = 0;

			for(int j = 0; j < arr.length ; j++){

				if(arr[i] > arr[j]){
					count++;
				}

				if(count > k){
					break;
				}
			}
			if(count == 2){
				System.out.println(arr[i]);
				break;
			}
		}
	}
}
