/*
Key Pair

Given an array Arr of N positive integers and another number X. Determine whether or not there exist two elements in Arr whose sum is exactly X.

Example 1:

Input:
N = 6, X = 16
Arr[] = {1, 4, 45, 6, 10, 8}
Output: Yes
Explanation: Arr[3] + Arr[4] = 6 + 10 = 16

Example 2:

Input:
N = 5, X = 10
Arr[] = {1, 2, 4, 3, 6}
Output: Yes
Explanation: Arr[2] + Arr[4] = 4 + 6 = 10

Your Task:
You don't need to read input or print anything. Your task is to complete the function hasArrayTwoCandidates() which takes the array of integers arr, n and x as parameters and returns a boolean denoting the answer.

Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)

Constraints:
1 ≤ N ≤ 105
1 ≤ Arr[i] ≤ 105

https://practice.geeksforgeeks.org/problems/key-pair5616/1?page=2&category[]=Arrays&sortBy=submissions

*/
class KeyPair {

	public static void main(String[] args) {

		int arr[] = {1,4,45,6,10,8};

		int num = 16;

		boolean ret = hasArrayTwoCandidates(arr,num);

		if(ret == true){
			System.out.println("Yes");
		}else{
			System.out.println("No");
		}
	}

	static boolean hasArrayTwoCandidates(int arr[],int num){

		for(int i = 0 ; i < arr.length ; i++){

			int sum = 0;
			sum = sum + arr[i];

			for(int j = i+1 ; j < arr.length ; j++){

				if(sum + arr[j] == num){
					return true;
				}
			}
		}
		return false;
	}
}


