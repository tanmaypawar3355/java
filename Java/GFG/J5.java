/*
Sort an array of 0s, 1s and 2s

Given an array of size N containing only 0s, 1s, and 2s; sort the array in ascending order.

Example 1:

Input: 
N = 5
arr[]= {0 2 1 2 0}
Output:
0 0 1 2 2
Explanation:
0s 1s and 2s are segregated 
into ascending order.

Example 2:

Input: 
N = 3
arr[] = {0 1 0}
Output:
0 0 1
Explanation:
0s 1s and 2s are segregated 
into ascending order.

https://practice.geeksforgeeks.org/problems/sort-an-array-of-0s-1s-and-2s4231/1?page=1&category[]=Arrays&sortBy=submissions

*/

class SortAnArray {

	public static void main(String[] args){

		int arr[] = {0,2,1,2,0};

		int start = 0 ; 
		int end = arr.length - 1;

		merge(arr,start,end);

		for(int i = 0 ; i < arr.length ; i++){
			System.out.print("| " +arr[i]+ " |");
		}
		System.out.println();
	}

	static void merge(int arr[],int start,int end){
	
		if(start < end){

			int mid = (start + end) / 2;

			merge(arr,start,mid);
			merge(arr,mid+1,end);
			sorting(arr,start,mid,end);
		}
	}

	static void sorting(int arr[],int start,int mid,int end){

		int ele1 = mid - start + 1;
		int ele2 = end - mid;

		int arr1[] = new int[ele1];
		int arr2[] = new int[ele2];

		for(int i = 0 ; i < ele1 ; i++){
			arr1[i] = arr[start + i];
		}

		for(int j = 0 ; j < ele2 ; j++){
			arr2[j] = arr[mid + 1 + j];
		}

		int itr1 = 0,itr2 = 0,itr3 = start;

		while(itr1 < ele1 && itr2 < ele2){

			if(arr1[itr1] < arr2[itr2]){

				arr[itr3] = arr1[itr1];
				itr1++;
			
			}else{
				arr[itr3] = arr2[itr2];
				itr2++;
			}
			itr3++;
		}

		while(itr1 < ele1){

			arr[itr3] = arr1[itr1];
			itr1++;
			itr3++;
		}

		while(itr1 < ele2){

                        arr[itr3] = arr2[itr2];
                        itr2++;
                        itr3++;
                }
	}
}





