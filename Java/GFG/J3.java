/*
Kadane's Algorithm
Given an array Arr[] of N integers. Find the contiguous sub-array(containing at least one number) which has the maximum sum and return its sum.

Example 1:

Input:
N = 5
Arr[] = {1,2,3,-2,5}
Output:
9
Explanation:
Max subarray sum is 9
of elements (1, 2, 3, -2, 5) which 
is a contiguous subarray.

Example 2:

Input:
N = 4
Arr[] = {-1,-2,-3,-4}
Output:
-1
Explanation:
Max subarray sum is -1 
of element (-1)

https://practice.geeksforgeeks.org/problems/kadanes-algorithm-1587115620/1?page=1&category[]=Arrays&sortBy=submissions

*/

class KadaneAlgorithm {

	public static void main(String[] args){

		int arr[] = {1,2,3,-2,5};
		//int arr[] = {-1,-2,-3,-4};

		int sum1 = arr[0];
	
		for(int i = 0 ; i < arr.length ; i++){		

			int sum = 0;

			for(int j = 0 ; j <= i ; j++){

				sum = sum + arr[j];
			}


			if(sum > sum1){
				sum1 = sum;
			}
		}

		System.out.println("Max sum = " +sum1);
	}
}
				






