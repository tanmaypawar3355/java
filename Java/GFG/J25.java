/*https://practice.geeksforgeeks.org/problems/find-triplets-with-zero-sum/1?page=2&category[]=Arrays&sortBy=submissionshttps://practice.geeksforgeeks.org/problems/find-triplets-with-zero-sum/1?page=2&category[]=Arrays&sortBy=submissions
Find triplets with zero sum

Given an array arr[] of n integers. Check whether it contains a triplet that sums up to zero.

Example 1:

Input: n = 5, arr[] = {0, -1, 2, -3, 1}
Output: 1
Explanation: 0, -1 and 1 forms a triplet
with sum equal to 0.

Example 2:

Input: n = 3, arr[] = {1, 2, 3}
Output: 0
Explanation: No triplet with zero sum exists.

Expected Time Complexity: O(n2)
Expected Auxiliary Space: O(1)

Constrains:
1 <= n <= 104
-106 <= Ai <= 106

https://practice.geeksforgeeks.org/problems/find-triplets-with-zero-sum/1?page=2&category[]=Arrays&sortBy=submissions

*/

class FindTripletsWithZeroSum {

	public static void main(String[] args) {

		int arr[] = {0,-1,2,-3,1};

		int sum = 0;
		int flag = 0;
		int count = 0;

		for(int i = 0 ; i < arr.length ; i++){

			sum = sum + arr[i];
			count++;

			for(int j = i+1 ; j < arr.length ; j++){

				sum = sum + arr[j];
				count++;

				if(sum == 0){
					break;
				}else{
					count--;
					sum = sum - arr[j];
				}
			}
			if(sum == 0 && count == 3){
				flag = 1;
				break;
			}
		}

		if(flag == 1){
			System.out.println("1");
		}else{
			System.out.println("0");
		}
	}
}



				
