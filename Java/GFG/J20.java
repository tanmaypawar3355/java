/*
Maximum Product Subarray

iven an array Arr[] that contains N integers (may be positive, negative or zero). Find the product of the maximum product subarray.

Example 1:

Input:
N = 5
Arr[] = {6, -3, -10, 0, 2}
Output: 180
Explanation: Subarray with maximum product
is [6, -3, -10] which gives product as 180

Example 2:

Input:
N = 6
Arr[] = {2, 3, 4, 5, -1, 0}
Output: 120
Explanation: Subarray with maximum product
is [2, 3, 4, 5] which gives product as 120.

Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)

Constraints:
1 ≤ N ≤ 500
-102 ≤ Arri ≤ 102

https://practice.geeksforgeeks.org/problems/maximum-product-subarray3604/1?page=1&category[]=Arrays&sortBy=submissions

*/

class MaxProductSubarray {

	public static void main(String[] args) {

		//int arr[] = {6,-3,-10,0,2};
		

		int arr[] = {-6,31,23,-56,78,-67};

		int product = 1;

		int max = arr[0];


		for(int i = 0 ; i < arr.length ; i++){

			product = product * arr[i];

			if(max < product){
				max = product;
			}			
		}

		System.out.println("Max subarray product = " +max);
	}
}


