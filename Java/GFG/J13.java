/*
Binary Search

Given a sorted array of size N and an integer K, find the position(0-based indexing) at which K is present in the array using binary search.

Example 1:

Input:
N = 5
arr[] = {1 2 3 4 5} 
K = 4
Output: 3
Explanation: 4 appears at index 3


Example 2:

Input:
N = 5
arr[] = {11 22 33 44 55} 
K = 445
Output: -1
Explanation: 445 is not present.


Expected Time Complexity: O(LogN)
Expected Auxiliary Space: O(LogN) if solving recursively and O(1) otherwise.


Constraints:

1 <= N <= 105
1 <= arr[i] <= 106
1 <= K <= 106


https://practice.geeksforgeeks.org/problems/binary-search-1587115620/1?page=1&category[]=Arrays&sortBy=submissions
      
*/
class BinarySearch {

	public static void main(String[] args){

		int arr[] = {1,2,3,4,5};
		//int arr[] = {11,22,33,44,55};
		
		int key = 4;

		int ret = BinarySearch(arr,arr.length,key);

		if(ret == -1){
			System.out.println("Key is not present");
		}else{
			System.out.println(key + " appears at index " + ret);
		}
	}

	static int BinarySearch(int arr[],int size,int key){

		int start = 0;
		int end = size - 1;
		int mid;

		while(start <= end){

			mid = (start + end) / 2;

			if(arr[mid] == key){
				return mid;
			}

			if(arr[mid] < key){
				start = mid + 1;
			}

			if(arr[mid] > key){
				end = mid - 1;
			}
		}
	return -1;
	}
}	


