/*
Second Largest

Given an array Arr of size N, print second largest distinct element from an array.


Example 1:

Input: 
N = 6
Arr[] = {12, 35, 1, 10, 34, 1}
Output: 34
Explanation: The largest element of the 
array is 35 and the second largest element
is 34.

Example 2:

Input: 
N = 3
Arr[] = {10, 5, 10}
Output: 5
Explanation: The largest element of 
the array is 10 and the second 
largest element is 5.

Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)

Constraints:
1 ≤ N ≤ 105
1 ≤ Arri ≤ 105

https://practice.geeksforgeeks.org/problems/second-largest3735/1?page=1&category[]=Arrays&sortBy=submissions

*/


class SecondLargest {

        public static void main(String[] args) {

                //int arr[] = {2,255,2,1554,15,65};
                //int arr[] = {12,35,1,10,34,1};
                int arr[] = {1,2,3,4,5,17,9,6,7,8,10};
                int large = arr[0];
                int secondLarge = 0;

                for(int i = 1 ; i < arr.length ; i++){
                    
                    for(int j = 0 ; j < arr.length; j++){

                        if(arr[i] > large && arr[j] > arr[i]){
                            secondLarge = large;
                                large = arr[i];
                        }
                    }
                }
                System.out.println(large);
        }
}	







