/*
Check if two arrays are equal or not

Given two arrays A and B of equal size N, the task is to find if given arrays are equal or not. Two arrays are said to be equal if both of them contain same set of elements, arrangements (or permutation) of elements may be different though.
Note : If there are repetitions, then counts of repeated elements must also be same for two array to be equal.

Example 1:

Input:
N = 5
A[] = {1,2,5,4,0}
B[] = {2,4,5,0,1}
Output: 1
Explanation: Both the array can be 
rearranged to {0,1,2,4,5}

Example 2:

Input:
N = 3
A[] = {1,2,5}
B[] = {2,4,15}
Output: 0
Explanation: A[] and B[] have only 
one common value.

Expected Time Complexity : O(N)
Expected Auxilliary Space : O(N)

Constraints:
1<=N<=107
1<=A[],B[]<=1018

https://practice.geeksforgeeks.org/problems/check-if-two-arrays-are-equal-or-not3847/1?page=2&category[]=Arrays&sortBy=submissions
*/    

class ArrayEqualOrNot {

	public static void main(String[] args){

		int arr1[] = {1,2,5,4,0};
		int arr2[] = {2,4,5,0,1};
		int flag = 0;

		for(int i = 0 ; i < arr1.length ; i++){

			for(int j = 0 ; j < arr2.length ; j++){

				if(arr1[i] == arr2[j]){
					flag = 1;
					break;
				}
				flag = 0;
			}
			if(flag == 0){
				break;
			}
		}
		if(flag == 1)
			System.out.println("Arrays are equal");
		else
			System.out.println("Arrays are not equal");
	}
}
