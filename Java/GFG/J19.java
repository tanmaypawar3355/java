/*
Find Missing And Repeating

Given an unsorted array Arr of size N of positive integers. One number 'A' from set {1, 2,....,N} is missing and one number 'B' occurs twice in array. Find these two numbers.

Example 1:

Input:
N = 2
Arr[] = {2, 2}
Output: 2 1
Explanation: Repeating number is 2 and 
smallest positive missing number is 1.

Example 2:

Input:
N = 3
Arr[] = {1, 3, 3}
Output: 3 2
Explanation: Repeating number is 3 and 
smallest positive missing number is 2.

Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)

Constraints:
2 ≤ N ≤ 105
1 ≤ Arr[i] ≤ N

https://practice.geeksforgeeks.org/problems/find-missing-and-repeating2512/1?page=1&category[]=Arrays&sortBy=submissions

*/

class FindMissingAndRepeating {

	public static void main(String[] args) {

		int arr[] = {1,3,3,4,5,7,5};
		int repeating = 0;
		int missing = arr[0];
		int flag = 0;
		int parat =0;

		int max = arr[0];

		for(int i = 1 ; i < arr.length ; i++){
			if(max < arr[i]){
				max = arr[i];
			}
		}

		for(int i = 0 ; i < arr.length ; i++){
				
			for(int j = i+1 ; j < arr.length ; j++){

				if(arr[i] == arr[j]){
	   				System.out.println("Repeating = " +arr[i]);	
				}

				if((arr[i]+1) == arr[j]){
					flag = 0;
					break;
				}else{
					missing = (arr[i]+1);
					flag = 1;
				}
			}


			if(flag == 1 && missing != parat && missing < max){
				System.out.println("Missing = " + missing);
	   			parat = missing;
			}
		}
	}
}
