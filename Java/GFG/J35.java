/*
Row with max 1s

Given a boolean 2D array of n x m dimensions where each row is sorted. Find the 0-based index of the first row that has the maximum number of 1's.

Example 1:

Input: 
N = 4 , M = 4
Arr[][] = {{0, 1, 1, 1},
           {0, 0, 1, 1},
           {1, 1, 1, 1},
           {0, 0, 0, 0}}
Output: 2
Explanation: Row 2 contains 4 1's (0-based
indexing).

Example 2:

Input: 
N = 2, M = 2
Arr[][] = {{0, 0}, {1, 1}}
Output: 1
Explanation: Row 1 contains 2 1's (0-based
indexing).

Expected Time Complexity: O(N+M)
Expected Auxiliary Space: O(1)


Constraints:
1 ≤ N, M ≤ 103
0 ≤ Arr[i][j] ≤ 1 

https://practice.geeksforgeeks.org/problems/row-with-max-1s0023/1?page=2&category[]=Arrays&sortBy=submissions

*/class RowWithMaxOne {

	public static void main(String[] args) {

		int arr[][] = {{0,1,1,1},{0,0,1,1},{1,1,1,1},{0,0,0,0}};
		int count = 0;
		int num = 0;

		for(int i = 0 ; i < 4 ; i++){

			int count1 = count;

			for(int j = 0 ; j < 4 ; j++){

				if(arr[i][j] == 1){
					count++;
				}
			}
			if(count > count1){
				count1 = count;
				num = i;
			}
		}

		System.out.println(num);
	}
}

