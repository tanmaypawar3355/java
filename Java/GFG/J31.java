/*
Cyclically rotate an array by one

Given an array, rotate the array by one position in clock-wise direction.

Example 1:

Input:
N = 5
A[] = {1, 2, 3, 4, 5}
Output:
5 1 2 3 4

Example 2:

Input:
N = 8
A[] = {9, 8, 7, 6, 4, 2, 1, 3}
Output:
3 9 8 7 6 4 2 1

Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)

 

Constraints:
1<=N<=105
0<=a[i]<=105

https://practice.geeksforgeeks.org/problems/cyclically-rotate-an-array-by-one2614/1?page=2&category[]=Arrays&sortBy=submissions

*/

class CyclicallyRotateArrayByOne {

	public static void main(String[] args) {

		int arr[] = {1,2,3,4,5};

		for(int i = arr.length-1 ; i > 0 ; i--){

			int temp = arr[i];
			arr[i] = arr[i-1];
			arr[i-1] = temp;
		}

		for(int i = 0 ; i < arr.length ; i++){
			System.out.print("| " +arr[i]+ " |");
		}
	}
}
