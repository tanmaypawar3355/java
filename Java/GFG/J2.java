/*
Missing number in array

Given an array of size N-1 such that it only contains distinct integers in the range of 1 to N. Find the missing element.

Example 1:

Input:
N = 5
A[] = {1,2,3,5}
Output: 4


Example 2:

Input:
N = 10
A[] = {6,1,2,8,3,4,7,10,5}
Output: 9

Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)


Constraints:
1 ≤ N ≤ 106
1 ≤ A[i] ≤ 106

https://practice.geeksforgeeks.org/problems/missing-number-in-array1416/1?page=1&category[]=Arrays&sortBy=submissions

*/

class MissingNumberInArray {

	public static void main(String[] args){

		int N = 10;

		int arr[] = {6,1,2,8,3,4,7,10,5};

		int sum = 0;

		for(int i = 1 ; i <= N ; i++){
			sum = sum + i;
		}

		int sum1 = 0;

		for(int i = 0 ; i < arr.length ; i++){
			sum1 = sum1 + arr[i];
		}
		int num = sum - sum1;
		System.out.println("Missing number is : " + num); 
	}
}


