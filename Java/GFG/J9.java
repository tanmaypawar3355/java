Minimize the Heights

https://practice.geeksforgeeks.org/problems/minimize-the-heights3351/1?page=1&category[]=Arrays&sortBy=submissions

class MinimizeTheHeight {

	public static void main(String[] args){

		int arr[] = {1,5,8,10};

		int  k = 2;

		for(int i = 0 ; i < arr.length ; i++){

			if(arr[i] % 2 == 0){
				arr[i] = arr[i] - k;
			
			}else{
				arr[i] = arr[i] + k;
			}
			System.out.println(arr[i]);
		}
	}
}

