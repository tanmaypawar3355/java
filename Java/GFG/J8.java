/*
Majority Element

Given an array A of N elements. Find the majority element in the array. A majority element in an array A of size N is an element that appears more than N/2 times in the array.

Example 1:

Input:
N = 3 
A[] = {1,2,3} 
Output:
-1
Explanation:
Since, each element in 
{1,2,3} appears only once so there 
is no majority element.

Example 2:

Input:
N = 5 
A[] = {3,1,3,3,2} 
Output:
3
Explanation:
Since, 3 is present more
than N/2 times, so it is 
the majority element.

Expected Time Complexity: O(N).
Expected Auxiliary Space: O(1).
 

Constraints:
1 ≤ N ≤ 107
0 ≤ Ai ≤ 106

https://practice.geeksforgeeks.org/problems/majority-element-1587115620/1?page=1&category[]=Arrays&sortBy=submissions


*/
class MajorityElement {


	public static void main(String[] args){

		int arr[] = {3,1,3,3,2};

		int N = arr.length / 2;

		for(int i = 0 ; i < arr.length ; i++){

			int count = 0;

			for(int j = i ; j < arr.length ; j++){

				if(arr[i] == arr[j]){
					count++;
				}
			}
			if(count > N){
				System.out.println("Majority element in array is " + arr[i]);
			}
		}
	}
}
