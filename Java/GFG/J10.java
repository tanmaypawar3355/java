/*
Equilibrium Point

Given an array A of n positive numbers. The task is to find the first Equilibrium Point in an array. 
Equilibrium Point in an array is a position such that the sum of elements before it is equal to the sum of elements after it.

Note: Retun the index of Equilibrium point. (1-based index)

Example 1:

Input: 
n = 5 
A[] = {1,3,5,2,2} 
Output: 3 
Explanation:  
equilibrium point is at position 3 
as elements before it (1+3) = 
elements after it (2+2).

Example 2:

Input:
n = 1
A[] = {1}
Output: 1
Explanation:
Since its the only element hence
its the only equilibrium point.

https://practice.geeksforgeeks.org/problems/equilibrium-point-1587115620/1?page=1&category[]=Arrays&sortBy=submissions

*/

class EquilibriumIndex {

	public static void main(String[] args) {

		int arr[] = {1,3,5,2,2};

		int sum = 0;

		if(arr.length < 2){
			System.out.println("EquilibriumIndex = " + arr[0]);
		}

		if(arr.length == 2){
			System.out.println("Array having only 2 elements so their is no any eluilibrium index");
		}

		for(int i = 0 ; i < arr.length ; i++){

			sum = sum + arr[i];

			int sum1 = 0;

			for(int j = i+2 ; j < arr.length ; j++){

				sum1 = sum1 + arr[j];
			}
			if(sum == sum1){
				System.out.println("EquilibriumIndex = " + arr[i+1]);
			}
		}
	}
}
