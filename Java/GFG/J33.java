/*
Largest subarray with 0 sum

Given an array having both positive and negative integers. The task is to compute the length of the largest subarray with sum 0.

Example 1:

Input:
N = 8
A[] = {15,-2,2,-8,1,7,10,23}
Output: 5
Explanation: The largest subarray with
sum 0 will be -2 2 -8 1 7.

Your Task:
You just have to complete the function maxLen() which takes two arguments an array A and n, where n is the size of the array A and returns the length of the largest subarray with 0 sum.

Expected Time Complexity: O(N).
Expected Auxiliary Space: O(N).

Constraints:
1 <= N <= 105
-1000 <= A[i] <= 1000, for each valid i

https://practice.geeksforgeeks.org/problems/largest-subarray-with-0-sum/1?page=2&category[]=Arrays&sortBy=submissions
*/

class LargestSubarrayWithSZeroSum {

	public static void main(String[] args) {

		int arr[] = {15,-2,2,-8,1,7,10,23};

		//int sum = 0;
		int count = 0;
		int num = 0;

		for(int i = 0 ; i < arr.length ; i++){

			int sum = 0;
			count = 0;
			num = 0;

			for(int j = i+1 ; j < arr.length ; j++){
		
				sum = sum + arr[j];
				count++;

				if(sum == 0){
					num = count;
				} 
			}
			if(count > num){
                        	break;
                        }
		}
		System.out.println(num);
	}
}
