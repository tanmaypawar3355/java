// Online C compiler to run C program online
#include <stdio.h>
#include<stdlib.h>

struct TreeNode {
    
    int data;
    struct TreeNode *left;
    struct TreeNode *right;
};

struct TreeNode *createNode(int level){
    
    level++;
    
    struct TreeNode *newNode = (struct TreeNode*)malloc(sizeof(struct TreeNode));
    printf("Enter data\n");
    scanf("%d",&newNode->data);
    
    getchar();
    
    char ch1;    
    printf("Do you want to create left subtree for level %d\n",level);
    scanf("%c",&ch1);
    
    if(ch1 == 'y' || ch1 == 'Y') {
        struct TreeNode *newNode = createNode(level);
    }else{
        newNode -> left = NULL;
    }
    
    
    getchar();
    char ch2;    
    printf("Do you want to create right subtree for level %d\n",level);
    scanf("%c",&ch2);
    
    if(ch2 == 'y' || ch2 == 'Y') {
        struct TreeNode *newNode = createNode(level);
    }else{
        newNode -> left = NULL;
    }
    return newNode;
}

void preOrder(struct TreeNode *root){
    
    if(root == NULL){
        return;
    }
        printf("%d  ",root->data);
        preOrder(root->left);
        preOrder(root->right);
}

int main() {
    printf("Creating binary tree........\n");
    
    struct TreeNode *root = (struct TreeNode*)malloc(sizeof(struct TreeNode));
    printf("Enter data\n");
    scanf("%d",&root->data);
    
    printf("\t\t\t\tTree is root with %d\n",root->data);
    int level = 0;
    
    getchar();
    char ch1;    
    printf("Do you want to create left subtree for root node?\n");
    scanf("%c",&ch1);
    
    if(ch1 == 'y' || ch1 == 'Y') {
        struct TreeNode *left = createNode(level);
    }else{
        root -> left = NULL;
    }
    
    getchar();
    char ch2;    
    printf("Do you want to create right subtree for rootnode\n");
    scanf("%c",&ch2);
    
    if(ch2 == 'y' || ch2 == 'Y') {
        struct TreeNode *right = createNode(level);
    }else{
        root -> left = NULL;
    }
    
    printf("\nPrinting the tree\n");
    preOrder(root);
    
}
    
    
