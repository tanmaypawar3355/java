import java.io.*;

class HowToTakeArrayFromUser {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int arr[] = new int[3];
        
        System.out.println("Enter values");

	for(int i = 0 ; i < 3 ; i++){
		
		arr[i] = Integer.parseInt(br.readLine());
	}

	for(int i = 0 ; i < 3 ; i++){

		System.out.print("|" +arr[i]+ "|");
	}

        /*
        arr[0] = Integer.parseInt(br.readLine());
        arr[1] = Integer.parseInt(br.readLine());
        arr[2] = Integer.parseInt(br.readLine());
        
        System.out.println(arr[0]);
        System.out.println(arr[1]);
        System.out.println(arr[2]);
	*/
        
    }
}
