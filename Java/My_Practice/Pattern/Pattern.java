/*

1
3   2
6   5  4
10  9  8  7

*/
class Pattern{

	public static void main(String[] args){

		int temp = 0;


		for(int i = 1 ; i <= 4 ; i++){

			int num = temp + i;

			for(int j = 1 ; j <= i ;j++){

				if(j == 1){
					temp = num;
				}

				System.out.print(num-- + "  ");
			}
			System.out.println();
		}
	}
}
/*
class Balya{
        public static void main(String[]s){
                int row=4,num=0;
                for(int i=1;i<=row;i++){
                        num=num+i;
                        for(int j=1;j<=i;j++){
                                System.out.print(num--+" ");
                        }
                        num=num+i;
                        System.out.println();
                }
        }
}
*/

