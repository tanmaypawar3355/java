class HalfPyramid{

	public static void main(String[] args){

		int N = 6;

		for(int i = 1 ; i<= N ; i++){

			for(int j = 1 ; j <= N+i-1 ; j++){

				if( j <= N-i){

					System.out.print("   ");
				}else{
					System.out.print("*  ");
				}
			}System.out.print("\n");
		}
	}
}
