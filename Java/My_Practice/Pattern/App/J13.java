/*
 1  10  11  20  21
 2  9   12  19  22
 3  8   13  18  23
 4  7   14  17  24
 5  6   15  16  25

*/

class Pattern{

        public static void main(String[] args){

		int N = 5;

                for(int i = 1 ; i <= N ; i++){

			int num = 0;

                        for(int j = 1 ; j <= N ; j++){

				if(j % 2 == 1){
					num = num + i;
				}
				System.out.print(num +"  ");
					num = N * 2 - i + 1;
			
			}
	                System.out.println();
                }
        }
}
