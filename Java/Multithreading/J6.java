import java.util.concurrent.*;

class MyThread implements Runnable {

	int num;

	MyThread(int num) {

		this.num = num;
	}

	public void run() {

		System.out.println(Thread.currentThread() + "Start thread : " + num);
		dailyTask();
		System.out.println(Thread.currentThread() + "End thread : " + num);
	}
		
	void dailyTask() {
			
		try {

			Thread.sleep(10000);
		}catch(InterruptedException ie){

		}
	}	
}

class ThreadPoolDemo {

	public static void main(String[] args) {

		ExecutorService ser = Executors.newWorkStealingPool();

		for(int i = 0 ; i < 25 ; i++) {

			MyThread obj = new MyThread(1);
			ser.execute(obj);
		}

		ser.shutdown();
	}
}
