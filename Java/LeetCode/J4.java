/*

4. Median of Two Sorted Arrays

Given two sorted arrays nums1 and nums2 of size m and n respectively, return the median of the two sorted arrays.

The overall run time complexity should be O(log (m+n)).

Example 1:Constraints:

nums1.length == m
nums2.length == n
0 <= m <= 1000
0 <= n <= 1000
1 <= m + n <= 2000
-106 <= nums1[i], nums2[i] <= 106

Input: nums1 = [1,3], nums2 = [2]
Output: 2.00000
Explanation: merged array = [1,2,3] and median is 2.

Example 2:
Input: nums1 = [1,2], nums2 = [3,4]
Output: 2.50000
Explanation: merged array = [1,2,3,4] and median is (2 + 3) / 2 = 2.5.

Constraints:
nums1.length == m
nums2.length == n
0 <= m <= 1000
0 <= n <= 1000
1 <= m + n <= 2000
-106 <= nums1[i], nums2[i] <= 106

*/
import java.io.*;

class Leet4 {

	public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of 1st array");
                int size1 = Integer.parseInt(br.readLine());

                int arr1[] = new int[size1];

                System.out.println("Enter elements of array");
                for(int i = 0 ; i < arr1.length ; i++){

                        arr1[i] = Integer.parseInt(br.readLine());
                }

		System.out.println("Enter size of 2nd array");
                int size2 = Integer.parseInt(br.readLine());

                int arr2[] = new int[size2];

                System.out.println("Enter elements of array");
                for(int i = 0 ; i < arr2.length ; i++){

                        arr2[i] = Integer.parseInt(br.readLine());
                }

		int arr3[] = new int[size1+size2];

		for(int i = 0 ; i < arr1.length ; i++){

			for(int j = 0 ; j < arr2.length ; j++){

				if(arr1[i] < arr2[j]){

					arr3[i] = arr1[i];
				}else{
					arr3[i] = arr2[i];
				}
			}
		}

		for(int i = 0 ; i < arr3.length ; i++){

			System.out.println(arr3[i]);
		}

	}
}


