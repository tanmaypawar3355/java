class LengthDemo {

	public static void main(String[] args) {

		String str1 = "Core2Web";

		int count = MyStrLen(str1);
		System.out.println(count);

	}

	static int MyStrLen(String str) {

		char arr[] = str.toCharArray();

		int count = 0;

		for(int i = 0 ; i < arr.length ; i++) {

			count++;
		}

		return count;
	}
}
