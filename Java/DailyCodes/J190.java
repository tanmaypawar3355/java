class JaggedArray {

	public static void main(String[] args) {

		int arr[][] = new int[3][];

		arr[0] = new int[3];
		arr[1] = new int[2];
		arr[2] = new int[1];
		
		arr[0] = new int[]{1,2,3};
		arr[1] = new int[]{4,5};
		arr[2] = new int[]{7};
	}
}

