/*

method : public String replace(char oldChar,char newChar);

Description :

Replaces every instance of character in the given String with a new character.

Parameter : character(old character)
	    character(new character)

return type : String

*/

class ReplaceDemo {
	public static void main(String[] args) {

		String str1 = "Shashi";

		System.out.println(str1.replace('h','p'));

		System.out.println(str1);
	}
}


