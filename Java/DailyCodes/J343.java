class Parent {

        private void fun() {       // access specifier default

                System.out.println("Parent fun");
        }
}

class Child extends Parent {

        void fun() {       // access specifier public

                System.out.println("Child fun");
        }
}

class CLient {

        public static void main(String[] args) {

		Parent obj = new Child();
		obj.fun();

        }
}
