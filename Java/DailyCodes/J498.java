import java.util.*;

class Platform {

	String str;
	int foundYear;

	Platform(String str,int foundYear) {

		this.str = str;
		this.foundYear = foundYear;
	}

	public String toString() {

		return "{" + str + " : " + foundYear + "}";
	}
}

class SortByName implements Comparator {

	public int compare(Object obj1,Object obj2) {

		return ((Platform)obj1).str.compareTo(((Platform)obj2).str);
	}
}

class SortByFoundYear implements Comparator {

	public int compare(Object obj1,Object obj2) {
		
		return ((Platform)obj1).foundYear - (((Platform)obj2).foundYear);
	}
}	



class TreeMapDemo {

        public static void main(String[] args) {

                TreeMap tm = new TreeMap(new SortByFoundYear());
                //TreeMap tm = new TreeMap(new SortByName());
		//Collections.sort(tm,new SortByName()); 
		
  		tm.put(new Platform("Yt",2003),"Google");
                tm.put(new Platform("Insta",2010),"Meta");
                tm.put(new Platform("FB",2004),"Meta");
                tm.put(new Platform("ChatGPT",2022),"OpenAI");
 
                System.out.println(tm);
        }
} 
