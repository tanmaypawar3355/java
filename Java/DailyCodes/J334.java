class Parent {

	void fun() {

		System.out.println("Parent fun");
	}
}

class Child extends Parent {

	void fun() {
		
		System.out.println("Child fun");
		//super.fun();
	}

	void gun() {
		
		System.out.println("Child gun");
	}
}

class Client {

	public static void main(String[] args) {

		Parent obj1 = new Parent();
		obj1.fun();
		//obj1.gun();  error

		Child obj2 = new Child();
		obj2.fun();
		obj2.gun();
	}
}
