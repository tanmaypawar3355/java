class Demo {

	int x = 10;

	Demo() {  //Demo(Demo this)

		System.out.println("In constructor");
		System.out.println(x);  //(this.x)
		System.out.println(this.x);
	}

	void fun() {	//fun(Demo this)	   

		System.out.println(x);
		System.out.println(this.x);
	}

	public static void main(String[] args) {

		Demo obj1 = new Demo();
		
		obj1.fun();
	}
}

