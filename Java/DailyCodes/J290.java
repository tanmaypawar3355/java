class Demo {

	int x = 10;

	Demo() {

		System.out.println("No-args constructor");
	}

	Demo(int x) {
		
		System.out.println("In para constructor");
		this();
	}

	public static void main(String[] args) {

		Demo obj2 = new Demo(50);
	}
}
