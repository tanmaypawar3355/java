/*

2] Insert

method : public synchronized StringBuffer insert(int offset,String str);

Description :
- Insert the <code>String</code> argument into this <code>StringBuffer</code>.
- If str i snull, the string "null" is used instead.
- If str is null,the String "null" is used instead.

Parameter : - Integer(offset the place to insert in this buffer)
	    - String(str the <code>String</code> to insert)

Return type : StringBuffer(this <code>StringBuffer</code>)

*/

class InsertDemo {

	public static void main(String[] args) {

		StringBuffer sb = new StringBuffer("Shashi Bagal");

		System.out.println(sb);

		sb.insert(6," Core2Web");
		
		System.out.println(sb);
	}
}


