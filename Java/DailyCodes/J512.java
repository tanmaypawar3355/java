class MyThread extends Thread {

	String str = null;

	MyThread(String str) {

		this.str = str;
	}

	public void run() {

		System.out.println("Thread name = " + Thread.currentThread().getName());
		
		String str1 = Thread.currentThread().getName();

		Thread.currentThread().setName(str);
		
		System.out.println("New name of " + str1 + " = " + Thread.currentThread().getName());


	}
}

class ThreadDemo {

	public static void main(String[] args) throws InterruptedException {
		
		System.out.println("Thread name = " + Thread.currentThread().getName());

		MyThread obj1 = new MyThread("Thread 0.0");

		obj1.start();

		MyThread obj2 = new MyThread("Thread 1.1");
		
		obj2.start();
	}
}
	
