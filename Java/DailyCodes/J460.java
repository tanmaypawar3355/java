import java.util.*;

class ArrayListDemo extends ArrayList {

	public static void main(String[] args) {

		ArrayListDemo al = new ArrayListDemo();

		//add(Element)   =>  public boolean add(E);
		al.add(10);
		al.add(20.5f);
		al.add("tammy");
		al.add(10);
		al.add(10);
		System.out.println(al);

		
		//add(int,Element)   =>   public void add(int, E);
		al.add(3,"Core2Web");
		System.out.println(al);
	

		//size()   =>    public int size();
		System.out.println(al.size());


		//contains(Object)
		System.out.println(al.contains("tammy"));
		System.out.println(al.contains(30));


		//indexOf(Object)
		System.out.println(al.indexOf(20.5f));

		
		//lastIndexOf(Object)
		System.out.println(al.lastIndexOf(20.5f));

		
		//get(int)
		System.out.println(al.get(3));


		//set(int,Element)
		System.out.println(al.set(3,"Incubator"));
		System.out.println(al);


		
		//
		ArrayList al2 = new ArrayList();
		al2.add("Tohid");
		al2.add("Bala");
		al2.add("Raj");
		
		
		//addAll(Collection)
		al.addAll(al2);
		System.out.println(al);


		//addAll(index,collection)
		al.addAll(3,al2);
		System.out.println(al);


		//removeRange(int,int)
		al.removeRange(3,5);
		System.out.println(al);


		//remove(int)
		System.out.println(al.remove(3));
		System.out.println(al);


		//toArray
		Object arr[] = al.toArray();

		for(Object data : arr) {
		
			System.out.print(data + "  ");
		}

		System.out.println();

		
		
		//clear
		al.clear();
		System.out.println(al);
	}
}
