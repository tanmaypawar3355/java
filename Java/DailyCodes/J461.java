import java.util.*;


class ArrayListDemo {

	public static void main(String[] args) {

		ArrayList al = new ArrayList();

		al.add(10);
		al.add(20);
		al.add(30);
		al.add(new Integer(40));
		al.add(30);
		
		/* --------------------- 1
		for(Integer obj : al) {

			System.out.println(obj);
		}
		J461.java:15: error: incompatible types: Object cannot be converted to Integer
                for(Integer obj : al) {
                                  ^
		Note: J461.java uses unchecked or unsafe operations.
		Note: Recompile with -Xlint:unchecked for details.
		*/



		/* --------------------- 2
		for(var obj : al) {
			
			System.out.println(obj);
		}
		*/
		
		/* --------------------- 3
		for(Object obj : al) {
			
			System.out.println(obj);
		}
		*/

		for(int i = 0 ; i < al.size() ; i++) {

			System.out.println(al.get(i));
		}
	}
}	
