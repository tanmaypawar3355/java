class Demo{

	public static void main(String[] args){

		Demo obj = new Demo();
		obj.fun(true);

	}

	void fun(float x){

		System.out.println("In fun");
		System.out.println(x);
	}
}

/*

J138.java:6: error: method fun in class Demo cannot be applied to given types;
                obj.fun(true);
                   ^
  required: float
  found:    boolean
  reason: argument mismatch; boolean cannot be converted to float
2 errors
error: compilation failed

*/
