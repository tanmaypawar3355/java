import java.io.*;
import java.util.*;

class IODemo{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                                System.out.println("Enter player info");

                                String str = sc.nextLine();

                                StringTokenizer st = new StringTokenizer(str," ");

                                System.out.println("Count = " +st.countTokens());

                                String Token1 = st.nextToken();
                                String Token2 = st.nextToken();
                                String Token3 = st.nextToken();

                                String name = Token1;
                                int jerNo = Integer.parseInt(Token2);
                                float avg = Float.parseFloat(Token3);

                                System.out.println("Player name = " +Token1);
                                System.out.println("Jersey No = " +Token2);
                                System.out.println("Average = " +Token3);
        }
}
