class MyThread extends Thread {

	MyThread(String str) {

		super(str);
	}

	public void run() {
		
		System.out.println(getName());
		System.out.println(Thread.currentThread().getThreadGroup());
	}
}

class ThreadGroupDemo {

	public static void main(String[] args) {

		MyThread obj1 = new MyThread("xyz");
		obj1.start();
		
		MyThread obj2 = new MyThread("xyz");
		obj2.start();
		
		MyThread obj3 = new MyThread("xyz");
		obj3.start();
	}
}
