class ThreadDemo implements Runnable {

	public void run() {

		System.out.println(Thread.currentThread().getName());
	}
}

class MyThread {

	public static void main(String[] args) {

		ThreadDemo obj1 = new ThreadDemo();
		Thread T1 = new Thread(obj1);
		T1.start();
		
		ThreadDemo obj2 = new ThreadDemo();
		Thread T2 = new Thread(obj2);
		T2.start();
	}
}
