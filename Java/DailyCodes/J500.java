import java.util.*;

class SortedMapDemo {

	public static void main(String[] args) {

		SortedMap sm = new TreeMap();

		sm.put("Ind","India");
		sm.put("Pak","Pakistan");
		sm.put("SL","SriLanka");
		sm.put("Aus","Australia");
		sm.put("Ban","Bangladesh");
		
		System.out.println(sm);

		//subMap
		System.out.println(sm.subMap("Aus","Pak"));
		
		//headMap
		System.out.println(sm.headMap("Pak"));
		
		//tailMap
		System.out.println(sm.tailMap("Pak"));

		//firstKey
		System.out.println(sm.firstKey());

		//lastKey
		System.out.println(sm.lastKey());

		//keySet
		System.out.println(sm.keySet());

		//values
		System.out.println(sm.values());

		//entrySet
		System.out.println(sm.entrySet());
	}
}


