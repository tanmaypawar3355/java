class ArrayDemo{

	public static void main(String[] args){

		int arr1[] = {100,200,300,400};
		byte arr2[] = {1,2,3,4};
		short arr3[] = {10,20,30,40};
		//float arr4[] = {10.5,20.5,30.5};  incompatible types: possible lossy conversion from double to float - 3 pn data la
		
	
		System.out.println(arr1);
		System.out.println(arr2);
		System.out.println(arr3);
		//System.out.println(arr4);
	}
}
