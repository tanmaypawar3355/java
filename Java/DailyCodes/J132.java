class Demo {

	void fun(int x){

		System.err.println(x);
	}

	public static void main(String[] args){

		System.out.println("In main");

		Demo obj = new Demo();
		obj.fun();
		
		System.out.println("End main");
	}
}

/*

J132.java:13: error: method fun in class Demo cannot be applied to given types;
                obj.fun();
                   ^
  required: int
  found:    no arguments
  reason: actual and formal argument lists differ in length
1 error
error: compilation failed

*/

