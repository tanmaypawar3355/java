class NestedFor{

	public static void main(String[] args){

		int num = 1;
		char ch = 'A';
		int N = 4;

		for(int i = 1 ; i <= N ; i++){

			for(int j = 1 ; j <= N-i+1 ; j++){

				if(j % 2 == 1){

					System.out.print(ch++ +"  ");

				}else{
					System.out.print(num++ +"  ");
				}
			}
			System.out.println();
		}
	}
}


