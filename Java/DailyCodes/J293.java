class Demo {

	int x = 10;

	Demo() {
		
		System.out.println("No-args constructor");
	}

	Demo(int x) {

		System.out.println(x);
		System.out.println(this.x);
		System.out.println("Para args");
	}

	public static void main(String[] args) {

		Demo obj1 = new Demo();
		Demo obj2 = new Demo(50);
	}
}
