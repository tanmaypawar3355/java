interface Demo {

	void fun();
	void gun();
}

class DemoChild implements Demo {

	void fun() {

		System.out.println("In fun");
	}

	void gun() {
		
		System.out.println("In gun");
	}
}
