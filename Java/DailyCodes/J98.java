class NestedFor{

        public static void main(String[] args){

                int N = 4;
		int num = 1;

                for(int i = 1 ; i <= N ; i++){

                        for(int j = 1 ; j <= i ; j++){

                                if(j % 2 == 1){

                                        System.out.print(num++ +"  ");

                                }else{
                                        System.out.print(num*num++ +"  ");

                                }
                        }
                        System.out.println();
                }
        }
}
