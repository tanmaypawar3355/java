import java.io.*;

class Parsing{

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                int num1 = Integer.parseInt(br.readLine());
                float num2 = Float.parseFloat(br.readLine());
                br.skip(1);
                char num3 = (char)br.read();
                double num4 = Double.parseDouble(br.readLine());
                long num5 = Long.parseLong(br.readLine());
                byte num6 = Byte.parseByte(br.readLine());
                short num7 = Short.parseShort(br.readLine());
                boolean num8 = Boolean.parseBoolean(br.readLine());

                System.out.println();
                System.out.println("Print");
                System.out.println(num1);
                System.out.println(num2);
                System.out.println(num3);
                System.out.println(num4);
                System.out.println(num5);
                System.out.println(num6);
                System.out.println(num7);
                System.out.println(num8);
        }
}
