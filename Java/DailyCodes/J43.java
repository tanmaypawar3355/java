class Reverse{

	public static void main(String[] args){

		int x = 123;
		int rev = 0;

		for(          ; x > 0 ; x = x / 10){

			int rem = x % 10;

			rev = rev * 10 + rem;

		}

		System.out.println(rev);
	}
}
