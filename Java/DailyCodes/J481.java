import java.util.*;

class Movies {

	String movName = null;
	float movColl = 0.0f;
	float imdbRating = 0.0f;

	Movies(String movName,float movColl,float imdbRating) {
		
		this.movName = movName;
		this.movColl = movColl;
		this.imdbRating = imdbRating;
	}

	public String toString() {

		return movName + " : " + movColl + " : " + imdbRating;
	}
}

class SortByName implements Comparator { //<Movies> {

	//public int compare(Movies obi1,Movies obj2) {
	public int compare(Object obj1,Object obj2) {

		//return (((Movies)obj1).movName.compareTo(((Movies)obj2).movName));
		return (obj1.movName - obj2.movName);
	}
}

class SortByColl implements Comparator { 

	public int compare(Object obj1,Object obj2) {

		return (int) -(((Movies)obj1).movColl - (((Movies)obj2).movColl));
	}
}

class SortByRating implements Comparator {

	public int compare(Object obj1,Object obj2) {

		return (int) -(((Movies)obj1).imdbRating - (((Movies)obj2).imdbRating));
	}
}

class ListSortDemo {

	public static void main(String[] args) {

		ArrayList al = new ArrayList();

		al.add(new Movies("Gadar2",120.00f,8.8f));
		al.add(new Movies("OMG2",80.00f,8.1f));
		al.add(new Movies("Jailer",180.00f,9.9f));

		System.out.println(al);

		System.out.println("\nSorted by name");
		Collections.sort(al,new SortByName());
		System.out.println(al);
		
		System.out.println("\nSorted by collection");
		Collections.sort(al,new SortByColl());
		System.out.println(al);
		
		System.out.println("\nSorted by rating");
		Collections.sort(al,new SortByRating());
		System.out.println(al);
	}
}
