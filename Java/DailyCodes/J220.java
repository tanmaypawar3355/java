/*

method : public boolean endsWith(String suffix);

Description :

Predicate which determines if teh string ends with given suffix.
If the suffix is an empty String ,true is returned.
Throws NullPointerException if suffix is null

Parameters : prefix String to compare

Return type : boolean

*/

class EndsWithDemo {

	public static void main(String[] args) {

		String str1 = "Know The Code Till The Core";

		System.out.println(str1.endsWith("Core"));
	}
}
