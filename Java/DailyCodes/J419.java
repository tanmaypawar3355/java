import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int data = 0;

		try {

			data = Integer.parseInt(br.readLine());
		}catch(NumberFormatException obj) {
			
			System.out.println("Enter integer data");
			data = Integer.parseInt(br.readLine());
	
		}catch(IllegalArgumentException obj) {

			System.out.println("Please enter integer data");
			data = Integer.parseInt(br.readLine());
		}
			
		System.out.println(data);
	}
}


