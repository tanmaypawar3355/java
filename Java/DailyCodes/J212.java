import java.io.*;

class CheckStrings {

	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String str1 = br.readLine();
		String str2 = br.readLine();

		if(MyStrLen(str1) == MyStrLen(str2)) {

			System.out.println("Strings are equal");

		} else {
			
			System.out.println("Strings are not equal");
		}
	}

	static int MyStrLen(String str1) {

		char arr[] = str1.toCharArray();

		int count = 0;

		for(int i = 0 ; i < arr.length ; i++) {

			count++;
		}
		return count;
	}
}

