import java.util.*;

class CursorDemo {

	public static void main(String[] args) {

		ArrayList al = new ArrayList();

		al.add(10);
		al.add(20.5);
		al.add(30.5f);
		al.add("C2W");

		
		// forEach
		for(var x:al) {

			System.out.println(x);
			System.out.println(x.getClass().getName());
		}

		//iterator
		Iterator cur = al.iterator();
		System.out.println(cur.getClass().getName());


		System.out.println(cur.next());
		System.out.println(cur.next());
		System.out.println(cur.next());
		System.out.println(cur.next());
		//System.out.println(cur.next());
		

		while(cur.hasNext()) {
		
			System.out.println(cur.next());
			cur.remove();
		}

		System.out.println(al);

		// deleteing specific element using cursor
		ArrayList al2 = new ArrayList();
		
		al2.add("Rahul");
		al2.add("Kanha");
		al2.add("Ashish");
		al2.add("Tammy");

		Iterator curs = al2.iterator();

		while(curs.hasNext()) {

			if("Kanha".equals(curs.next())) {
				curs.remove();
			}
		}
		
		System.out.println(al2);


		// listIterator
		ListIterator litr = al.listIterator();

		System.out.println(litr.getClass().getName());

		while(litr.hasNext()) {

			System.out.println(litr.next());
		}
		
		while(litr.hasPrevious()) {

			System.out.println(litr.next());
		}
		
		while(litr.hasPrevious()) {

			System.out.println(litr.previous());
		}
		


		//Enumeration
		Vector v = new Vector();

		Enumeration curE = v.elements();
		System.out.println(curE.getClass().getName());

		while(curE.hasMoreElements()) {

			System.out.println(curE.nextElement());
		}		
	}
}
			
