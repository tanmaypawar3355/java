/*
 
method : public boolean equals(Object anObject);

Decsription :
anObject which compares anObject to this. This is true only for Strings with the same character sequence.
Returns true if anObject is semantically equal to this.

parameters : Object(anObject)
return type : boolean

*/

class EqualsDemo {

	public static void main(String[] args) {

		String str1  = "Shashi";
		String str2 = new String("Shashi");

		System.out.println(str1.equals(str2));

		/*

		char arr1[] = str1.toCharArray();
		char arr2[] = str2.toCharArray();

		int flag = 0;

		for(int i = 0 ; i < arr1.length ; i++){
			if(arr1[i]  == arr2[i]){
				flag = 1;
			}else{
				flag = 0;
				break;
			}
		}
		if(flag == 0){
			System.out.println("False");
		}else{
			System.out.println("True");
		}
		*/
	}
}
