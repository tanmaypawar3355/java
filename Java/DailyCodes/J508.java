import java.util.*;

class Project implements Comparable {

	String projName = null;
	int teamSize = 0;
	int duration = 0;

	Project(String projName,int teamSize,int duration) {

		this.projName = projName;
		this.teamSize = teamSize;
		this.duration = duration;
	}

	public String toString() {

		return "{" + projName + " : " + teamSize + " : " + duration + "}";
	}

	public int compareTo(Object obj) {

		return this.projName.compareTo(((Project)obj).projName);
	}
}

class PQDemo {

	public static void main(String[] args) {

		PriorityQueue pq = new PriorityQueue();

		pq.offer(new Project("QuizApp",50,2000));
		pq.offer(new Project("ChessGame",1000,5000));
		pq.offer(new Project("SPPUWeb",10,50));

		System.out.println(pq);
	}
}
