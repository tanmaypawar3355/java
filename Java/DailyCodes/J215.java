/*

method : public int indexOf(int ch,int fromIndex);

Desciption :

Finds the first instance of the character int the given String.

Parameters : character(ch to find)
	     Integer(index to starts the search)

return type : Integer

*/

class IndexOfDemo {

	public static void main(String[] args) {

		String str1 = "Shashi";

		System.out.println(str1.indexOf('h',0));
		System.out.println(str1.indexOf('h',2));
	}
}
