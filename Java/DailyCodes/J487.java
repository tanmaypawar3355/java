import java.util.*;

class HashMapDemo {

	public static void main(String[] args) {

		String str1 = "RAM";
		String str2 = "SAM";
		String str3 = "KAM";

		HashMap hm = new HashMap();

		hm.put(str1,"1");
		hm.put(str2,"2");
		hm.put(str3,"3");

		System.out.println(hm);
	}
}
