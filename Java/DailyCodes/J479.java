import java.util.*;

class Employee {

	String empName = null;
	float sal = 0.0f;

	Employee(String empName,float sal) {

		this.empName = empName;
		this.sal = sal;
	}

	public String toString() {

		return empName + " : " + sal;
	}
}

class ListSortDemo {

	public static void main(String[] args) {

		ArrayList al = new ArrayList();

		al.add(new Employee("Tammy",20.00f));
		al.add(new Employee("Saggy",15.00f));
		al.add(new Employee("Ranveer",50.00f));
		al.add(new Employee("Singh",20.00f));

		System.out.println(al);

		Collections.sort(al);
		
		System.out.println(al);
	}
}

