
/*
 
method : public String substring(int i, int j);

Descirption :
Crates a substring of the given string starting at a specified index & ending at an character before the specified index.

Parameters : Integer(staring Index),
	     Integer(endig Index)

Return type : String

*/


class SubStringDemo {

	public static void main(String[] args) {

		String str = "Core2Web";

		System.out.println(str.substring(5,8));
	}
}
