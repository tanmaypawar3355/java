/*
 
method : public String trim();

Descirption : trims all the white spaces before and after of the String
	      ex. str.trim();

Parameters : no parameter

Return type : String

*/

class TrimDemo {

	public static void main(String[] args) {

		String str = "    Know The Code Till The Core     ";

		String str1 = "    Know The Code Till The Core     ";

		System.out.println(str.trim());
		
		System.out.println(str1);
	}
}
