class StringDemo {

	public static void main(String[] args) {

		String str1 = "Shashi";
		String str2 = new String("Shashi");

		if(str1 == str2) {                               //checks the identityHashCode
			System.out.println("Equal");
		
		} else {
			
			System.out.println("Not equal");
		}

		if(str1.equals(str2)){                          //checks the content of strings(1-1 character compare karto)
			
			System.out.println("Equal");

		} else {

			System.out.println("Not equal");
		}
	}
}


