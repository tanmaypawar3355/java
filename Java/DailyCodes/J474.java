import java.util.*;

class MyClass implements Comparable<MyClass> {

	String str = null;

	MyClass(String str) {

		this.str = str;
	}

	public int compareTo(MyClass obj) {

		System.out.println("Obj.str =" + obj.str);
		System.out.println("This.str =" + this.str);
		System.out.println();

		//return (this.str).compareTo(obj.str);

		return (this.str).compareTo(((MyClass)obj).str);
	}
	
	public String toString() {

		return str;
	}
}

class TreeSetDemo {

	public static void main(String[] args) {

		TreeSet ts = new TreeSet();

		ts.add(new MyClass("Kanha"));
		ts.add(new MyClass("Ashish"));
		ts.add(new MyClass("Rahul"));
		ts.add(new MyClass("Badhe"));

		System.out.println(ts);
	}
}
