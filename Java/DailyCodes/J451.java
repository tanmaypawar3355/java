class MyThread extends Thread {

	MyThread(ThreadGroup tg, String str) {

		super(tg,str);
	}

	public void run() {

		System.out.println(Thread.currentThread());
	}
}

class ThreadGroupDemo {

	public static void main(String[] args) {

		ThreadGroup pthreadGp = new ThreadGroup("Core2Web");
		MyThread obj1 = new MyThread(pthreadGp, "C");
		
		
		ThreadGroup cthreadGp = new ThreadGroup("Incubator");
		MyThread obj2 = new MyThread(cthreadGp, "C++");
				
		
		ThreadGroup tthreadGp = new ThreadGroup("Tammy");
		MyThread obj3 = new MyThread(tthreadGp, "Pyhton");

		obj1.start();
		obj2.start();
		obj3.start();
	}
}
