/*
 
5] toString

method : public String toString();

Description :
- Convert this <code>StringBuffer</code> to a <code>String<.code>.
- The String is composed of the characters currently in this StringBuffer.
- Notw that the result is a copy & that future modifications to this buffer do not affect the String.

Parameters : No parameter

Return type : String(the characters int this StringBuffer)

*/

class MyToString {

	public static void main(String[] args) {

		StringBuffer str1 = new StringBuffer("know the code till the core");

		String str2 = "Core2Web ";

		String str3 = str1.toString();

		String str4 = str2.concat(str3);

		System.out.println(str4);
	}
}
