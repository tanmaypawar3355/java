/*
 
3] delete

method : public synchronized StringBuffer insert(int start,int end);

Description :
- Delete characters from this <code>StringBuffer</code>.
  <code>delete</code> will delete 10 & 11 but not 12.
- It is harmless for the end to be larger than length().

Parameters : - Integer(start the first character to delete)
	     - Integer(end the index after last character to delete)

Return type : StringBuffer(this <code>StringBuffer</code>)

*/

class MyDelete {

	public static void main(String[] args) {

		StringBuffer sb = new StringBuffer("Core2Web");

		sb.delete(2,7);

		System.out.println(sb);
	}
}
