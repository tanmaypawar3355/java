class Demo {

        void fun(String str) {

                System.out.println("String");
        }

        void fun(StringBuffer str1) {

                System.out.println("StringBuffer");
        }
}

class Client {

        public static void main(String[] args) {

		// String str = null;
		//					hey donhi chalata
		// StrngBuffer sb = null;

                Demo obj = new Demo();

                obj.fun("Core2Web");

                obj.fun(new StringBuffer("Core2Web"));

		obj.fun(null);
        }
}
