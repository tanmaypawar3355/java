interface Demo1 {

	static void m1() {

		System.out.println("Demo1 - m1");
	}
}

interface Demo2 {

	static void m1() {
		
		System.out.println("Demo2 - m1");
	}
}

interface Demo3 extends Demo1,Demo2 {


}
