class Parent {

	static int x = 10;

	static void fun() {
		
		System.out.println("Parent fun");
	}
}

class Client {

	public static void main(String[] args) {

		Parent.fun();
		System.out.println(Parent.x);
	}
}
