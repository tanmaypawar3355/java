import java.io.*;

class CompareDemo {
       
	public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                String str1 = br.readLine();
                String str2 = br.readLine();

		if(str1.length() == str2.length()) {

			int ret = MyCompareString(str1,str2);

			System.out.println(ret);			
		
		} else {
			System.out.println("Strings are not equal");
               }
	}

	static int MyCompareString(String str1,String str2) {

		char arr1[] = str1.toCharArray();
		char arr2[] = str2.toCharArray();

		for(int i = 0 ; i < arr1.length ;    ) {

			if(arr1[i] == arr2[i]) {

				i++;
			}else{
				return arr1[i] - arr2[i];
			}
		}
		return 0;
	}
}

