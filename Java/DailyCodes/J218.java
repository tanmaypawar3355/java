/*
 
method : public String substring(int index);

Description:
Creates a substring of the given String starting at a specified index & ending at the end of given String.

parameters : Integer(index of the String)

return type : String

*/

class SubstringDemo {

	public static void main(String[] args) {

		String str1 = "Core2Web Tech";

		System.out.println(str1.substring(5));
		
		System.out.println(str1.substring(0,3));
	}
}

