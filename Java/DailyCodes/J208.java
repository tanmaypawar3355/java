/*
 
method : public int compareTo(String str2);

Description :
It compares the str1 & str2 (case sensitive) ,if both the strings are equal ot return 0 otherwise returns the comparison
ex . str1.compareTo(str2);

parameter : String(second string)
return type : integer

*/

class CompareToDemo {

	public static void main(String[] args) {

		String str1 = "Ashish";
		String str2 = "ashish";

		System.out.println(str1.compareTo(str2));
		
		String str3 = "Ashish";
		String str4 = "Ashish";
		
		System.out.println(str3.compareTo(str4));
	
		/*	
		char arr1[] = str1.toCharArray();
		char arr2[] = str2.toCharArray();

		int flag = 0;

		for(int i = 0 ; i < arr1.length ; i++){

			if(arr1[i] == arr2[i]){
				flag = 1;
			}else{
				flag = 0;
				break;
			}
		}

		if(flag == 0){
			System.out.println("Not same");
		}else{
			System.out.println("Same");
		}	
		*/	
	}
}
