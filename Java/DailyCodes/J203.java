class HashCodeDemo {

	public static void main(String[] args) {

		String str1 = "Tammy";
		String str2 = new String("Tammy");
		String str3 = "Tammy";
		String str4 = new String("Tammy");

		System.out.println(str1.hashCode());
		System.out.println(str2.hashCode());
		System.out.println(str3.hashCode());
		System.out.println(str4.hashCode());
	}
}


