class Demo {

	static void fun() {
		
		System.out.println("In fun Demo");
	}
}

class DemoChild extends Demo {

}

class Client {

	public static void main(String[] args) {

		DemoChild obj1 = new DemoChild();
		obj1.fun();
		
		DemoChild obj2 = new DemoChild();
		obj2.fun();
	}
}
