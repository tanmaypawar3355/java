/*

method : public synchronized int lastIndexOf(String str, int fromIndex);

Description :
- Finds the last instance of a string in this StringBuffer, starting at a given index.
- If starting index is greater than the maximum valis index, then the search begins at the end o fthe string.
i Is the starting index is less than zero, or the substring is not found, -1 is returned.

Parameters : - String(str String to find).
	     - Integer(fromIndex index to start the search)

Return type : - Integer(location (base 0) of the String or -1 if not found)

*/

class MyLastIndexOf {

	public static void main(String[] args) {

		StringBuffer str1 = new StringBuffer("Core2web");

		System.out.println(str1.lastIndexOf("e"));
	}
}
