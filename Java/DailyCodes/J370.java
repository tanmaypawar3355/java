interface Demo1 {

	static void fun() {

		System.out.println("In fun Demo");
	}
}

interface Demo2 {

	default void gun() {
		
		System.out.println("In gun Demo");
	}
}
