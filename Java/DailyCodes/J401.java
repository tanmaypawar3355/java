class Outer {

	int x = 10;
	static int y = 20;

	static class Inner {

	     // int a = 30;
		final static int c = 50;
	}
}

class Client {

	public static void main(String[] args) {

                System.out.println(Outer.y);
		Outer obj = new Outer();
                System.out.println(obj.x);

                System.out.println(Outer.Inner.c);
	}
}



