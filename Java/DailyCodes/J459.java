import java.util.*;

class CollectionDemo {

	public static void main(String[] args) {

		List<Integer> obj = new ArrayList<Integer>();

		obj.add(10);
		obj.add(20);

		obj.add("Tammy");
		
		System.out.println(obj);
	}
}
