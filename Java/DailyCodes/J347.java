class Parent {

        static int x = 10;

        static void fun() {

                System.out.println("Parent fun");
        }
}

class Child extends Parent {

	void fun() {
		
		System.out.println("Child fun");
	}
}

class Client {

        public static void main(String[] args) {

                Parent.fun();
                System.out.println(Parent.x);
        }
}
