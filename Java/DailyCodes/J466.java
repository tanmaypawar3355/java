import java.util.*;

class VectorDemo {

	public static void main(String[] args) {

		Vector v = new Vector();

		v.addElement(10);
		v.addElement(20);
		v.addElement(30);
		v.addElement(40);

		System.out.println(v);
		System.out.println(v.capacity());

		v.removeElement(20);
		System.out.println(v);
		
		v.removeElement(2);
		System.out.println(v);

		ArrayList al = new ArrayList();
		al.add("Tammy");
		al.add("Tohid");
		al.add("Abhi");

		Vector ve = new Vector(al);
		System.out.println(al);
	}
}

