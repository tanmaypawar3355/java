import java.io.*;

class InputDemo{

        public static void main(String[] args)throws IOException{

                InputStreamReader isr = new InputStreamReader(System.in);

                System.out.println("Enter char");
                char ch = (char)isr.read();
                System.out.println(ch);

		isr.close();

		System.out.println("Enter char");
		int ch1 = isr.read();
		
		System.out.println("Enter char");
		int ch2 = isr.read();
        }
}
