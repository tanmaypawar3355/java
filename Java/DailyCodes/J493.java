import java.util.*;

class IdentityHashMapDemo {

	public static void main(String[] args) {

		HashMap hm1 = new HashMap();

		hm1.put(10,"Rahul");
		hm1.put(10,"Badhe");
		System.out.println(hm1);

		HashMap hm2 = new HashMap();
		
		hm2.put(new Integer(10),"Rahul");
		hm2.put(new Integer(10),"Badhe");
		System.out.println(hm2);

		IdentityHashMap ihm = new IdentityHashMap();
		
		ihm.put(new Integer(10),"Kanha");
		ihm.put(new Integer(10),"Rahul");
		ihm.put(new Integer(10),"Badhe");
		System.out.println(ihm);
	}
}
	
		
