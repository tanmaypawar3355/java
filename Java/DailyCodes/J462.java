import java.util.*;

class Biencaps {

	String compName = null;
	int compEmp = 0;

	Biencaps(String compName, int compEmp) {

		this.compName = compName;
		this.compEmp = compEmp;
	}
}

class ArrayListDemo {

	public static void main(String[] args) {

		List obj = new ArrayList();

		obj.add(10);
		obj.add(10.5);
		obj.add(new Biencaps("Tammy",20));

		System.out.println(obj);
	}
}
