class Automorphic{

	public static void main(String[] args){

		int N = 25;    
		int num = N;
		int square = N * N;
		int count = 0;

		while(num != 0){

			count++;
			num = num / 10;
		}

		int temp1 = N;
		int flag = 0;

		while(count > 0){
			
			int temp2 = square % 10;
			int temp3 = temp1 % 10;

			if(temp2 == temp3){

				flag = 1;
			
			}else{
				flag = 0;
			}

			square = square / 10;
			temp1 = temp1 / 10;
			count--;
		}

		if(flag == 1){

			System.out.println(N+ " is a automorphic no");

		}else{
			System.out.println(N+ " is not an automorphic no");
		}
	}
}




