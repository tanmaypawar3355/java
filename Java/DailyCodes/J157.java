import java.util.*;

class BoilerPlateCode{

        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);

                System.out.println("Enter array size");
                int size = sc.nextInt();

                int arr[] = new int[size];

                int count1 = 0,count2 = 0;

                System.out.println("Enter elements");

                for(int i = 0 ; i < arr.length ; i++){

                        arr[i] = sc.nextInt();
                        if(arr[i] % 2 == 0){
                                count1++;
                        
			}else{
				count2++;
			}
                }

                System.out.println("Even count = "+count1);
                System.out.println("Odd count = "+count2);
        }
}
