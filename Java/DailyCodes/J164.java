class IntegerCache{

	public static void main(String[] args){

		int x = 10;
		int y = 20;

		System.out.println(System.identityHashCode(x));
		System.out.println(System.identityHashCode(y));
		
		int a = 10;
		int b = 10;
		
		System.out.println(System.identityHashCode(a));
		System.out.println(System.identityHashCode(b));
	}
}
