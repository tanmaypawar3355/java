interface Demo {

	int x = 10; // public static fina int x;
		   
	void fun(); // public abstaract void fun();
}

class DemoChild implements Demo {

	public void fun() {

		System.out.println(Demo.x);
		System.out.println(x);
	}
}

class Client {

	public static void main(String[] args) {

		Demo obj = new DemoChild();
	
		obj.fun();
	}
}

