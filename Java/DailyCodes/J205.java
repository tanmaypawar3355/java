/*

method : public int length();

Description :
- It returns the number of characters contained in given String

parameters : No parameter
Return type : Integer

*/

class LengthDemo {

	public static void main(String[] args) {

		String str1 = "Core2Web";

		System.out.println(str1.length());
	}
}
