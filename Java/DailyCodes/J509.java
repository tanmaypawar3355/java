import java.util.*;

class Project {

	String projName = null;
	int teamSize = 0;
	int duration = 0;

	Project(String projName,int teamSize,int duration) {

		this.projName = projName;
		this.teamSize = teamSize;
		this.duration = duration;
	}

	public String toString() {

		return "{" + projName + " : " + teamSize + " : " + duration + "}";
	}
}

class SortByName implements Comparator {

	public int compare(Object obj1,Object obj2) {

		return ((((Project)obj1).projName).compareTo(((Project)obj2).projName));
	}
}

class SortByTeamSize implements Comparator {

        public int compare(Object obj1,Object obj2) {

                return ((((Project)obj1).teamSize) - (((Project)obj2).teamSize));
        }
}

class SortByDuration implements Comparator {

        public int compare(Object obj1,Object obj2) {

                return ((((Project)obj1).duration) - (((Project)obj2).duration));
        }
}

class PQDemo {

	public static void main(String[] args) {

		PriorityQueue pq = new PriorityQueue(new SortByName());

		pq.offer(new Project("QuizApp",50,2000));
		pq.offer(new Project("ChessGame",1000,5000));
		pq.offer(new Project("SPPUWeb",10,50));

		System.out.println(pq);
	}
}
