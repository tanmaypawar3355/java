/*
       1
method : String concat(String str);

Description :
- Concatinate String to this String i.e Another String is concatinatiated with the first String.
- Impelents new array of character whose length is sum of str1.length & str2.length

parameter : String (only 1)
Return type : String

*/

class ConcatDemo {

	public static void main(String[] args) {

		String str1 = "Core2";
		String str2 = "Web";

		String str3 = str1.concat(str2);
		System.out.println(str3);

		/*
		My
		String str5 = str1 + str2;
		System.out.println(str5);
		*/
	}
}
