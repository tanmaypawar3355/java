class MyThread extends Thread {

	MyThread(ThreadGroup tg, String str) {

		super(tg,str);
	}

	public void run() {

		System.out.println(Thread.currentThread());
	}
}

class ThreadGroupDemo {

	public static void main(String[] args) {

		ThreadGroup pThreadGp = new ThreadGroup("India");
		MyThread t1 = new MyThread(pThreadGp,"Maha");
		MyThread t2 = new MyThread(pThreadGp,"Goa");
		t1.start();
		t2.start();


		ThreadGroup cThreadGp = new ThreadGroup(pThreadGp,"Pakistan");
		MyThread t3 = new MyThread(pThreadGp,"Karachi");
		MyThread t4 = new MyThread(pThreadGp,"Lahore");
		t3.start();
		t4.start();


		ThreadGroup mThreadGp = new ThreadGroup(pThreadGp,"Bangladesh");
		MyThread t5 = new MyThread(pThreadGp,"Dhaka");
		MyThread t6 = new MyThread(pThreadGp,"Bhaka");
		t5.start();
		t6.start();

		cThreadGp.interrupt();

		System.out.println(pThreadGp.activeCount());
		System.out.println(pThreadGp.activeGroupCount());

	}
}
