class Demo {

        static {

                System.out.println("Static block 1");
        }

        public static void main(String[] args) {

                System.out.println("In Demo main");
        }
}

class Client {

        public static void main(String[] args) {

                System.out.println("In Client main");
		Demo obj = new Demo();
        }
}
