import java.util.concurrent.*;
import java.util.*;

class Employee {

	String name;

	Employee(String name) {
		
		this.name = name;
	}

	public String toString() {

		return name;
	}
}

class SortByName implements Comparator {

	public int compare(Object obj1, Object obj2) {

		return ((((Employee)obj1).name).compareTo(((Employee)obj2).name));
	}
}

class PriorityBlockingQueueDemo {

	public static void main(String[] args) {

		BlockingQueue bq = new PriorityBlockingQueue(4,new SortByName());

		bq.offer(new Employee("Kanha"));
		bq.offer(new Employee("Ashish"));
		bq.offer(new Employee("Rahul"));
		bq.offer(new Employee("Badhe"));

		System.out.println(bq);
	}
}


