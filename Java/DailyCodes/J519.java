interface Core2Web {

	void lang();
}

class Year2022 {

	public static void main(String[] args) {

		/*		
		Core2Web obj = new Core2Web() {

			public void lang() {

				System.out.println("Bootcamp,DS,Java,Python");
			}
		};

		obj.lang();
		*/
		
		Core2Web c2w = () -> {
				
			System.out.println("Bootcamp,DS,Java,Python");
		};
		
		c2w.lang();
	}
}
