import java.util.*;

class HashMapDemo {

	public static void main(String[] args) {

		HashSet hs = new HashSet();
			     
	        hs.add("Kanha");
		hs.add("Ashish");
		hs.add("Badhe");
		hs.add("Rahul");
		System.out.println(hs);

		HashMap hm = new HashMap();

		      // Key     Value
		hm.put("Kanha","Infosys");
		hm.put("Ashish","Dell");
		hm.put("Badhe","Zudio");
		hm.put("Rahul","H & M");
		System.out.println(hm);
	}
}
