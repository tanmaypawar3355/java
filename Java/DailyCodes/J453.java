class MyThread implements Runnable {

	public void run() {
		
		System.out.println(Thread.currentThread());

		try {
			Thread.sleep(5000);
		}catch(InterruptedException ie) {

			System.out.println(ie.toString());
		}
	}
}

class ThreadGroupDemo {

        public static void main(String[] args)  {

                ThreadGroup pThreadGp = new ThreadGroup("India");
                MyThread obj1 = new MyThread();
                MyThread obj2 = new MyThread();
                Thread t1 = new Thread(pThreadGp,obj1,"Maha");
                Thread t2 = new Thread(pThreadGp,obj2,"Goa");
		t1.start();
                t2.start();


                ThreadGroup cThreadGp = new ThreadGroup("Pakistan");
                MyThread obj3 = new MyThread();
                MyThread obj4 = new MyThread();
                Thread t3 = new Thread(pThreadGp,obj3,"Lahore");
                Thread t4 = new Thread(pThreadGp,obj4,"Karachi");
                t3.start();
                t4.start();


                ThreadGroup mThreadGp = new ThreadGroup("Bangladesh");
                MyThread obj5 = new MyThread();
                MyThread obj6 = new MyThread();
		Thread t5 = new Thread(pThreadGp,obj5,"Dhaka");
                Thread t6 = new Thread(pThreadGp,obj6,"Bhaka");
                t5.start();
                t6.start();

                cThreadGp.interrupt();

                System.out.println(pThreadGp.activeCount());
                System.out.println(pThreadGp.activeGroupCount());

        }
}
