class Parent {

	Parent() {
		
		System.out.println("In parent constructor");

	}

	void parentProperty() {
		
		System.out.println("Flat,car,gold");
	}
}

class Child extends Parent {

	Child() {
		
		System.out.println("In child constructor");
	}
}

class Client {

	public static void main(String[] args) {

		Child obj2 = new Child();
		obj2.parentProperty();
	}
}
