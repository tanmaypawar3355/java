class Demo{

        public static void main(String[] args){

                Demo obj = new Demo();
                obj.fun(10);
		obj.fun(10.5f);
        }

        void fun(int x){

                System.out.println("In fun");
                System.out.println(x);
        }
}

/*

J136.java:7: error: method fun in class Demo cannot be applied to given types;
                obj.fun(10.5f);
                   ^
  required: int
  found:    float
  reason: argument mismatch; possible lossy conversion from float to int
1 error
error: compilation failed

*/
