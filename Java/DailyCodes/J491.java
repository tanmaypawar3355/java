import java.util.*;

class LinkedHashMapDemo {

	public static void main(String[] args) {

		LinkedHashMap lhm = new LinkedHashMap();

		lhm.put("Badhe","Infosys");
		lhm.put("Kanha","Zudio");
		lhm.put("Ashish","H&M");
		lhm.put("Rahul","BMC");

		System.out.println(lhm);
	}
}
