class Parent {

	Object fun() {

		return new Object();
		//Object obj = new Object();
		//return obj;
	}
}

class Child extends Parent {

	String fun() {

		return new String("Tammy");
	}
}

class Client {

	public static void main(String[] args) {

		Parent obj = new Child();
		obj.fun();
	}
}
