import java.util.*;

class NavigableSetDemo {

	public static void main(String[] args) {

		NavigableSet ns = new TreeSet();

		ns.add("Kanha");
		ns.add("Rahul");
		ns.add("Ashish");
		ns.add("Badhe");
		ns.add("Tammy");

		System.out.println(ns);
		
		System.out.println(ns.lower("Badhe"));
		
		System.out.println(ns.higher("Badhe"));
		
		System.out.println(ns.floor("Kanha"));
		
		System.out.println(ns.ceiling("Ashish"));

		System.out.println(ns.pollFirst());
		
		System.out.println(ns.pollLast());
		
		System.out.println(ns.iterator());
		
		System.out.println(ns.descendingSet());
		
		System.out.println(ns.descendingIterator());
		
		System.out.println(ns.subSet("Rahul",true,"Tammy",true));

		System.out.println(ns.headSet("Kanha",true));
		
		System.out.println(ns.tailSet("Kanha",true));

		System.out.println(ns.subSet("Ashish","Rahul"));
		
		System.out.println(ns.headSet("Kanha"));
		
		System.out.println(ns.tailSet("Badhe"));
	}
}



