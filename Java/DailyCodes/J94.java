class NestedFor{

	public static void main(String[] args){

		int num = 1;
		int N = 4;

		for(int i = 1 ; i <= N ; i++){

			for(int j = 1 ; j <= i ; j++){

				System.out.print(num*num++ +"  ");
			}
			System.out.println();
		}
	}
}
