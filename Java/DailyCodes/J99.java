class NestedFor{

        public static void main(String[] args){

                int N = 4;
		int num = 1;
		char ch = 'A';

                for(int i = 1 ; i <= N ; i++){

                        for(int j = 1 ; j <= i ; j++){

                                if(j % 2 == 1){

                                        System.out.print(num++ +"  ");
					ch++;

                                }else{
                                        System.out.print(ch++ +"  ");
					num++;

                                }
                        }
                        System.out.println();
                }
        }
}
