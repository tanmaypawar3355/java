abstract class Brand {

	void product() {
		
		System.out.println("3 products ");
	}

	abstract void price();
}

class Franchiese extends Brand {

	void price() {
		
		System.out.println("My price");
	}
}

class Client {

	public static void main(String[] args) {

		Brand obj = new Franchiese();
		obj.product();
		obj.price();
	}
}
