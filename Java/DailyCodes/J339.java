class Parent {

        String fun() {

                return new String();
        }
}

class Child extends Parent {

        Object fun() {

                return new Object();
        }
}

class Client {

        public static void main(String[] args) {

                Parent obj = new Child();
                obj.fun();
        }
}
