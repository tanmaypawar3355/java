import java.util.concurrent.*;

class Employee implements Comparable {

	String name;

	Employee(String name) {
		
		this.name = name;
	}

	public String toString() {

		return name;
	}

	public int compareTo(Object obj) {

		return (this.name).compareTo(((Employee)obj).name);
	}

}

class PriorityBlockingQueueDemo {

	public static void main(String[] args) {

		BlockingQueue bq = new PriorityBlockingQueue();

		bq.offer(new Employee("Kanha"));
		bq.offer(new Employee("Ashish"));
		bq.offer(new Employee("Rahul"));
		bq.offer(new Employee("Badhe"));

		System.out.println(bq);
	}
}


