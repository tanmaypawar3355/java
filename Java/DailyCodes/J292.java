class Demo {

	int x = 10;

	Demo() {

		this(70);
	}

	Demo(int x) {

		this();      //      super();
		             //        OR
   		super();    //       this();
	}
}
