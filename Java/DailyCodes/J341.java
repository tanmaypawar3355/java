class Parent {

	void fun() {       // access specifier default 

		System.out.println("Parent fun");
	}
}

class Child extends Parent {

	public void fun() {       // access specifier public 
		
		System.out.println("Child fun");
	}
}


