/*

4] Reverse

method : public synchronized StringBuffer reverse();

Description :
- Reverse the characters in this StringBuffer.
- The same sequence of characters exists, but in the reverse index ordering.

Parameters : No parameter 

Return type : StringBuffer(this <code>StringBuffer</code>).

*/

class ReverseDemo {

	public static void main(String[] args) {

		StringBuffer sb = new StringBuffer("Shashi");

		System.out.println(sb.reverse());
	}
}
