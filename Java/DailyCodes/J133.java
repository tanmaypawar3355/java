class Demo{

	public static void main(String[] args){

		Demo obj = new Demo();
		obj.fun();

	}

	void fun(int x){
		System.out.println("In fun");
	}
}

/*

133.java:6: error: method fun in class Demo cannot be applied to given types;
                obj.fun();
                   ^
  required: int
  found:    no arguments
  reason: actual and formal argument lists differ in length
1 error
error: compilation failed

*/
