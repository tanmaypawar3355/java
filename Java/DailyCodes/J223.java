/*
 
method : public String toUpperCase();

Description : it uppercases to this String
	      ex.  str.toUpperCase();

Parameters : no parameter

Return type : String

*/

class toUpperCaseDemo {

	public static void main(String[] args) {

		String str = "core2web";

		System.out.println(str.toUpperCase());
	}
}
