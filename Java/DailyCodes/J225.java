/*

method : public String[] split(String delimiter(;

Description : Splits this String around matches of regular expressions.

Parameter : delimiter(pattern to match)

Return type : String[](array of split strings)

*/

class SplitDemo {

	public static void main(String[] args) {

		String str = "Know the code till the core";

		String[] strResult = str.split(" ");

		for(int i = 0 ; i < strResult.length ; i++) {

			System.out.println(strResult[i]);
		}
	}
}
