/*
 

method : public int compareToIgnoreCase(String str);

Description :
It compare str1 & str2 (case sensitive)

paramteres : String
return type : Integer

*/

class CompareIgnore {

	public static void main(String[] args) {

		String str1 = "SHASHI";
		String str2 = "sHashi";

		System.out.println(str1.compareToIgnoreCase(str2));

		String str3 = "SHASHI";
		String str4 = "shashikant";
		
		System.out.println(str3.compareToIgnoreCase(str4));

		/*
		char arr1[] = str1.toCharArray();
                char arr2[] = str2.toCharArray();

                int flag = 0;

                for(int i = 0 ; i < arr1.length ; i++){

                        if(arr1[i] == arr2[i] || (arr1[i] - arr2[i]) == -32 || (arr1[i] - arr2[i]) == 32){
                                flag = 1;
                        }else{
                                flag = 0;
                                break;
                        }
                }

                if(flag == 0){
                        System.out.println("Not same");
                }else{
                        System.out.println("Same");
                }
		*/
	}
}


