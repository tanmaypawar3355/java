class Parent {

	void fun() {
		
		System.out.println("Parent fun");
	}
}

class Child extends Parent {

	private void fun() {
		
		System.out.println("Child fun");
	}
}
