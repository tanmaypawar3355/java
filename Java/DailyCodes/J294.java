class Player {

	private int jerNo = 18;
	private String name = null;

	Player(int jNo, String pName) {

		jerNo = jNo;
		name = pName;
	}

	void info() {
		
		System.out.println(name + " = " + jerNo);
	}
}

class Client {

	public static void main(String[] args) {

		Player obj1 = new Player(18,"Virat");
		obj1.info();
		
		Player obj2 = new Player(7,"MSD");
		obj2.info();
		
		Player obj3 = new Player(45,"Rohit");
		obj3.info();
	}
}
