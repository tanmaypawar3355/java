class StringBufferDemo {

	public static void main(String[] args) {

		StringBuffer str1 = new StringBuffer("");

		System.out.println(str1.capacity());

		StringBuffer str2 = new StringBuffer("Hi");
		
		System.out.println(str2.capacity());
	}
}
