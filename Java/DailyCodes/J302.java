class Parent {

	static int x = 10;
}

class Child extends Parent {
	
	System.out.println(x);
}

class Client {

	public static void main(String[] args) {

		Child obj = new Child();
	}
}
