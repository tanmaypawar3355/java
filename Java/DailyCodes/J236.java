/*

1] Append

method : public synchronized StringBuffer append(String str);

Description :
-Append th <code>String</code> to the <code>StringBuffer</code>.
-If str is null, the String "null" is appended.

Parameters : String(str the <code>String</code>

Return type : StringBuffer(this <code>String</code>

*/

class MyAppend {

	public static void main(String[] args) {

		StringBuffer str1 = new StringBuffer("Hello");
		String str2 = " World";
		str1.append(str2);
		System.out.println(str1);
	}
}
