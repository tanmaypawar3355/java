class Demo {

	int x = 10;

	Demo() {

		this(70);
		System.out.println("No-args");
	}

	Demo(int x) {

		this();
		System.out.println("Para");
	}

	public static void main(String[] args) {

		Demo obj2 = new Demo(50);
	}
}
