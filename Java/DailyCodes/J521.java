interface Core2Web {

	String lang();
}

class Year2022 {

	public static void main(String[] args) {

		Core2Web c2w = () -> "Bootcamp,DS,Java,Python";

		System.out.println(c2w.lang());
	}
}
