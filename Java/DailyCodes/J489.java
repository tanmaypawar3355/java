import java.util.ArrayList;
import java.util.List;

class HashTable {
    private int size;
    private List<Integer>[] table;

    @SuppressWarnings("unchecked")
    public HashTable(int size) {
        this.size = size;
        table = new ArrayList[size];
    }

    private int hashFunction(int value) {
        return value % size;
    }

    public void insert(int value) {
        int hashValue = hashFunction(value);
        if (table[hashValue] == null) {
            table[hashValue] = new ArrayList<>();
        }
        table[hashValue].add(value);
    }

    public void display() {
        for (int index = 0; index < size; index++) {
            System.out.print("Index " + index + ": ");
            if (table[index] != null) {
                for (Integer value : table[index]) {
                    System.out.print(value + " ");
                }
            }
            System.out.println();
        }
    }
}

class Main {
    public static void main(String[] args) {
        HashTable hashTable = new HashTable(11);

       int[] values = {15, 22, 10, 8, 16, 45, 13, 11, 26, 37 };

        for (int value : values) {
            hashTable.insert(value);
        }

        hashTable.display();
    }
}

