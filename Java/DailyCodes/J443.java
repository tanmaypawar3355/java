class MyThread extends Thread {

	public void run() {

		Thread t = Thread.currentThread();
		System.out.println(t.getName());
	}
}

class ThreadDemo {

	public static void main(String[] args) throws InterruptedException {

		Thread t = Thread.currentThread();
		System.out.println(t.getName());

		MyThread obj1 = new MyThread();
		obj1.start();

		t.sleep(5000);

		obj1.start();
	}
}
		

