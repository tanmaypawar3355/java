class Demo {

	int x = 10;

	Demo() {

		System.out.println(x);
	}
	
	Demo() {

		System.out.println(x);
	}
}

/*
 error: constructor Demo() is already defined in class Demo
        Demo() {

	Karan method table banavla jato compile time tevha Demo()
							   Demo()

	Asa method table compiler la disto tyala kalat nhi kuth jau tyamula error
*/
