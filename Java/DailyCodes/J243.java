/*
 
7] indexOf

method : public synchronized int index of (String str, in fromIndex)

Description :
- Finds the first instance of  a string in this StringBuffer starting at a given index.
- If the starting index is less than 0, the search starts at the beginning of this string.
- If a starting index is greater than the length of the string or the substring is not found  -1 is returned.

Parameters : - String(str String to find).
	     - Integer(fromIndex index to start the search).

Return type : Integer(location (base 0) of the string or -1 if not found)

*/

class MyIndexOf {

	public static void main(String[] args) {

		StringBuffer str1 = new StringBuffer("Core2Web");

		System.out.println(str1.indexOf("e"));
	}
}
