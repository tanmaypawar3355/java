class NestedFor{

        public static void main(String[] args){


                for(int i = 1 ; i <= 3 ; i++){

			char ch = 'A';

                        for(int j = 1 ; j <= 3 ; j++){

                                System.out.print(ch++ +" ");
                }
                System.out.println();
                }
        }
}

/*   BY USING ASCII VALUES

class NestedFor((1)){

        public static void main(String[] args){


                for(int i = 1 ; i <= 3 ; i++){

                        for(int j = 1 ; j <= 3 ; j++){

                                System.out.print((char)(64 + j)+" ");
                }
                System.out.println();
                }
        }
}

class NestedFor((2)){

        public static void main(String[] args){


                for(int i = 1 ; i <= 3 ; i++){

                    int ch = 65;

                        for(int j = 1 ; j <= 3 ; j++){

                                System.out.print((char)(ch++)+" ");
                }
                System.out.println();
                }
        }
}
*/
