/*
 method : public boolean startsWith(String prefix,int toffset);

Description :
Predicate which determines if the given String contains the given prefix beginning comparison at toffset 
The result is false if the toffset is negatuve or greater than str.length().

Parameters : prefix String to compare,toffset for this String where the comparison starts.

return type : boolean


*/

class startsWithDemo {

	public static void main(String[] args) {

		String str = "Core2Web";

		System.out.println(str.startsWith("or",1));
	}
}
