/*
 
6] length

method : public synchronized int length();

Description :
- Get the length of the <code>String</code>. This <code>StringBuffer<code> woukd create.
- Not to be confused with the <em>capacity</em> of the <code>StrinBuffer</code>

Parameter : No parameter

Return type : Integer(the length of this <code>StringBuffer</code>)

*/

class MyLength {

	public static void main(String[] args) {

		StringBuffer str1 = new StringBuffer("Core2Web");

		System.out.println(str1.length());

	}
}
