class Outer {

	class Inner {
		
		void m1() {	
			
			System.out.println("In m1-inner");
		}
	}

	void m2() {
                
		System.out.println("I m2-outer");
	}
}

class Client {

	public static void main(String[] args) {

		Outer obj = new Outer();
		obj.m2();
	     // obj.m1();                   // cannot find symbol

	     // Inner obj1 = new Inner();  // hey chalat nhi tyala reference pn milat nhi . object pn milat nahi

		Outer.Inner obj1 = obj.new Inner();
	     // Outer.Inner obj1 = new Outer().new Inner();	
	     	
		obj1.m1();
	}
}
