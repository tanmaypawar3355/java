import java.util.*;

class PQueueDemo {

	public static void main(String[] args) {

		PriorityQueue pq = new PriorityQueue();

		pq.offer("Kanha");
		pq.offer("Ashish");
		pq.offer("Rahul");
		pq.offer("Badhe");
		pq.offer("Rajesh");

		System.out.println(pq);
	}
}
