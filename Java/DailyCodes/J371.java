class Demo {

	static void fun() {
		
		System.out.println("In fun Demo");
	}
}

class DemoChild extends Demo {

	void fun() {
		
		System.out.println("In fun ChildDemo");
	}
}

class Client {

	public static void main(String[] args) {

		Demo obj = new DemoChid();
		obj.fun();
	}
}
