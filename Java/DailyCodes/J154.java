import java.io.*;

class ArraySum{

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of the array");
		int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                int sum = 0;

                for(int i = 0 ; i < arr.length ; i++){

                        arr[i] = Integer.parseInt(br.readLine());

                        sum = sum + arr[i];
                }

		//System.out.println(arr.length);

                System.out.println("Sum = "+sum);
        }
}
