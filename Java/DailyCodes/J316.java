class Parent {

        Parent() {

                System.out.println("Parent constructor");
        }

        void fun() {

                System.out.println("In fun");
        }
}

class Child extends Parent {

        Child() {

                System.out.println("Child constructor");
        }

        void gun() {

                System.out.println("In gun");
        }
}

class Client {

        public static void main(String[] args) {

                Parent obj1 = new Child();

                Child obj2 = new Parent();
        }
}
