/*
 
method : public int lastIndexOf(int ch,int uptoIndex);

Description :

Finds the last instance of the character in the given String.

parameters : character(ch to find)
	     Integer(index to end the search)

return type : Integer

*/

class LastIndexOfDemo {

	public static void main(String[] args) {

		String str1 = "Shashi";


		System.out.println(str1.lastIndexOf('h',5));
	}
}
