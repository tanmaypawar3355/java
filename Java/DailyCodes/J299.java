class Parent {

	Parent() {
		
		System.out.println("In parent constructor");

	}
}

class Child extends Parent {

	Child() {
		
		//this();
		//super();

		System.out.println("In child constructor");
	}
}

class Client {

	public static void main(String[] args) {

		Child obj2 = new Child();
	}
}
