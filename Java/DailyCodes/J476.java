import java.util.*;
//import java.util.Collections;

class CountryInheritance {
	
	int num = 0;
        String countryName = null;
        float GDP = 0.0f;
        float exportPerc = 0.0f;

        CountryInheritance(int num,String countryName,float GDP,float exportPerc) {
		
		this.num = num;
                this.countryName = countryName;
                this.GDP = GDP;
                this.exportPerc = exportPerc;
        }

        public String toString() {

		if(num == 1) 

			return "Parent : " + countryName + " : " + GDP + " : " + exportPerc + " || ";

		else
			return "Child : " + countryName + " : " + GDP + " : " + exportPerc;
        }
}

class SortByInheritance implements Comparator <CountryInheritance> {

        public int compare(CountryInheritance obj1,CountryInheritance obj2) {

                //return (((CountryInheritance)obj1).countryName.compareTo(((CountryInheritance)obj2).countryName));        
		return (obj1.countryName.compareTo((obj2).countryName));
        }
}

class SortByHighestGDP implements Comparator <CountryInheritance> {

	public int compare(CountryInheritance obj1,CountryInheritance obj2) {

		return (int) -(obj1.GDP - obj2.GDP);
	}
}

class SortByHighestExports implements Comparator <CountryInheritance> {

        public int compare(CountryInheritance obj1,CountryInheritance obj2) {

                return (int) -(obj1.exportPerc - obj2.exportPerc);
        }
}

class ListSortDemo {

        public static void main(String[] args) {

                ArrayList al = new ArrayList();


                al.add(new CountryInheritance(1,"India",3605.00f,18.08f));
                al.add(new CountryInheritance(0,"India-Pakistan",378.41f,9.58f));
                al.add(new CountryInheritance(0,"India-Bangladesh",485.51f,12.18f));

                System.out.println(al);

                System.out.println("\nSorted by inheritance");
                Collections.sort(al,new SortByInheritance());
                System.out.println(al);
                
		System.out.println("\nSorted by Highest GDP");
                Collections.sort(al,new SortByHighestGDP());
                System.out.println(al);
		
		System.out.println("\nSorted by Highest exports");
                Collections.sort(al,new SortByHighestExports());
                System.out.println(al);
        }
}
