class Demo {

	static {
		
		System.out.println("Static block 1");
	}

	public static void main(String[] args) {

		System.out.println("In Demo main");
	}
}

class Client {

	static {
		
		System.out.println("Static block 2");
	}

	public static void main(String[] args) {
		
		System.out.println("In Client main");
	}

	static {
		
		System.out.println("Static block 3");
	}
}
