class Parent {
	
	StringBuffer fun() {

                Object fun = new Object();
                System.out.println("Parent fun");
                return new StringBuffer("Tammy");
        }
}

class Child extends Parent {

        String fun() {

                System.out.println("Child fun");
                return "Shsashi";
        }
}

class Client {

        public static void main(String[] args) {

                Parent obj = new Child();
                obj.fun();
        }
}
