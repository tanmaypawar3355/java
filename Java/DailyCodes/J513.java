class MyThread implements Runnable {

	String str = null;

	MyThread(String str) {

		this.str = str;
	}

	public void run() {

		System.out.println(Thread.currentThread().getName());

		Thread.currentThread().setName(str);
		
		System.out.println(Thread.currentThread().getName());
	}
}

class ThreadDemo {

	public static void main(String[] args) {
		
		System.out.println(Thread.currentThread().getName());

		MyThread obj1 = new MyThread("Thread 0.0");

		Thread T1 = new Thread(obj1);

		T1.start();
		
		MyThread obj2 = new MyThread("Thread 1.1");

		Thread T2 = new Thread(obj2);

		T2.start();
	}
}

