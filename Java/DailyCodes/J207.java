/*
 
method : public char charAt(int index);

Description :
It returns the character located at specified index within the given String

parameters : integer(index)
return type : character

*/

class charAtDemo {

	public static void main(String[] args) {

		String str = "Core2Web";

		System.out.println(str.charAt(4));
		System.out.println(str.charAt(0));
		System.out.println(str.charAt(8));
		
		/*
		char arr[] = str.toCharArray();
		System.out.println(arr[4]);
		System.out.println(arr[0]);
		System.out.println(arr[8]);
		*/
	}
}
