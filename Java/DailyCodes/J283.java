class Demo {

	int x = 10;    // constructor mdhi initialize hotana this.x = 10; asa hoto

	Demo() {

		System.out.println("In constructor");
	}

	void fun() {             // Ghenara fun(Demo this)
		
		System.out.println(x);
	}

	public static void main(String[] args) {

		Demo obj1 = new Demo();

		obj1.fun();         // internally goes as a ---> fun(obj1)
	}
}
