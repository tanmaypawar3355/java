import java.util.concurrent.*;

class MyThread implements Runnable {

	int num;

	MyThread(int num) {

		this.num = num;
	}

	public void run() {

		System.out.println(Thread.currentThread() + "Start thread : " +num);
		dailyTask();
		System.out.println(Thread.currentThread() + "End thread : " +num);
	}

	void dailyTask() {

		try {

			Thread.sleep(1000);
		}catch(InterruptedException obj) {

		}
	}
}

class ThreadPoolDemo {

	public static void main(String[] args) {

		ExecutorService ser = Executors.newSingleThreadExecutor();

		for(int i = 0 ; i < 6 ; i++) {

			MyThread obj = new MyThread(1);
			ser.execute(obj);
		}

		ser.shutdown();
	}
}
