class Demo {

	int x = 10;

	Demo() {   // Demo(Demo this)

		System.out.println("No - args Constructor");
	}

	Demo(int x) {   // Demo(Demo this, int x)
		
		System.out.println("Para - Constuctor");
	}

	public static void main(String[] args) {

		Demo obj1 = new Demo();  // Demo(obj1)

		Demo obj2 = new Demo(10);  //Demo(obj2,10)
	}
}
