class Outer {

	class Inner {

		static int x = 10;
	}
}

class Client {

	public static void main(String[] args) {

		Outer obj = new Outer();

		Outer.Inner obj1 = obj.new Inner();
	}
}
