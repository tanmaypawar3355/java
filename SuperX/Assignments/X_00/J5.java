/*

5) WAP to reverse a number, and put successive number sum into an array and print it.
4 pts.
Input : 45689
Output : 17,14,11,9,4

*/


import java.io.*;

class Output {

	void method(int num) {

		int num1 = num,count = 0,sum = 0;

		while(num1 != 0) {

			int rem = num1 % 10;
			sum = (sum * 10) + rem;
			count++;
			num1 = num1 / 10;
		}
	
		//System.out.println(sum);
		//System.out.println(count);

		int num2 = num,index = 0;
		int arr1[] = new int[count];

		while(num2 != 0) {

			int rem = num2 % 10;
			arr1[index] = rem;
			num2 = num2 / 10;
			index++;
		}

		int arr[] = new int[count];

		int rem1 = sum % 10;
		arr[count-1] = rem1;

		for(int i = 0 ; i < count - 1 ; i++) {

			arr[i] = arr1[i] + arr1[i+1];
			System.out.print("[" + arr[i] + "]");
		}
			
		System.out.println("[" + arr[count-1] + "]");

	}
}

class SuccessiveNo {

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter number");
                int num = Integer.parseInt(br.readLine());

                Output obj = new Output();
                obj.method(num);
        }
}
