/* 

1) WAP to print the factorial of even numbers in a given number. 2 pts.
Input : 256946
Output : 2, 720, 24, 720


*/

import java.io.*;

class Output {

	void method(int num) {

		int sum = 0;

		while(num != 0) {
			
			int rem1 = num % 10;
			sum = (sum * 10) + rem1;
			num = num / 10;
		}

		//System.out.println(sum);

		while(sum != 0) {

			int rem2 = sum % 10;

			if(rem2 % 2 == 0) {

				int sum1 = 1;

				for(int i = rem2 ; i >= 1 ; i--) {

					sum1 = sum1 * i;
				}

				System.out.println(sum1);
			}
				sum = sum / 10;
		}
	}
}

class Facto {

	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Number");

		int num = Integer.parseInt(br.readLine());

		Output obj = new Output();

		obj.method(num);
	}
}






