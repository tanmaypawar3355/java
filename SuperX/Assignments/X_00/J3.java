/*
 
3) Print the following pattern 3 pts.
	D C B A
	e f g h
	F E D C
	g h i j

*/

import java.io.*;

class Output {

	void method(int num) {
		
		char ch1 = (char) (64 + num);
		char ch2 = (char) (98 + num);

		for(int i = 0 ; i < num ; i++) {

			for(int j = 0 ; j < num ; j++) {
				
				if(i % 2 == 0) {
					System.out.print("    " + ch1--);
				}else{
					System.out.print("    " + ch2++);

				}
			}
			
			ch1 = (char) (64 + num);
			ch1++;
			ch1++;

			ch2--;

			System.out.println();
		}
	}
}

class Pattern {

	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter number");
		int num = Integer.parseInt(br.readLine());

		Output obj = new Output();
		obj.method(num);
	}
}



