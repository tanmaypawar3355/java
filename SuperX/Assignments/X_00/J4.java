/*

4) Print the following pattern 3 pts.
	A b C d E
	e D c B
	B c D
	d C
	C

*/



import java.io.*;

class Output {

        void method(int num) {

                char ch2 = 97;
                char ch1 = 65;

                for (int i = 0 ; i < num ; i++) {

                        for(int j = 0 ; j < num - i ; j++) {

                                if(i % 2 == 0) {

                                        if((i + j) % 2 == 0) {

                                                System.out.print(ch1++ + "  ");
                                                ch2++;
                                        }else{
                                                System.out.print(ch2++ + "  ");
                                                ch1++;
                                        }

                                } else {

                                        if((i + j) % 2 != 0) {

                                                System.out.print(--ch2 + "  ");
                                                ch1--;
                                        }else{
                                                System.out.print(--ch1 + "  ");
                                                ch2--;
                                        }
                                }
                        }
                        System.out.println();

                }
        }
}

class Pattern {

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter number");
                int num = Integer.parseInt(br.readLine());

                Output obj = new Output();
                obj.method(num);
        }
}
