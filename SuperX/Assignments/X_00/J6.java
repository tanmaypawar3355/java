/*

6) WAP to find a number which has numbers on its left is less than or equal to itself.
4 pts.
Input : 456975962
Output : 9

*/

import java.io.*;


class Output {

	void method(int num) {

		int num1 = num,count = 0,sum = 0;

		while(num1 != 0) {

			int rem = num1 % 10;
			count++;
			sum = (sum * 10) + rem;
			num1 = num1 / 10;
		};

		//System.out.println(sum);

		int arr[] = new int[count];
		int index = 0;

		while(sum != 0) {

			int rem = sum % 10;
			arr[index] = rem;
			sum = sum / 10;
			index++;
		}

		int bigNo = 0;

		for(int i = 0 ; i < count ; i++) {

			for(int j = i+1 ; j < count ; j++) {

				if(arr[j] > arr[i] || arr[j] == arr[i]) {

					bigNo = arr[j];
					break;
				}
			}
		}

		System.out.println("----" + bigNo);

	}
}

class LeftIsLess {

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter number");
                int num = Integer.parseInt(br.readLine());

                Output obj = new Output();
                obj.method(num);
        }
}
