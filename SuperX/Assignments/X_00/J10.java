/*

10) WAP to check whether the given string is palindrome or not. 3 pts.
Input 1 : malayalam
Output 1 : It is palindrome String
Input 2 : armstrong
Output 2 : It is not a palindrome String

*/

import java.io.*;

class Output {

	void method(String str) {

		char arr[] = str.toCharArray();

		int first = 0,flag = 0,last = arr.length-1;

		int length = arr.length/2;
		System.out.println(length);

		for(int i = 0 ; i < length ; i++) {

			if(arr[i] == arr[last]) {

				last--;
			}else{
				flag = 1;
				break;
			}
		}


		if(flag == 0) {
			System.out.println("It is palindrome String");
		}else{
			System.out.println("It is not a palindrome String");
		}
	}
}

class PalindromeString {

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter string");
                String str = br.readLine();

                Output obj = new Output();
                obj.method(str);
        }
}
