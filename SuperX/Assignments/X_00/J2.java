/*

2) WAP to find whether the number is an armstrong number or not. 3 pts.
Input 1 : 153
Output : It is an armstrong number
Input 2 : 376
Output : It is not a armstrong number

*/

import java.io.*;

class Output {

	void method(int num) {

		int count = 0,num1 = num,num2 = num,sum = 0;

		while(num2 != 0) {

			num2 = num2 / 10;
			count++;
		}

		while(num != 0) {

			int rem = num % 10;
			int mul = 1;

			for(int i = 1 ; i <= count ; i++) {

				mul = mul * rem;
			}
			sum = sum + mul;
			num = num / 10;
		}

		if(num1 == sum) {

			System.out.println("It is an armstrong number");
		}else{
			System.out.println("It is not an armstrong number");
		}
	}
}


class ArmNo {

	public static void main(String[] args) throws IOException {
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter Number");

                int num = Integer.parseInt(br.readLine());

		Output obj = new Output();

		obj.method(num);

	}
}


