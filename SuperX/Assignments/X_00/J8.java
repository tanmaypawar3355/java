/*

8) WAP to find the occurrence of vowels in a given string.

Input : adgtioseobi
Output : a = 1
	 e = 1
	 i = 2
	 o = 2

*/

class Output {
	
	void method(String str) {

                char arr1[] = {'a','e','i','o','u'};
                char arr2[] = str.toCharArray();

                for(int i = 0 ; i < arr1.length ; i++) {

                        int count = 0;

                        for(int j = 0 ; j < arr2.length ; j++) {

                                if(arr1[i] == arr2[j]) {

                                        count++;
                                }
                        }

                        if(count >= 1)
                        System.out.println(arr1[i] + " = " + count);
                }
        }
}

class StringVowels {

	public static void main(String[] args) {

		String str = "adgtioseobi";
	
		Output obj = new Output();
		obj.method(str);		
	}
}
