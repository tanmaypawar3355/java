/*

7) Take the size of the array from the user. Create two arrays of that size. Initialize all
second array elements as zero(0). For the first array take all elements from the user.
Check if the elements in the first array are even or not if it's even then replace the
value of the second array of that index with 1 and print both the array.

Input : Size : 10
Array 1 Elements : 4 2 3 6 8 7 1 0 9 5
Output : Array 1 Elements : 4 2 3 6 8 7 1 0 9 5
Array 2 Elements : 1 1 0 1 1 0 0 1 0 0

*/



import java.io.*;

class Output {

	void method(int num) throws IOException {

		int arr1[] = new int[num];
		int arr2[] = new int[num];

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter " + num + " elements for array 1");
		for(int i = 0 ; i < num ; i++) {

			int no = Integer.parseInt(br.readLine());
			arr1[i] = no;
			if(arr1[i] % 2 == 0) {

				arr2[i] = 1;
			}
		}
		
		System.out.print("Array 1 elements : ");
		for(int i = 0 ; i < num ; i++) {

			System.out.print("[" + arr1[i] + "]");
		}

		System.out.println();
		System.out.print("Array 2 elements : ");
		for(int i = 0 ; i < num ; i++) {

			System.out.print("[" + arr2[i] + "]");
		}
	}
}

class ArrayEvenDemo {

        public static void main(String[] args) throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size of array");
                int num = Integer.parseInt(br.readLine());

                Output obj = new Output();
                obj.method(num);
        }
}
