/*

9) WAP to take string from user and convert all even indexes of string to uppercase and
odd indexes of a string to lowercase. 
Input : dfTbnSrOvryt
Output : DfTbNsRoVrYt

*/

class UpperLower {

	public static void main(String[] args) {

		String str = "dfTbnSrOvryt";
		
		System.out.println("Input : " + str);
		System.out.print("Output : " );
	


		char arr[] = str.toCharArray();

		for(int i = 0 ; i < arr.length ; i++) {

			if(i % 2 == 0) {

				if(arr[i] >= 65 && arr[i] <= 90) {

				}else{
					arr[i] = (char) (arr[i] - 32);
				}
			}else{
				if(arr[i] >= 97 && arr[i] <= 122) {

				}else{
					arr[i] = (char) (arr[i] + 32);
				}
			}
			System.out.print(arr[i]);
		}
		System.out.println();

	}
}


